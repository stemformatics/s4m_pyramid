"""Script to convert an expression data flie with entrez ids as row ids into ensembl ids as row ids
so that it can be used in stemformatics.

Usage:
    > python scripts/convert_entrez_ids.py

Customise each run by creating a separate function for that run.

The annotation comes from postgresql table genome_annotations, and extracted like this:
> portal_prod=# copy (select gene_id,entrezgene_id,associated_gene_name from genome_annotations where db_id=59) to '/tmp/genome_annotations.tsv' with csv delimiter E'\t';

Not db_id=59, which should correspond to the latest ensembl build.
"""

import sys, pandas

annotationFile = "/data/db/new_datasets/genome_annotations.tsv"
def convert(df, geneSymbolColumn=None):
    """Convert entrez ids from df and return a data frame with ensembl ids.
    Note that entrez ids will be read in as integer - just keep consistent with the same column from annotation
    """
    #print(df.shape, len(set(df.index)))
    anno = pandas.read_csv(annotationFile, sep="\t", header=None, names=["ensemblId", "entrezId","symbol"])

    newRows, newIndex = [], []   # these will form the new data frame
    for index,row in df.iterrows():
        annoSubset = anno[anno["entrezId"]==index]
        if geneSymbolColumn is not None and len(annoSubset)==0:  # try gene symbol
            annoSubset = anno[anno["symbol"]==row[geneSymbolColumn]]
        for item in annoSubset["ensemblId"]:
            newIndex.append(item)
            newRows.append(row)

    return pandas.DataFrame(newRows, index=newIndex, columns=df.columns)

# Actual conversions made. Put custom code here for each dataset.
def convert_7415():  # 2020-04, for dataset id 7415
    # convert raw counts
    infile = '/data/db/new_datasets/7415/7415.raw.entrezId.tsv'
    outfile = '/data/db/new_datasets/7415/7415.raw.tsv'

    counts = pandas.read_csv(infile, sep="\t", index_col=0)
    df = convert(counts, geneSymbolColumn="Symbol")
    df = df[[col for col in df.columns if col not in ["Length","Symbol"]]]  # drop these columns
    df.to_csv(outfile, sep='\t')

    # convert rpkm values - we need to attach the gene symbol column from counts
    infile = '/data/db/new_datasets/7415/rpkm.entrezId.tsv'
    outfile = '/data/db/new_datasets/7415/7415.rpkm.tsv'
    rpkm = pandas.read_csv(infile, sep="\t", index_col=0)
    symbolDict = counts["Symbol"].to_dict()
    rpkm["Symbol"] = [symbolDict.get(entrezId, "") for entrezId in rpkm.index]
    df = convert(rpkm, geneSymbolColumn="Symbol")
    df = df[[col for col in df.columns if col not in ["Symbol"]]]  # drop these columns
    df.to_csv(outfile, sep="\t")

    # create gct file from rpkm
    outfile = '/data/db/new_datasets/7415/dataset7415.gct'
    fileobj = open(outfile, 'w')
    fileobj.write("#1.2\n%s\t%s\n" % (len(df), len(df.columns)))
    df.insert(0, "Description", ["" for item in df.index])
    df.to_csv(fileobj, sep="\t")
    fileobj.close()

    # Afer running this, I copied the gct file over using this:
    # (virtualenv_dev) (base) [portaladmin@w4-s4m s4m_pyramid]$ cp /data/db/new_datasets/7415/dataset7415.gct /var/www/pylons-data/prod/GCTFiles/.

if __name__ == "__main__":
    #convert_7415()
    pass
    