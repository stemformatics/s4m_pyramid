# https://www.cyberciti.biz/faq/linux-unix-minify-compress-css-javascript-files-shell-prompt/
# Please visit above link to set up yui-compressor
HOMEDIR=/home/isha/s4m_pyramid
cd /home/isha/s4m_pyramid/S4M_pyramid/public/js
ls -alh
cd expressions/
rm *_min.js
rm geg.min.js
rm biojs.min.js helper.min.js graph_data.min.js
java -jar /home/isha/yuicompressor-2.4.8.jar --type js -o '.js$:_min.js' *.js
cat graph_min.js gene_expression_graph_triggers_min.js gene_expression_graphs_min.js  > geg.min.js
cat biojs-vis-line-plot_min.js biojs-vis-scatter-plot_min.js biojs-vis-violin-plot_min.js biojs-vis-box-bar-plot_min.js > biojs.min.js
cat box_bar_line_min.js axis_min.js general_min.js features_min.js test_min.js > helper.min.js
cat graph_data_box_bar_min.js graph_data_line_min.js graph_data_scatter_min.js graph_data_violin_min.js > graph_data.min.js
ls -alh

cd /home/isha/s4m_pyramid/S4M_pyramid/public

# This needs to be used as it has the cookie consent plugin. We want to have the css being used but not the js.
# This is because that existing users can then have the css cached so that when we turn on the js later, they
# will have a better experience
#cat js/cookieconsent.plugin.min.js js/main.js js/table2CSV.js  | yui-compressor --type=js > js/main_table2CSV_help_min.js

cat js/main.js js/table2CSV.js  | java -jar /home/isha/yuicompressor-2.4.8.jar  --type=js > js/main_table2CSV_help_min.js
ls -alh js/main_table2CSV_help_min.js


cd /home/isha/s4m_pyramid/S4M_pyramid/public/js/msc_signature
rm *_min.js
cat rohart_msc_test.js rohart_msc_graph.js | java -jar /home/isha/yuicompressor-2.4.8.jar  --type=js  > rohart_msc_test_min.js
ls -alh

