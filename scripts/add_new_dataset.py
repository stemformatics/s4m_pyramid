"""This script is designed to add a new dataset in Stemformatics, where the source is the data portal.
The steps required are:
1. Obtain the expression matrix, samples and dataset metadata from the data portal through the api.
    Write these as files locally, so that we don't have to go back to step 1 each time.
2. Insert records into sql tables for dataset metadata and samples.
3. Copy expression files to destination (raw counts actually have to go to pipe.stemformatics.org, while rpkm is local).
4. Insert records into redis, including user access privileges.
"""
import os, sys, pandas, numpy, psycopg2, requests, json, redis

# Define some globals here to clearly show where various input/output files are.
# localFilePath is where data from api server get saved as text files. It will be created if it doesn't exist.
sqlConnectionPath = "dbname=portal_prod user=portaladmin"
apiServerURL = 'api.stemformatics.org'
localFilepath='/data/db/new_datasets'
gctFileLocation = "/var/www/pylons-data/prod/GCTFiles"
redisSocketPath = '/data/redis/redis.sock'
tokenFileLocation = "/data/portal_data/api_token.txt"
datasetId = sys.argv[1]
dataType = None   # one of ['microarray','rna-seq'], to be set after running setDataType

#################################################################
# Utility functions
#################################################################
def _runSql(sql, data=None, type="select", printSql=False):
    '''Run sql command. Use type="update" for an update command. type="test" will just show sql without committing.
    '''
    conn = psycopg2.connect(sqlConnectionPath)
    cursor = conn.cursor()

    if printSql or type=="test":  # To see the actual sql executed, use mogrify:
        print(cursor.mogrify(sql, data))
        if type=="test": return None
        
    cursor.execute(sql, data)

    if type=="select":
        result = cursor.fetchall()
    elif type=="update":
        result = cursor.rowcount
        conn.commit()  # doesn't update the database permanently without this

    cursor.close()
    conn.close()
    return result
    
def convert(df, geneSymbolColumn=None):
    """Convert entrez ids from df and return a data frame with ensembl ids.
    Note that entrez ids will be read in as integer - just keep consistent with the same column from annotation.
    If df contains gene symbol, we can try to use that to find ensembl id, if we can't initially find a matching
    ensembl id based on entrez id. Here, geneSymbolColumn is the name of the column in df containing gene symbol.
    """
    ##annotationFile = os.path.join(localFilepath, "genome_annotations.tsv")
    result = _runSql("select gene_id,entrezgene_id,associated_gene_name from genome_annotations where db_id=59")
    anno = pandas.DataFrame(result, columns = ["ensemblId", "entrezId","symbol"])

    newRows, newIndex = [], []   # these will form the new data frame
    for entrezId,row in df.iterrows():
        ensemblIds = set(anno[anno["entrezId"]==entrezId]["ensemblId"])
        # This contains the set of matching ensembl ids for this entrez id.
        # There may be no matches, or multiple ensembl id matches. We only want to assign this row
        # once to the new data frame, so if geneSymbolColumn is specified maybe we can use this to help.
        if geneSymbolColumn is not None and (len(ensemblIds)==0 or len(ensemblIds)>1): 
            ensemblIds2 = set(anno[anno["symbol"]==row[geneSymbolColumn]]["ensemblId"])
            ensemblIds = ensemblIds.intersection(ensemblIds2)
        
        # Ready to add to new data frame. If we still have multiple matches, we'll just have to take the first one
        if len(ensemblIds)>0:
            newIndex.append(list(ensemblIds)[0])
            newRows.append(row)

    return pandas.DataFrame(newRows, index=newIndex, columns=df.columns)


#################################################################
# Main functions
#################################################################
def getDataFromDataPortal():
    '''Fetch data from the data portal using the api and copy them as text files locally.
    We need to convert many of the fields into keys compatible with stemformatics.
    We also need to insert some values which don't exist in the data portal.
    Note that double quotes must be used for json like values, such as for sampleTypeDisplayGroups.
    '''
    print("#### Getting data from %s" % apiServerURL)

    # Fetch datasets and dataset_metadata from api server - note the (private) also works with public datasets
    #data = { 'username' : 'jarnyc@unimelb.edu.au', 'password' : 'default' }
    #r = requests.post('https://api.stemformatics.org/api_jwt_token_generated', data=json.dumps(data), verify=False)
    #token = json.loads(r.text)['token']
    token = [line for line in open(tokenFileLocation).read().split("\n") if not line.startswith("#")][0]
    headers = {'accept': 'application/json', 'X-API-KEY':token}

    print("  Fetching dataset and sample metadata ...")
    r = requests.get('https://%s/(private) samples / metadata/%s/metadata' % (apiServerURL, datasetId), headers=headers)
    datasets = pandas.Series(json.loads(r.json())[0]['datasets'][0])
    dataset_metadata = pandas.Series(json.loads(r.json())[0]['dataset_metadata'][0])
    assert len(datasets)>0
    assert len(dataset_metadata)>0

    # Fetch expression matrix from api server
    print("  Fetching expression matrix ...")
    r = requests.get('https://%s/(private) expression/%s/raw' % (apiServerURL, datasetId), headers=headers)
    result = r.json()
    df = pandas.DataFrame(result['data'], index=result['index'], columns=result['columns'])

    # Create directories if needed
    if not os.path.exists(localFilepath):
        os.mkdir(localFilepath)
    outputDir = os.path.join(localFilepath, str(datasetId))
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    # Columns of datasets table 
    datasetsColumns = ['id', 'lab', 'dtg', 'handle', 'published', 'private', 'chip_type', 'min_y_axis', 
                      'show_yugene', 'show_limited', 'db_id', 'number_of_samples', 'data_type_id', 'mapping_id', 'log_2']

    # Column map of biosamples_metadata
    columnMap = {'Replicate_group_id':'Replicate Group ID', 'Sample_name':'Sample name', 'Sample_name_long':'Sample name long',
                 'Sample_type':'Sample Type', 'Sample_type_long':'Sample type long', 'Generic_sample_type':'Generic sample type',
                 'Generic_sample_type_long':'Generic sample type long', 'Sample_description':'Sample Description',
                 'Tissue_organism_part':'Tissue/organism part', 'Parental_cell_type':'Parental cell type', 
                 'Final_cell_type':'Final cell type', 'Cell_line':'Cell line', 'Reprogramming_method':'Reprogramming method',
                 'Developmental_stage':'Developmental stage', 'Media':'Media', 'Disease_state':'Disease State', 'Labelling':'Labelling',
                 'Genetic_modification':'Genetic modification', 'Facs_profile':'FACS profile', 'Age':'Age', 'Sex':'Sex', 'Organism':'Organism'}

    print(datasets)

def updateSqlTables():
    '''Update the stemformatics sql tables with dataset metadata and sample info.
    The input files for this function come from localFilePath/{datasetId}/ and are assumed to
    have correct keys ready for input after running getDataFromDataPortal() function.
    '''
    print("#### Updating sql tables at %s" % sqlConnectionPath)

    print("  Updating dataset table ...")
    filepath =  os.path.join(localFilepath, str(datasetId), "datasets.tsv")
    values = pandas.read_csv(filepath, sep="\t", index_col=0, header=None, squeeze=True).fillna("")  # read as pandas Series
    chip_type = int(values['chip_type'])
    # We will put single quote around values unless the field is number
    values = [("%s" % val) if key in ['id','chip_type','db_id','number_of_samples','data_type_id','mapping_id']\
                 else ("'%s'" % val) for key,val in values.items()]
    _runSql("delete from datasets where id=%s", (datasetId,), type="update")
    _runSql("insert into datasets values (%s)" % ",".join(values), type="update")

    print("  Updating dataset_metadata table ...")
    # Use this to sort index ignoring case: values = values.loc[sorted(values.index.tolist(), key=str.casefold)]
    filepath =  os.path.join(localFilepath, str(datasetId), "dataset_metadata.tsv")
    values = pandas.read_csv(filepath, sep="\t", index_col=0, header=None, squeeze=True).fillna("")  # read as pandas Series
    _runSql("delete from dataset_metadata where ds_id=%s", (datasetId,), type="update")
    for index,val in values.items():
        _runSql("insert into dataset_metadata values (%s,%s,%s)", (datasetId, index, val), type="update")

    print("  Updating biosamples_metadata table ...") # note: need chip type from datasets table
    filepath =  os.path.join(localFilepath, str(datasetId), "biosamples_metadata.tsv")
    df = pandas.read_csv(filepath, sep="\t", index_col=0).fillna('')
    _runSql("delete from biosamples_metadata where ds_id=%s", (datasetId,), type="update")
    for column in df.columns:
        for sampleId,value in df[column].items():
            try:
                result = _runSql("insert into biosamples_metadata values (%s,%s,%s,%s,%s)", (chip_type, sampleId, column, value, datasetId), type="update")
            except:
                print(column,value)
                raise
    
def copyExpressionFiles():
    '''Process expression files and place the rpkm version in the expected location as a gct file.
    '''
    print("#### Processing expression file to go into %s" % gctFileLocation)

    # Read expression file
    df = pandas.read_csv(os.path.join(localFilepath, str(datasetId), "%s.raw.tsv" % datasetId), sep="\t", index_col=0)
    
    print("  Converting to rpkm log2, then writing as gct file ...")
    df = pandas.read_csv(os.path.join(localFilepath, str(datasetId), "%s.rpkm.tsv" % datasetId), sep="\t", index_col=0)
    df = numpy.log2(df+1)

    # Write it as a gct file to where it should go
    gct = df.copy()
    gct.insert(0, "Description", ["" for item in gct.index])
    gct.index.name = "NAME"
    filepath = os.path.join(gctFileLocation, "dataset%s.gct" % datasetId)
    f = open(filepath, 'w')
    f.write("#1.2\n%s\t%s\n" % df.shape)
    gct.to_csv(f, sep="\t")
    f.close()

def insertDataIntoRedis():
    '''Insert data into Redis server.
    Currently we're running /admin/setup_new_dataset/7415 (substitute correct dataset id) on the browser after copyExpresionFiles()
    has been run. It would be good to find out what is run in the back-end and bypass that need on the front-end at some point.
    '''
    print("#### Inserting data into redis server at %s" % redisSocketPath)
    r = redis.Redis(unix_socket_path = redisSocketPath, decode_responses = True)

    print("  Setting gct_label key ...")
    # This is a list of sample ids, which we can get from expression matrix
    df = pandas.read_csv(os.path.join(localFilepath, str(datasetId), "%s.raw.tsv" % datasetId), sep="\t", index_col=0)
    r.set('gct_labels|%s' % datasetId, '|'.join(df.columns))

    #print("  Setting user access ...")
    # Will look to implement this in the next version.

if __name__ == "__main__":
    getDataFromDataPortal()
    #updateSqlTables()
    #copyExpressionFiles()
    #insertDataIntoRedis()
    
