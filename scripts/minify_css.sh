#!/bin/bash
HOMEDIR=/home/isha/s4m_pyramid

cd $HOMEDIR/S4M_pyramid/public/css/tripoli
cat tripoli.base.css plugins/tripoli.visual.css plugins/tripoli.type.css tripoli.simple.css | java -jar /home/isha/yuicompressor-2.4.8.jar --type=css > tripoli.full.min.css
cd $HOMEDIR/S4M_pyramid/guide/public/css
cat ../themes/ui-lightness/jquery-ui-1.8rc3.custom.min.css cookiecontent.style.min.css tripoli/tripoli.full.min.css > combined_jquery_ui_tripoli_full.min.css
ls -alh combined_jquery_ui_tripoli_full.min.css
