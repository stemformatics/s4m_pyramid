import pyramid
from pyramid.paster import get_appsettings
from pyramid.config import Configurator
from S4M_pyramid.model.stemformatics import *
import re

app = pyramid.paster.get_app('production.ini')

def test_this():
    # 12 characters and with a space
    config = Stemformatics_Admin.get_all_configs()
    correct_validation_regex_text = '(?=^.{12,}$)(?=.*\s+).*$'
    password = 'I am the SCA'
    validation_regex_text = config['validation_regex']

    validation_regex = re.compile(validation_regex_text)
    m = validation_regex.match(password)

    assert validation_regex_text == correct_validation_regex_text
    assert m is not None
