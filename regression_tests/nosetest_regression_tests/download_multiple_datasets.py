import pyramid
from pyramid.paster import get_appsettings
from pyramid.config import Configurator
import json
from S4M_pyramid.model.stemformatics import *

app = pyramid.paster.get_app('production.ini')

all_sample_metadata = ''
all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()


"""
monocyte
fibroblast
kidney and monocyte - changed to kidney and macrophage
kidney or monocyte 
kidney and monocyte or fibroblast 

2000
matigian

kidney and not monocyte - might skip this one for now
not kidney and not monocyte - might skip this one for now

"""

uid = 3



def aaa_test_dataset_search():
    search_term = 'ryan'
    get_samples = True
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert len(result) >=2 
    assert 'Ryan' in result[6063]['handle'] 
    all_samples = temp_result['all_samples_by_ds_id']
    assert len(all_samples)==0

def test_dataset_search():
    search_term = 'monocyte'
    
    result = Stemformatics_Dataset.dataset_search(uid,search_term,{})
    assert 'Watkins' in result[1000]['handle'] 

    search_term = 'Mus musculus and cardiac'
    result = Stemformatics_Dataset.dataset_search(uid,search_term,{})
    assert 'Pinto' in result[6160]['handle'] 

def test_monocyte_simple():
    search_term = 'monocyte'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert len(result) > 4
    assert 'Watkins' in result[1000]['handle'] 
    assert 'Beckhouse' in result[5020]['handle']
    assert 5005 not in result

def test_fibroblast_simple():
    search_term = 'fibroblast'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert len(result) > 45
    assert 'Ieda' in result[5001]['handle'] 
    assert 'Matigian' in result[2000]['handle'] 
    assert 'Chin' in result[6166]['handle'] 

def test_msc_simple():
    search_term = 'msc'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert len(result) > 60
    assert 'Matigian' in result[2000]['handle'] 
    assert 'Chin' in result[6166]['handle'] 

 
 
def test_monocyte_complex():
    search_term = 'monocyte and homo sapien'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert len(result) > 4
    assert 'Watkins' in result[1000]['handle'] 
    assert 'Beckhouse' in result[5020]['handle']
    assert 50 == result[1000]['number_of_samples'] 
    assert 6 == result[5020]['number_of_samples'] 
    assert 5005 not in result


    search_term = 'Mus musculus and cardiac'
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert 'Pinto' in result[6160]['handle'] 
    assert 9 == result[6160]['number_of_samples'] 

    search_term = 'kidney and macrophage'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert len(result) >= 1


def test_dataset_id():
    search_term = '2000'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert 'Matigian' in result[2000]['handle'] 
    assert 62 == result[2000]['number_of_samples'] 
    assert 5005 not in result
    assert len(result) > 10

    search_term = '6111'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert 'Grandiose' in result[6111]['handle'] 
    assert 16 == result[6111]['number_of_samples'] 
    assert 5005 not in result
    assert len(result) >= 1


def test_handle_search():
    search_term = 'Matigian'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert 'Matigian' in result[2000]['handle'] 
    assert 62 == result[2000]['number_of_samples'] 
    assert 5005 not in result
    assert len(result) == 4 

    search_term = 'McWeeney'
    get_samples = False
    
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    assert 'McWeeney' in result[6329]['handle'] 
    assert 59 == result[6329]['number_of_samples'] 
    assert 5005 not in result
    assert len(result) == 1 

def test_get_samples_true():
    search_term = 'monocyte'
    get_samples = True
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    all_samples = temp_result['all_samples']

    row = all_samples[0]

    chip_type = row[0]
    chip_id = row[1]
    ds_id = row[2]

    meta_data_values = all_sample_metadata[chip_type][chip_id][ds_id]

    assert meta_data_values['Replicate Group ID'] == '61230-145'

    
    samples_for_watkins = []
    for row in all_samples:
        chip_type = row[0]
        chip_id = row[1]
        ds_id = row[2]
        if ds_id == 1000:
            meta_data_values = all_sample_metadata[chip_type][chip_id][ds_id]
            meta_data_values['chip_id'] = chip_id
            samples_for_watkins.append(meta_data_values)
            
    # Should be 7 monocyte samples for Watkins dataset
    assert len(samples_for_watkins) ==7
    assert samples_for_watkins[0]['Replicate Group ID'] == '61230-145'
    assert samples_for_watkins[0]['Organism Part'] == 'blood'
    assert samples_for_watkins[0]['Labelling'] == 'biotin'
    assert samples_for_watkins[0]['Sample Type'] == 'monocyte'
    assert samples_for_watkins[0]['chip_id'] == '1674120023_F'
    assert len(result) > 4
    assert 'Watkins' in result[1000]['handle'] 
    assert 'Beckhouse' in result[5020]['handle']
    assert 5005 not in result
 
def test_get_samples_only_mouse_true():
    search_term = 'Mus musculus and neonatal'
    search_term = 'neonatal and mus musculus'
    get_samples = True
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search_term,get_samples,uid,all_sample_metadata)
    result = temp_result['datasets'] 
    all_samples = temp_result['all_samples']
    all_samples_by_ds_id = temp_result['all_samples_by_ds_id']

    row = all_samples[0]

    chip_type = row[0]
    chip_id = row[1]
    ds_id = row[2]

    meta_data_values = all_sample_metadata[chip_type][chip_id][ds_id]

    assert meta_data_values['Replicate Group ID'] == 'aMHC neonatal cardiac fibroblasts 2 weeks GFP- rep1'

    
    samples_for_ieda = []
    check_duplicates = []
    for row in all_samples:
        chip_type = row[0]
        chip_id = row[1]
        ds_id = row[2]
        if ds_id == 5001:
            if chip_id not in check_duplicates:
                meta_data_values = all_sample_metadata[chip_type][chip_id][ds_id]
                meta_data_values['chip_id'] = chip_id
                samples_for_ieda.append(meta_data_values)
                check_duplicates.append(chip_id) 


    len(all_samples_by_ds_id[5001]) == len(samples_for_ieda)

    # Should be 7 monocyte samples for Watkins dataset
    assert len(samples_for_ieda) ==18
    assert samples_for_ieda[0]['Replicate Group ID'] == 'aMHC neonatal cardiac fibroblasts 2 weeks GFP- rep1'
    assert samples_for_ieda[0]['Organism Part'] == 'heart'
    assert samples_for_ieda[0]['Labelling'] == 'biotin'
    assert samples_for_ieda[0]['Sample Type'] == 'week 2 cardiac fibroblasts GFP-'
    assert samples_for_ieda[0]['chip_id'] == 'GSM554203'
    assert 'Ieda' in result[5001]['handle'] 
    assert 5005 not in result
 
     
