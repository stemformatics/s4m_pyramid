"""Package to act as primary data interface to Stemformatics application.
The aim is to gradually phase out stemformatics_*.py files with this package, as the legacy code there 
is not well designed, and hard to make modifications.

Notes on redis:
- Redis seems to store values as strings, even if you pass a list of numbers:
redis /mnt/data/redis/redis.sock> set test [0,1,2]
OK
redis /mnt/data/redis/redis.sock> get test
"[0,1,2]"

"""
import redis, psycopg2

def redisServer(redisSocketPath):
    return redis.Redis(unix_socket_path = redisSocketPath, decode_responses = True)

def _runSql(sqlConnectionPath, sql, data=None, type="select", printSql=False):
    conn = psycopg2.connect(sqlConnectionPath)
    cursor = conn.cursor()

    if printSql:  # To see the actual sql executed, use mogrify:
        print(cursor.mogrify(sql, data))
        
    cursor.execute(sql, data)

    if type=="select":
        result = cursor.fetchall()
    elif type=="update":
        result = cursor.rowcount
        conn.commit()  # doesn't update the database permanently without this

    cursor.close()
    conn.close()
    return result
