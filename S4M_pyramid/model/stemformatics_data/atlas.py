"""Model to handle atlas data in Stemformatics. Atlas data refers to an object of integrated data, 
and should sit behind the integrated PCA as the primary data interface. Mostly the data for the atlas
come from the data portal as text files. These files are then processed using functions here to store
them in the format appropriate for the stemformatics application.

Note that this data model really just acts as an interface to the atlas data within
the Stemformatics system after it has been deposited here. Most of the data come from the data portal,
and some functions here are designed to check that input data and create local data structures appropriately.
However, this model also interfaces with some Stemformatics application specific data that you will
not find at the data portal: colours for sample types and sample type ordering.

Examle usage:
--------------

from stemformatics_data.atlas import AtlasData

# We need to provide 2 things to instantiate an object here
# 1. redis socket path: this allows the object to fetch data from redis. eg: "/data/redis/redis.sock"
# 2. path to the files which contains the pca coordinates as well as sample annotations specific to the atlas.
# The specific files in this path are expected to be named this way:
#   blood_atlas_coordinates.tsv: tab separated file containing PCA coordinates of the blood atlas.
#   blood_atlas_annotations.tsv: tab separated file containing sample annotations of the blood atlas.
# (substitute "blood" for "myeloid" for the equivalent files for the myeloid atlas)

atlasData = AtlasData('/data/redis/redis.sock', '/var/www/pylons-data/prod/PCAFiles/atlas')
print(atlasData.pcaCoordinates("blood").head())
"""
from . import redisServer
import os, pandas, json, sys

def rankTransform(df):
    """Return a rank transformed version of data frame
    """
    return (df.shape[0] - df.rank(axis=0, ascending=False, na_option='bottom')+1)/df.shape[0]

class AtlasData(object):

    def __init__(self, redisSocketPath, atlasFilePath, atlasType):
        self.redisServer = redisServer(redisSocketPath)
        self.atlasFilePath = atlasFilePath
        self.atlasType = atlasType
        self._pcaCoords = {}
        self._sampleTable = {}

    def pcaCoordinates(self):
        """Return a pandas DataFrame object, specifying the PCA coordinates. The shape of the data frame will be
        number_of_samples x 3, with sample ids as index. There's no guarrantee columns will be named.
        """
        if self.atlasType not in self._pcaCoords:
            self._pcaCoords[self.atlasType] = pandas.read_csv(os.path.join(self.atlasFilePath, "%s_atlas_coordinates.tsv" % self.atlasType), sep="\t", index_col=0)   
        return self._pcaCoords[self.atlasType]

    def expressionValues(self, geneIds):
        """Return expression values of gene ids as a list. Uses values stored in redis for quick access.
        See setExpressionValuesInRedis function below on what key was used to store the values under.
        """
        result = self.redisServer.get("expression|atlas|%s|%s" % (self.atlasType, geneIds[0]))
        return [] if result is None else json.loads(result)

    def expressionTable(self, filtered=False):
        """Return expression matrix as pandas DataFrame. If filtered=False, this is the "full" expression matrix
        that includes all genes, but rank normalised. This method reads the text file on disk rather than using redis values.
        """
        if filtered:
            return pandas.read_csv(os.path.join(self.atlasFilePath, "%s_atlas_expression.filtered.tsv" % self.atlasType), sep="\t", index_col=0)   
        else:
            return pandas.read_csv(os.path.join(self.atlasFilePath, "%s_atlas_expression.tsv" % self.atlasType), sep="\t", index_col=0)   

    def sampleIds(self):
        """Return all sample ids in this atlas as a pandas Series object.
         These should be in the form of "sample_id;dataset_id"
        """
        return self.pcaCoordinates().index

    def sampleTable(self):
        """Return sample annotation table for the atlas as a pandas DataFrame object. The shape of the data frame
        will be number_of_samples x number_of_columns, with sample ids as index.
        """
        if self.atlasType not in self._sampleTable:
            df = pandas.read_csv(os.path.join(self.atlasFilePath, "%s_atlas_annotations.tsv" % self.atlasType), sep="\t", index_col=0) 
            self._sampleTable[self.atlasType] = df.fillna("")
        return self._sampleTable[self.atlasType]

    def geneInfo(self):
        """Return a pandas DataFrame of information about all genes in the atlas, after reading the xxxx_atlas_genes.tsv
        file in the atlas file directory. Ensembl ids form the index.
        """
        return pandas.read_csv(os.path.join(self.atlasFilePath, "%s_atlas_genes.tsv" % self.atlasType), sep="\t", index_col=0)

    def coloursAndOrders(self):
        """Return dictionaries of colours and ordering of sample type items based on "xxxx_atlas_colours.tsv" file inside the
        atlas file directory. Return empty dictionaries if such a file doesn't exist.
        Note that this data does not come here externally from the data portal, but rather are created here within Stemformatics.
        """
        filepath = os.path.join(self.atlasFilePath, "%s_atlas_colours.tsv" % self.atlasType)
        colours, orders = {}, {}
        if os.path.exists(filepath):
            values = {}
            for line in open(filepath).read().split("\n"):
                if line=="": continue   # ignore blanks
                if line.startswith("###"):  # sample type
                    key = line.replace("###","").strip()
                    colours[key] = {}
                    orders[key] = []
                else:   # sample type items
                    if line.startswith("----"):  # blank line, used to create a gap in the display
                        orders[key].append("")
                    else:   # sample type item and colour
                        items = line.split("\t")
                        colours[key][items[0]] = items[1]
                        orders[key].append(items[0])
        return colours, orders
        
    def projection(self, name, testData, includeCombinedCoords=True):
        """Perform projection of testData onto this atlas and return a dictionary of objects.
        Params:
            name (str): Name of the testData, used as prefix for projected points if includeCombinedCoords is True.
            testData (DataFrame): Expression matrix of test data to be projected onto this atlas.
            includeCombinedCoords (bool): Should atlas coords and projected coords be returned together, rather than just projections?

        Returns a dictionary with following keys and values:
            coords (DataFrame): projected coordinates of testData.
            combinedCoords (DataFrame): data frame of atlas coordinates + projected coords if includeCombinedCoords is True.
                The projected points will have index in the format of "{name}_sample1", etc, so that these points
                can be distinguished from atlas points.
            error (str): Error message. Empty string if there was no error.
            name (str): Same as the value used as input.
        """
        result = {"error":"", "coords":pandas.DataFrame(), "name":name, "combinedCoords":pandas.DataFrame()}

        # Some validation before projecting
        if len(testData)==0:
            result["error"] = "Data to project appears to have 0 rows. Check format of the file."

        # Read expression matrix - we only need filtered version. 
        df = self.expressionTable(filtered=True)
        genes = self.geneInfo()

        commonGenes = testData.index.intersection(df.index)  # common index between test and atlas
        if len(commonGenes)==0:
            result["error"] = "No genes common between test data and atlas, likely due to row ids not in Ensembl ids."
            
        elif len(commonGenes)/len(genes[genes["inclusion"]])<0.5:
            result["error"] = "Less than 50% of genes in test data are common with atlas ({} common)".format(len(commonGenes))

        if result["error"]!="":
            return result

        # We reindex testData on df.index, not on commonGenes, since pca is done on df. This means any genes in df not found in 
        # testData will gene None assigned - we will live with this, as long as there aren't so many.
        dfTest = rankTransform(testData.reindex(df.index))
        expression = pandas.concat([df, dfTest], axis=1)

        # perform pca on atlas
        from sklearn.decomposition import PCA
        pca = PCA(n_components=10, svd_solver='full')
        coords = pandas.DataFrame(pca.fit_transform(df.values.T), index=df.columns)  # can also just run fit
        
        # make projection
        result["coords"] = pandas.DataFrame(pca.transform(dfTest.values.T)[:,:3], index=dfTest.columns, columns=['x','y','z'])

        if includeCombinedCoords:   # also return row concatenated data frame of atlas+projection.
            projectedCoords = result['coords']
            projectedCoords.index = ["%s_%s" % (name, item) for item in projectedCoords.index]
            coords = coords.iloc[:,:3]
            coords.columns = projectedCoords.columns
            result['combinedCoords'] = pandas.concat([coords, projectedCoords])

        return result
        
# ----------------------------------------------------------
# Functions to run to check the input data and store this locally in a format that can be accessed by this model. 
# Run from command line to use these functions (after activating the correct environment).
#  eg: > python -m S4M_pyramid.model.stemformatics_data.atlas checkInputFiles /data/redis/redis.sock /var/www/pylons-data/prod/PCAFiles/atlas
# (Note -m flag as well as dots to specify the path and absence of .py. All these ensure that this is read as a module.)
# First argument is the name of the function to run, second is redis socket path, and third is atlas file dir path.
# ----------------------------------------------------------
def checkInputFiles(adata):
    """When expression files are received from external sources, we need to check for consistency.
    """
    df = adata.expressionTable()
    genes = adata.geneInfo()

    # Check that index of genes is the same as the index of df
    assert set(genes.index)==set(df.index)

    # Check that columns of df are all accounted for in annotations
    samples = adata.sampleTable()
    if set(df.columns)!=set(samples.index):
        print("Columns of expression file not matching samples in annotation file. ", adata.atlasType, set(df.columns).difference(set(samples.index)), set(samples.index).difference(set(df.columns)))
        raise AssertionError

def setExpressionValuesInRedis(adata):
    """Use this function to set the expression values in redis using the values in the expression file.
    """
    df = adata.expressionTable()

    # The values in df are rank normalised with all genes included. However we would like to re-do this for the
    # included genes after filtering. Note that inclusion column from genes is a boolean column.
    genes = adata.geneInfo()

    # Save values for filtered out genes
    for index,row in df.loc[genes[~genes["inclusion"]].index].iterrows():
        adata.redisServer.set("expression|atlas|%s|%s" % (adata.atlasType,index), json.dumps(row.tolist()))

    # For genes filtered in, save values after transform
    subset = df.loc[genes[genes["inclusion"]].index]
    subset = (subset.shape[0] - subset.rank(axis=0, ascending=False, na_option='bottom')+1)/subset.shape[0]
    for index,row in subset.iterrows():
        adata.redisServer.set("expression|atlas|%s|%s" % (adata.atlasType,index), json.dumps(row.tolist()))

def saveCoordinateFiles(adata):
    """Use this function to create a PCA coordinate file based on expression matrix.
    Previously this was saved to redis, but since this is small, we will just use text file.
    """
    # Read expression matrix
    df = adata.expressionTable()
    
    # This is the full matrix - need to subset based on inclusion (which is a boolean column from this data frame)
    genes = adata.geneInfo()

    # Rank normalise this subset again, with unwanted genes removed
    df = rankTransform(df.loc[genes[genes["inclusion"]].index])

    # df should already be spearman rank expression, so apply PCA and save to file
    from sklearn.decomposition import PCA
    pca = PCA(n_components=10, svd_solver='full')
    pandas.DataFrame(pca.fit_transform(df.values.T), index=df.columns).\
        to_csv(os.path.join(adata.atlasFilePath, "%s_atlas_coordinates.tsv" % adata.atlasType), sep="\t")

def saveFilteredExpressionFiles(adata):
    """AtlasData.expressionTable() can be slow to run if full expression file is read in. Provide an option to 
    read in filtered expression matrix. 
    """
    df = adata.expressionTable()
    genes = adata.geneInfo()
    df = rankTransform(df.loc[genes[genes["inclusion"]].index])
    df.to_csv(os.path.join(adata.atlasFilePath, "%s_atlas_expression.filtered.tsv" % adata.atlasType), sep="\t")

def changeSampleGroupOrdering(adata):
    """Christine would like to have Cell Type as the first option for the atlas sample types. (June 2020)
    """
    df = adata.sampleTable()
    df = df[["Cell Type", "Sample Source", "Progenitor Type"]]
    df.to_csv(os.path.join(adata.atlasFilePath, "%s_atlas_annotations.tsv" % adata.atlasType), sep="\t")

if __name__ == "__main__":
    if sys.argv[1]=="setExpressionValuesInRedis_myeloid":
        adata = AtlasData(sys.argv[2], sys.argv[3], "myeloid")
        setExpressionValuesInRedis(adata)
    else:
        for atlasType in ["myeloid","blood"]:
            adata = AtlasData(sys.argv[2], sys.argv[3], atlasType)
            if sys.argv[1]=="checkInputFiles":
                checkInputFiles(adata)
            elif sys.argv[1]=="setExpressionValuesInRedis":
                setExpressionValuesInRedis(adata)
            elif sys.argv[1]=="saveCoordinateFiles":
                saveCoordinateFiles(adata)
            elif sys.argv[1]=="saveFilteredExpressionFiles":
                saveFilteredExpressionFiles(adata)
            elif sys.argv[1]=="changeSampleGroupOrdering":
                changeSampleGroupOrdering(adata)

# ----------------------------------------------------------
# Tests. eg: > nosetests -s S4M_pyramid/model/stemformatics_data/atlas.py:test_init
# ----------------------------------------------------------
def _atlasDataForTesting(atlasType):
    return AtlasData('/data/redis/redis.sock', '/var/www/pylons-data/prod/PCAFiles/atlas', atlasType)

def test_init():
    adata = _atlasDataForTesting()
    keys = adata.redisServer.keys("*expression*")
    assert len(keys)>20000
    assert 'expression|atlas|myeloid|ENSG00000166682' in keys
    assert 'expression|atlas|blood|ENSG00000151365' in keys

def test_expressionValues():
    assert round(adata.expressionValues(['ENSG00000000003'])[0],3)==0.087
    assert adata.expressionValues([''])==[]
    expressionValues = adata.expressionValues(["ENSG00000157404"])
    print(len(expressionValues), len(adata.sampleIds()))

def test_pca():
    adata = _atlasDataForTesting()
    print(adata.pcaCoordinates("blood").head())
    print(adata.sampleIds()[:3])

def test_sampleTable():
    adata = _atlasDataForTesting()
    print(adata.sampleTable("blood").head())

def test_geneIds():
    adata = _atlasDataForTesting()
    adata.geneIds("myeloid")

def test_colours():
    adata = _atlasDataForTesting()
    print(adata.coloursAndOrders("blood"))
    print(adata.coloursAndOrders("myeloid"))