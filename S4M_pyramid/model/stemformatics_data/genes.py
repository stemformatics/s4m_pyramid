"""
select gene_biotype, count(gene_id) as count from genome_annotations group by gene_biotype order by count desc;

"""
from . import _runSql
import pandas

class Geneset(object):

    def __init__(self, sqlConnectionPath):
        self.sqlConnectionPath = sqlConnectionPath

        # This is a list of columns from the database which are deemed "most popular"
        self.popularAttributes = ["gene_id","description","associated_gene_name","associated_gene_synonym"]

    def geneFromEnsemblId(self, ensemblId):
        result = _runSql(self.sqlConnectionPath, 
            "select {} from genome_annotations where gene_id=%s".format(",".join(self.popularAttributes)), 
            (ensemblId,))
        return dict(zip(self.popularAttributes, result[0])) if len(result)>0 else None

    def commonlySearchedGenes(self):
        """Return a pandas DataFrame of genes that are commonly searched - ie. without Rik or 
        """
        pass

    def geneInfo(self, queryString, caseSensitive=False, species=""):
        """
        """
        speciesString = ""
        if species.lower()=="mus musculus" or species.lower()=="musmusculus" or species.lower()=="mouse":
            speciesString = " and gene_id~'^ENSMUSG.*'"
        elif species.lower()=="homo sapiens" or species.lower()=="homosapiens" or species.lower()=="human":
            speciesString = " and gene_id~'^ENSG.*'"

        if caseSensitive:
            result = _runSql(self.sqlConnectionPath, 
                "select {0} from genome_annotations where associated_gene_name like %s and gene_biotype='protein_coding'{1}"\
                    .format(",".join(self.popularAttributes), speciesString), ('%' + queryString + '%',))
        else:
            result = _runSql(self.sqlConnectionPath, 
                "select {0} from genome_annotations where lower(associated_gene_name) like %s and gene_biotype='protein_coding'{1}"\
                    .format(",".join(self.popularAttributes), speciesString), ('%' + queryString.lower() + '%',))
        return pandas.DataFrame(result, columns=self.popularAttributes).set_index("gene_id")

