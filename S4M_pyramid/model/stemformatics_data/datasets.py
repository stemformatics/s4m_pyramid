"""
Data model for all datasets in Stemformatics application. Note that this class should eventually replace the model scripts
under model/stemformatics/, which were written long time ago and badly designed.

Example usage:
> import datasets
> ds = datasets.Datasets('postgresql://portaladmin:password@localhost:5432/portal_prod')
> print(ds.sampleInfoFromSampleIdDatasetId(1000))

Usual way to get sqlConnectionPath used to instantiate Datasets object is
> sqlConnectionPath = request.registry.settings.get("psycopg2_conn_string")

"""
from . import _runSql
import pandas

class Datasets(object):

    def __init__(self, sqlConnectionPath):
        self.sqlConnectionPath = sqlConnectionPath
    
    @staticmethod
    def sampleTableKeys():
        """We're only interested in this subset of md_name values from biosamples_metadata table. These will effectively form
        the columns of the samples table.
        """
        return ['Replicate Group ID', 'Sample name', 'Sample name long', 'Sample Type', 'Sample type long', 'Generic sample type', 'Generic sample type long', 'Sample Description', 'Tissue/organism part', 'Parental cell type', 'Final cell type', 'Cell line', 'Reprogramming method', 'Developmental stage', 'Media', 'Disease State', 'Labelling', 'Genetic modification', 'FACS profile', 'Age', 'Sex', 'Organism']
        
    '''
    def sampleFields(self, type="minimal"):
        """Return a list of sample fields eg ["sampleType","genericSampleType",...]
        which are used by this model to interface with the data. Note that these are different to the actual
        fields held in the sql tables, so that change in those tables do not directly affect code downstream.
        type can be used to specify groups of sample fields which belong in the same category.
        """
        if type=="atlas":
            return ["sampleType", "facsProfile"]
        else:
            return ["sampleType"]

    def _sampleSqlField(self, sampleFields):
        """Return the sql fields which maps the sampleFields. 
        """
        mapping = {"sampleType":"Sample Type", "facsProfile":"FACS Profile"}
        return [mapping[item] for item in sampleFields]
    '''
    def sampleInfoFromSampleIdDatasetId(self, sampleIdDatasetId, joinChar=";", fields=["Sample Type", "FACS profile"]):
        """Return a dictionary of sample information keyed on the combination of sample id and dataset id:
            {'GSM2064216;6731': {'Sample Type': 'Clec4e-/- microglia I/R', 'FACS profile': ''}}
        """
        #result = _runSql("select chip_id||'{}'||ds_id as rowId,md_name,md_value from biosamples_metadata where chip_id||';'||ds_id in %s".format(joinChar), 
        #    ((',').join(sdc),))
        result = _runSql(self.sqlConnectionPath, 
                         "select chip_id||'{0}'||ds_id as rowId,md_name,md_value from biosamples_metadata where chip_id||'{0}'||ds_id in %s and md_name in %s"\
                            .format(joinChar), (tuple(sampleIdDatasetId), tuple(fields)))
        dictToReturn = {}
        for row in result:
            if row[0] not in dictToReturn:
                dictToReturn[row[0]] = {}
            dictToReturn[row[0]][row[1]] = row[2] if str(row[2]).lower()!="null" else ""
        return dictToReturn

    def datasetInfo(self, *args, **kwargs):
        """Return a pandas DataFrame containing info about datasets.
        extraColumns (list): subset of ["Title", "Authors", "Description", "PubMed ID", "Contact Name", "Contact Email", "Release Date", "data_type", "Platform"]
        """
        datasetIds = kwargs.get("datasetIds",[])
        queryString = kwargs.get("queryString","")
        extraColumns = kwargs.get("extraColumns",[])
        species = kwargs.get("species","")

        columns = ["datasetId","author","year","pubmedId","platform","numberOfSamples"]  # standard columns that all queries will return
        if len(datasetIds)==0:  # use all dataset ids
            result = _runSql(self.sqlConnectionPath,  "select id,handle,data_type_id,number_of_samples from datasets")
        else:
            result = _runSql(self.sqlConnectionPath,  "select id,handle,data_type_id,number_of_samples from datasets where id in %s", (tuple(datasetIds),))
        df = pandas.DataFrame([[item[0]] + item[1].split("_")[:3] + [item[2]] + [item[3]] for item in result], columns=columns).set_index("datasetId")
        df["platform"] = [{1:"microarray", 2:"RNAseq", 3:"sc-RNAseq"}.get(item, "other") for item in df["platform"]]

        if len(extraColumns)>0 or species!="": # need to make another query on dataset_metadata table
            columnsToQuery = extraColumns
            if species!="":
                columnsToQuery.append('Organism')

            if len(datasetIds)==0:  # use all dataset ids
                result = _runSql(self.sqlConnectionPath,
                                "select ds_id,ds_name,ds_value from dataset_metadata where ds_name in %s", 
                                (tuple(columnsToQuery),))
            else:
                result = _runSql(self.sqlConnectionPath,
                                "select ds_id,ds_name,ds_value from dataset_metadata where ds_id in %s and ds_name in %s",
                                (tuple(datasetIds), tuple(columnsToQuery)))
            for row in result: # join results onto df
                df.at[row[0], row[1]] = "" if str(row[2]).lower()=="null" else row[2]  # first column (row[0]) is ds_id, and the next two columns form key value pair
            #result = _runSql(self.sqlConnectionPath, "select ds_name,ds_value,handle from dataset_metadata join datasets on dataset_metadata.ds_id=datasets.id where ds_id in %s and ds_name in %s", (datasetIds, tuple(columns)))

        if species!="": # filter df on species and drop this column afterwards
            s = df["Organism"].str.lower()
            df = df.loc[s[s==species.lower()].index].drop("Organism", axis=1)

        if queryString!="":   # filter df on queryString
            commonIndex = set()
            for col in df.columns:
                if col=="numberOfSamples": continue  # no need to query this column
                s = df[col].fillna("").str.contains(queryString, case=False, regex=False, na=False)
                commonIndex = commonIndex.union(set(s[s].index))
            df = df.loc[commonIndex]

        return df
    
    def sampleTable(self, *args, **kwargs):
        """Return a pandas DataFrame containing sample information. 
        Example:
        > print(ds.sampleTable(datasetIds=[1000,2000]).shape)
        > (174, 22)
        """
        datasetIds = tuple(kwargs.get("datasetIds",[]))
        result = _runSql(self.sqlConnectionPath, "select * from biosamples_metadata where ds_id in %s", (datasetIds,))
        df = pandas.DataFrame(index=set([item[1] for item in result]))  # empty DataFrame with chip_id as index
        for row in result:
            df.at[row[1], row[2]] = "" if str(row[3]).lower()=="null" else row[3]  # second column (row[1]) is chip_id, and the next two columns form key value pair
        df = df[[col for col in Datasets.sampleTableKeys() if col in df.columns]]
        df.index.name = "sampleId"
        return df
    
    def convertProbeIdsToGeneIds(self, df, geneIdsSubset=None):
        # db_id column in stemformatics.feature_mappings table is related to ensembl version. 
        # By looking at stemformatics.ensembl_versions_gene_mappings table, we can see that db_id=59 corresponds to 
        # Ensembl version 91.
        subquery = "" if geneIdsSubset is None else " and from_id in ('%s')".format("','".join(geneIdsSubset))
        result = _runSql(self.sqlConnectionPath, 
            "select distinct from_id,to_id from stemformatics.feature_mappings where db_id=59 and to_id in ('{}') {} limit 10".format("','".join(df.index.tolist()), subquery))
        genes = pandas.DataFrame(result, columns=["geneId", "probeId"])
        return genes

    def saveToFiles(self, *args, **kwargs):
        """Save expression matrix and sample matrix to files.
        """
        import os

        # Parse input
        datasetId = kwargs.get("datasetId")
        prefix = kwargs.get("prefix")
        outputdir = kwargs.get("outputdir")
        sampleColumns = kwargs.get("sampleColumns")

        # Get expression matrix
        filepath = "/var/www/pylons-data/prod/GCTFiles/dataset%s.gct" % datasetId
        df = pandas.read_csv(filepath, sep="\t", skiprows=2, index_col=0).drop('Description', axis=1)

        # Get sample matrix
        samples = self.sampleTable(datasetIds=[datasetId]).loc[df.columns]
        if sampleColumns is not None: samples = samples[sampleColumns]

        # Save to files
        df.to_csv(os.path.join(outputdir, "%s_expression.tsv" % prefix), sep="\t")
        samples.to_csv(os.path.join(outputdir, "%s_samples.tsv" % prefix), sep="\t")

def test_sampleInfo():
    ds = Datasets("dbname=portal_prod user=portaladmin")
    print(ds.sampleInfoFromSampleIdDatasetId(sampleIdDatasetId=["GSM2064216;6731"]))

def test_datasetInfo():
    ds = Datasets("dbname=portal_prod user=portaladmin")
    assert ds.datasetInfo(datasetIds=[1000]).shape==(1,5)
    assert "HaemAtlas" in ds.datasetInfo(datasetIds=[1000,6000], extraColumns=["Title"]).at[1000,"Title"]
    assert ds.datasetInfo(queryString="Watkins", extraColumns=["Title"]).at[1000,"numberOfSamples"] == 50
    print(ds.datasetInfo(queryString="watkins", extraColumns=["Title"], species="homo sapiens"))

def test_sampleTable():
    ds = Datasets("dbname=portal_prod user=portaladmin")
    print(ds.sampleTable(datasetIds=[1000,2000]).shape)
    print(ds.sampleTable(datasetIds=[2000]).shape)

def test_convertProbeIdsToGeneIds():
    ds = Datasets("dbname=portal_prod user=portaladmin")
    df = pandas.read_csv("/var/www/pylons-data/prod/GCTFiles/dataset1000.gct", sep="\t", skiprows=2, index_col=0).drop('Description', axis=1)
    print(df.index[:3].tolist())
    print(ds.convertProbeIdsToGeneIds(df))

def saveExampleFilesForAtlasProjection():
    # Used to save example tsv expression and sample files for projecting onto the atlas
    ds = Datasets("dbname=portal_prod user=portaladmin")
    ds.saveToFiles(datasetId=7283, prefix="notta", outputdir="/var/www/pylons-data/prod/PCAFiles/atlas", sampleColumns=["Sample Type","Parental cell type"])

def saveFiles_7172():
    # Used once to save dataset files
    ds = Datasets("dbname=portal_prod user=portaladmin")
    ds.saveToFiles(datasetId=7172, prefix="Chung", outputdir="/tmp")
