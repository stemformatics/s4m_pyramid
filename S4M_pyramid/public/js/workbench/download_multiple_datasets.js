$(document).ready(function() {
    search_value = $('#search_value').html();
    if (search_value == 'False'){
        $.ajax({
            'url': '/workbench/download_multiple_datasets_ajax_call',
            success: function(response){
                all_sample_metadata_json = $.parseJSON(response);
            },
            error: function(response){
                all_sample_metadata_json = 'None';
            }
        }
        )
    }

    var params_url = window.location.search.substring(1);
    if (params_url != "") {
        var param_array = params_url.split("&");
        var filter_param = param_array[0].split("=")[1].replace('+',' ');
        var filed_param = param_array[1].split("=")[1];    
        $("#filter").val(filter_param)
        $("#field").val(filed_param)  
    }


    


    $('tr.sample').removeClass('hidden').hide();
    $('a.toggle_samples').click(function(){
        ds_id = $(this).attr('data-id');
        $('tr.samples_of_'+ds_id).toggle();  
    });

    $('th.toggle_select_all').unbind('click');
    $('th.toggle_select_all').click(function(){
        number_selected = $('div.dataset_results input:checked').length;

        if (number_selected == 0){
            // if none selected, then select all
            $('div.dataset_results input').prop('checked',true);


        } else {
            // if one or more selected, then select none
            $('div.dataset_results input').prop('checked',false);

        }

        
    });
    $('a.searchButton').click(function(){
        $('#searchForm').submit();
    });
    $('a.export').click(function(){
        var ds_ids = Array();
        $('div.dataset_results input:checked').each(function(){
            ds_id = $(this).attr('name');
            ds_ids.push(ds_id); 
        });
        url = $(this).attr('href');
        url_ds_ids_text = ds_ids.join(',');
        window.location = url + '&ds_ids=' + url_ds_ids_text;
        return false;
    });

     ds_table = $('#datasets_summary_table').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "aaSorting": [[ 0, "asc" ]],
        "bAutoWidth": false,
        "order":[[3,'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
          ],
        "oLanguage": {
            "sSearch": "Filter: "
        } } );

    $("#datasets_summary_table td").click(function() {
        var tr_id = $(this)[0].parentElement.id
        $(this).closest('span').css("visibility","visible");
    });
    temp_all_dataset_metadata_json = $('#all_dataset_metadata_json').html();
    if (search_value == 'True'){
        temp_all_sample_metadata_json = $('#all_sample_metadata_json').html();
        all_sample_metadata_json = $.parseJSON(temp_all_sample_metadata_json);
    }

    all_dataset_metadata_json =$.parseJSON(temp_all_dataset_metadata_json);

    ($("#reset")).insertAfter('.search_box');

    
});

function show_summary_of_dataset(ds_id) {
        var chosen_dataset_metadata_json = all_dataset_metadata_json[ds_id];
        var sample_type_length =chosen_dataset_metadata_json['sample_type_order'].split(",").length;
        if (typeof(all_sample_metadata_json[ds_id]) == "undefined") {
            alert('Please wait sample metadata is loading');
        }
        var chosen_sample_metadata_json = all_sample_metadata_json[ds_id];
        temp_sample_data_for_ds_id = {};
        for(i=0;i<chosen_sample_metadata_json.length;i++) {
            sample_id = chosen_sample_metadata_json[i][2];
            name = chosen_sample_metadata_json[i][3];
            value = chosen_sample_metadata_json[i][4];
            if (value == "null" || value == "NULL") {
                value = '';
            }
            if (!(sample_id in temp_sample_data_for_ds_id)){
                temp_sample_data_for_ds_id[sample_id] = {};
            }
            temp_sample_data_for_ds_id[sample_id][name] = value;
        }

        // now lets create modal
        $('<div id="mdd_modal" ><a class="modalCloseImg simplemodal-close" title="Close"></a><div id="wb_modal_title" class="wb_modal_title">'+chosen_dataset_metadata_json['handle'].split('_')[0]+' Dataset</div><div id="menus"><div id="main_headers"><div class="header"><a class="selected" id="dataset_summary_modal">Dataset Summary</a></div><div class="header" ><a id="sample_summary_modal">Sample Summary</a></div></div></div><div>Click <a target="_blank" href="/datasets/search?ds_id='+ds_id+'"class="link">here</a> to view Dataset Summary Page</div><div></br><a id="export_sample_summary_button" class="link">Download all Samples</a></div></br><table id="table_dataset_summary" style="display:inline-table"><tbody><tr><td>Total samples</td><td>'+chosen_dataset_metadata_json['number_of_samples']+'</td></tr><tr><td>Species</td><td>'+chosen_dataset_metadata_json['organism']+'</td></tr><tr><td>Platform</td><td>'+chosen_dataset_metadata_json['data_type_id']+'</td></tr><tr><td>Sample Types ('+sample_type_length+')</td><td>'+chosen_dataset_metadata_json['sample_type_order']+'</td></tr></tbody></table> <table id="table_sample_summary" style="display:none"></table></div>').modal();

        var list_of_headers = ['Chip ID','Replicate Group ID','Sample Description','Organism','Sample Type','Final cell type','Parental cell type','Sex','Cell Type','Labelling']

        // create header row
        var header_row = '<thead>'
        for (var j=0; j< list_of_headers.length;j++) {
                header_row += '<th>'+list_of_headers[j]+'</th>'
            }
        header_row += '</thead>'
        var row = '<tbody>'

        // create sample table rows
        for (key in temp_sample_data_for_ds_id) {
            row += '<tr>';
            row += '<td>'+key+'</td>'
            for (var j=1; j< list_of_headers.length;j++) {
                row += '<td>'+temp_sample_data_for_ds_id[key][list_of_headers[j]]+'</td>'
            }
            row += '</tr>';
            
        } 
        row += '</tbody>'      
         $("#table_sample_summary").append(header_row);
         $("#table_sample_summary").append(row);

    $("#sample_summary_modal").click(function(){
        $("#table_sample_summary").css("display","inline-table");
        $("#table_dataset_summary").css("display","none");
        $("#table_sample_summary_wrapper").css("display","inline-table");
        $("#sample_summary_modal").addClass("selected");
        $("#dataset_summary_modal").removeClass("selected");
    });


    $("#dataset_summary_modal").click(function(){
        $("#table_sample_summary").css("display","none");
        $("#table_sample_summary_wrapper").css("display","none");
        $("#table_dataset_summary").css("display","inline-table");
        $("#sample_summary_modal").removeClass("selected");
        $("#dataset_summary_modal").addClass("selected");

    });

    $('#export_sample_summary_button').click(function(){
        $('#table_sample_summary').table2CSV();
    });

      ds_table = $('#table_sample_summary').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "aaSorting": [[ 0, "asc" ]],
        "bAutoWidth": false,
        "oLanguage": {
            "sSearch": "Filter: "
        } } );

      $("#table_sample_summary_wrapper").css("display","none");

    }
