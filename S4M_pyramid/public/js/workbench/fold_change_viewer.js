
function reset_table() {
    
    var sample_type_1 = $('#sample_type_1').val();
    var sample_type_2 = $('#sample_type_2').val();
    
    var json_view_data = $('#json_view_data').html();
    
    var view_data = jQuery.parseJSON(json_view_data);
    
    var probe_list = view_data.raw_probe_list;
    var is_log_2 = view_data.log_2;    

    for ( var i = 0; i < probe_list.length; i++){
        var probe = probe_list[i];
        
        value1 = view_data.plot_data[sample_type_1]['values'][i]['average'];
        value2 = view_data.plot_data[sample_type_2]['values'][i]['average'];
        
        if (is_log_2){
            fold_change = Math.pow(2,value2 - value1);
        } else {
            fold_change = value2 / value1;
        }
        row_count = i + 2;
        
        $('#fold_change_output_table tr:eq('+row_count+') td:eq(1)').html(value1);
        $('#fold_change_output_table tr:eq('+row_count+') td:eq(2)').html(fold_change);
        $('#fold_change_output_table tr:eq('+row_count+') td:eq(3)').html(value2);
        
        
    } 

    
}

function exportTableToCSV($table, filename) {

    var $rows = $table.find('tr:has(td)'),

      // Temporary delimiter characters unlikely to be typed by keyboard
      // This is to avoid accidentally splitting the actual contents
      tmpColDelim = String.fromCharCode(11), // vertical tab character
      tmpRowDelim = String.fromCharCode(0), // null character

      // actual delimiter characters for CSV format
      colDelim = '","',
      rowDelim = '"\r\n"',

      // Grab text from table into CSV formatted string
      csv = '"' + $rows.map(function(i, row) {
        var $row = $(row),
          $cols = $row.find('td');

        return $cols.map(function(j, col) {
          var $col = $(col),
            text = $col.text();

          return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

      }).get().join(tmpRowDelim)
      .split(tmpRowDelim).join(rowDelim)
      .split(tmpColDelim).join(colDelim) + '"';

    // Deliberate 'false', see comment below
    if (false && window.navigator.msSaveBlob) {

      var blob = new Blob([decodeURIComponent(csv)], {
        type: 'text/csv;charset=utf8'
      });

      // Crashes in IE 10, IE 11 and Microsoft Edge
      // See MS Edge Issue #10396033
      // Hence, the deliberate 'false'
      // This is here just for completeness
      // Remove the 'false' at your own risk
      window.navigator.msSaveBlob(blob, filename);

    } else if (window.Blob && window.URL) {
      // HTML5 Blob        
      var blob = new Blob([csv], {
        type: 'text/csv;charset=utf-8'
      });
      var csvUrl = URL.createObjectURL(blob);

      $(this)
        .attr({
          'download': filename,
          'href': csvUrl
        });
    } else {
      // Data URI
      var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

      $(this)
        .attr({
          'download': filename,
          'href': csvData,
          'target': '_blank'
        });
    }
  }


$(document).ready(function() {


    
    $('select').change(function(){
        
        
        reset_table();
        
        
        
    });

    $('#exportTableCSVButton').click(function(){
		$('#fold_change_output_table').table2CSV();	
	});
    
    
    reset_table();
    document.getElementById("sample1_value").innerHTML = $("#sample_type_1").val();
    document.getElementById("sample2_value").innerHTML = $("#sample_type_2").val();

     document.getElementById("sample_type_1").onchange=function() {
        var sample1 = this.value;
        document.getElementById("sample1_value").innerHTML = sample1;
     }

     document.getElementById("sample_type_2").onchange=function() {
        var sample2 = this.value;
        document.getElementById("sample2_value").innerHTML = sample2;
     }
    
});
