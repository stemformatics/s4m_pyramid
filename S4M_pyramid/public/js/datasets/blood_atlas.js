// http://stackoverflow.com/questions/486896/adding-a-parameter-to-the-url-with-javascript
function changeUrl(value,key) {
  key = encodeURI(key); value = encodeURI(value);
  var kvp = document.location.search.substr(1).split('&');
  var i=kvp.length; var x; while(i--)  {
      x = kvp[i].split('=');
      if (x[0]==key)  {
          x[1] = value;
          kvp[i] = x.join('=');
          break;
      }
  }
  if(i<0) {kvp[kvp.length] = [key,value].join('=');}

  window.history.pushState({}, '',"?"+kvp.join('&'))
}

$(document).ready(function(){
  $("#plot_type").on('change', function(event) {
        var dropdown_value = $("#plot_type").val().replace(/ /g,"_");
        changeUrl(dropdown_value,'plot_type');
   })
  click_share_link();

})


function click_share_link(){
    var elements = $('#share_link,a.share_link');
    elements.unbind().click(function(){

        value = check_user_logged_in_for_sharing(); // this is in main.js
        if (value == false){ return false; }

        var gene_set_id_element = $('#gene_set_id');
        if (gene_set_id_element.length ==0){
            gene_set_id = 0;
        } else {
            gene_set_id = gene_set_id_element.html();
        }

        $('#wb_modal_title').html('Share this graph');
        var form_html  = "<form id='share_link_form'>" +
                "<div id=help_share_link_form>Please note that you can add more than one email address separating each with commas and no spaces. Please be aware that this does not share private objects, such as datasets or gene lists. If the email address you use doesn't have access, they will not be able to view this graph.</div>" +
                "<dl>" +
                    // "<dt>From Name:</dt><dd><input class='share_input' type='text' name='from_name' value='"+$('#full_name').html()+"'/></dd>" +
                    "<dt id='to_email_label'>To Email:</dt><dd><input id='to_email_input' class='share_input' type='text' name='to_email' value=''/></dd>" +
                    "<dt>Subject:</dt><dd><input class='share_input' type='text' name='subject' value='"+SITE_NAME+" - " + "Blood Atlas" +"'/></dd>" +
                    "<dt>Body:</dt><dd><textarea class='share_input' name='body' >Here is a link I thought you might want to see:\r\n"+ window.location.href +"\r\n\r\n"+"From \""+$('#full_name').html()+"\" " + $('#user').html() +" via "+SITE_NAME+"</textarea></dd>" +
                    "<dt><button id='share_link_form_submit' type='button'>Submit</button></dt><dd></dd>" +
                "</dl>" +
                "<div class='clear'/>"+
                "<input type='hidden' name='ds_id' value="+"1000"+">"+

                "<input type='hidden' name='gene_set_id' value="+gene_set_id+">"+
            "</form>" +

            "<div class='clear'/>";

        $('#wb_modal_content').html(form_html);

        $('#modal_div').modal({
            minHeight: 480,
            /* minWidth: 400, */
            onShow: function (dialog) {
                var modal = this;
                $('#share_link_form_submit').unbind('click');
                $('#share_link_form_submit').click(function(e){
                    // stop things from happening first
                    e.preventDefault();

                    $(this).html('Sharing...').addClass('wait');
                    $('#simplemodal-container').addClass('wait');

                    var post_url = BASE_PATH + 'auth/share_gene_expression';
                    $.post(post_url, $("#share_link_form").serialize(),
                        function(data) {
                            $('#wb_modal_title').html('Share this graph');
                            $('#wb_modal_content').html(data);
                            $('#simplemodal-container').height('auto').removeClass('wait');
                        }
                    );

                });
            }
        });
    });

}
