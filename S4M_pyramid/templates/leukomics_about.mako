<%namespace name="Base" file="common.mako"/>

<!DOCTYPE html>
<html lang="en">
<head>
${Base.header_elements()}

<title>About Leukomics</title>    
<style>
.content {
    font-family: arial, sans-serif;
}
.content a {
    color: #0080ff;
}
.pagetitle {
    font-family: Avenir,Helvetica,Arial,sans-serif;
    font-size: 2rem;
    margin: auto;
    width: 350px;
    color: rgb(235, 149, 52);
}
div p, div ul {
    font-size:14px;
    line-height:1.3em;
    color:#787878;
    margin-top:10px;
}
div ul {
    list-style-type: inherit;
    padding-left:15px;
}
div.hover {
    position:absolute;
    top:130px;
    left:80px;
    width:120px;
    padding:15px;
    font-size:14px;
    color:black;
    background:#F99820;
    line-height:1.3em;
    opacity:0.8;
    border-radius:10px;
}
</style>

</head>

<body>
${Base.banner()}
<br><br><br><br>

<div class="content" v-cloak>
    <div style="display:flex; width:1000px; margin:auto;">
        <div style="margin:auto; flex:70%; padding:20px;">
            <h2 class="pagetitle" style="">About Leukomics</h2>
            <p>Leukaemias arise from normal blood cells that have gained the ability for uncontrollable self-renewal and defective differentiation. 
            Haematopoietic stem cells (HSCs) self-renew and differentiate into all blood lineages. Leukaemic stem cells (LSCs) share the capacity 
            for the extensive self-renewal and survival with normal HSCs, but instead of differentiating into blood lineages they give rise to leukaemia.</p>
            <p>Chronic myeloid leukaemia (CML) is caused by the constitutive expression of the chimeric oncogene BCR-ABL1 in HSCs. 
            Huge improvement in patient care was achieved when BCR-ABL1 tyrosine kinase inhibitors were discovered. 
            However, resistance in those patients is still an issue, and therefore the development of novel therapies specifically targeting the LSC, 
            which are responsible for relapse, is very important.</p>
            <p>Acute myeloid leukaemia (AML) is the most common form of adult leukaemia and also the most aggressive haematological malignancy. 
            It is frequently associated with relapse which is usually fatal. The standard of care in AML patients has not improved outcomes in decades. 
            Childhood and adult AML have very different survival rates and also molecular signatures. 
            Understanding age-specific biology and molecular features of AML is important to provide patients with adequate and 
            most appropriate targeted treatments.</p>
            <p>As we learn more about the molecular changes that drive cancer, so our power to combat it grows. 
            Researchers of many cancer types aspire to build our understanding of these drivers and use this knowledge to create targeted therapies. 
            High-throughput technologies, which are becoming ever more powerful, have the potential to lead us to a truer picture of 
            the molecular features of cancer cells. At Leukomics we believe that the ground breaking leukaemia research happening 
            across the world could be enhanced by use of a growing collection of high throughput data.</p>
            <p>Our goal is to bring these datasets together and create tools to facilitate in-depth analysis of the wealth of 
            information that resides within them. The Leukomics datasets detail gene expression in sample types relating to key areas of 
            modern AML and CML research. These include:</p>
            <ul>
            <li>Clinical parameters – such as response to treatment and aggressiveness of disease</li>
            <li>Stem cells – from AML and CML patients and healthy individuals</li>
            <li>Disease stage – chronic, accelerated and blast phases</li>
            <li>Drug treatments – response to drugs such as tyrosine kinase inhibitors</li>
            <li>Mouse models – stem and progenitor cells from mouse models of AML and CML</li>
            <li>Age – paediatric and adult AML</li>
            </ul>
        </div>
        <div style="flex:30%; padding:20px; background:lightyellow; border-radius:10px;">
            <h2 class="pagetitle" style="">Publications</h2>
            <p>Leukaemias arise from normal blood cells that have gained the ability for uncontrollable self-renewal and defective differentiation. 
            Haematopoietic stem cells (HSCs) self-renew and differentiate into all blood lineages. Leukaemic stem cells (LSCs) share the capacity 
            for the extensive self-renewal and survival with normal HSCs, but instead of differentiating into blood lineages they give rise to leukaemia.</p>
        </div>
    </div>
</div>

${Base.footer()}

<script>
var vm = new Vue({
    el: '.content',
    data: {	
    },
})
</script>

</body>
</html>