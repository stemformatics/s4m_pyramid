<%namespace name="Base" file="common.mako"/>

<!DOCTYPE html>
<html lang="en">
<head>
${Base.header_elements()}

<title>Leukomics at Stemformatics</title>    
<style>
.content {
    font-family: arial, sans-serif;
}
.content a {
    color: #0080ff;
}
.pagetitle {
    font-family: Avenir,Helvetica,Arial,sans-serif;
    font-size: 2rem;
    margin: auto;
    width: 350px;
    color: rgb(235, 149, 52);
}
div.info {
    width:200px;
    padding:15px;
    font-size:14px;
    color:white;
    background:#68228B;
    line-height:1.3em;
    opacity:0.8;
    border-radius:10px;
}
div.info a {
    color: #ffffd2;
}
div.info a:hover {
    color: white;
}
div.hover {
    position:absolute;
    top:130px;
    left:80px;
    width:120px;
    padding:15px;
    font-size:14px;
    color:black;
    background:#F99820;
    line-height:1.3em;
    opacity:0.8;
    border-radius:10px;
}
</style>

</head>

<body>
${Base.banner()}
<br><br><br><br>

<div class="content" v-cloak>
    <div style="display:flex; width:1000px; margin:auto;">
        <div style="margin:auto; flex:70%; text-align:center; position:relative; display:table">
            <h2 class="pagetitle" style="">Leukomics</h2>
            <div v-if="selectedInfo=='about'" class="hover">
            Read more about leukaemia and Leukomics. Access publications.
            </div>
            <div v-if="selectedInfo=='expressionDatasets'" class="hover">
            Show the list of datasets within Stemformatics which have been curated to be included in Leukomics.
            </div>
            <div v-if="selectedInfo=='molecularProfiles'" class="hover">
            Show the genes and gene sets implicated in leukaemias and follow links to visualise their expression in Stemformatics datasets.
            </div>
            <div v-if="selectedInfo=='atlas'" class="hover">
            Access the integrated blood atlas, which brings together multiple gene expression datasets
            to model normal human haematopoiesis.
            </div>
            <div style="position:absolute; top:80px; left:440px;">
                <a href="/leukomics_about" @mouseover="selectedInfo='about'" @mouseleave="selectedInfo=null">About leukomics</a>
            </div>
            <div style="position:absolute; top:400px; left:20px;">
                <a href="/datasets/search?filter=leukomics" @mouseover="selectedInfo='expressionDatasets'" @mouseleave="selectedInfo=null">Expression datasets</a>
            </div>
            <div style="position:absolute; top:400px; left:600px;">
                <a href="/leukomics_genes" @mouseover="selectedInfo='molecularProfiles'" @mouseleave="selectedInfo=null">Molecular profiles</a>
            </div>
            <div style="position:absolute; top:370px; left:310px;">
                <a href="/atlas?type=blood" @mouseover="selectedInfo='atlas'" @mouseleave="selectedInfo=null">Integrated blood atlas</a>
            </div>
            <img style="margin-top:20px;" src="/static_img/leukomics_main.png">
        </div>
        <div style="flex:20%; padding-top:80px;">
            <div class="info">
            <p>Leukomics is a data portal within Stemformatics 
            that brings together a collection of gene expression datasets relevant to leukaemia research. It also represents a collaboration between the 
            <a href="https://biomedicalsciences.unimelb.edu.au/sbs-research-groups/department-of-anatomy-and-neuroscience/wells-laboratory-stem-cell-systems" target="_blank">Wells Lab</a>
            and the <a href="https://www.gla.ac.uk/researchinstitutes/cancersciences/research/units/paulogormanleukaemiaresearchcentre/" target="_blank">
            Paul O'Gorman Leukaemia Research Centre</a> at the University of Glasgow.</p>
            <p style="margin-top:10px;">Our goal is to bring these datasets together and create tools, such as our <a href="/atlas?type=blood">integrated blood atlas</a>,
            to facilitate in-depth analysis of the wealth of information that resides within them.</p>
            </div>
        <!--
            <p style="background:lightyellow; padding:20px;">Leukaemias arise from normal blood cells that have gained the ability for uncontrollable self-renewal and defective differentiation. 
            Haematopoietic stem cells (HSCs) self-renew and differentiate into all blood lineages. Leukaemic stem cells (LSCs) share the capacity 
            for the extensive self-renewal and survival with normal HSCs, but instead of differentiating into blood lineages they give rise to leukaemia.</p>
            <p style="background:lightyellow; padding:20px;">Chronic myeloid leukaemia (CML) is caused by the constitutive expression of the chimeric oncogene BCR-ABL1 in HSCs. 
            Huge improvement in patient care was achieved when BCR-ABL1 tyrosine kinase inhibitors were discovered. 
            However, resistance in those patients is still an issue, and therefore the development of novel therapies specifically targeting the LSC, 
            which are responsible for relapse, is very important.</p>
            <p style="background:lightyellow; padding:20px;">Acute myeloid leukaemia (AML) is the most common form of adult leukaemia and also the most aggressive haematological malignancy. 
            It is frequently associated with relapse which is usually fatal. The standard of care in AML patients has not improved outcomes in decades. 
            Childhood and adult AML have very different survival rates and also molecular signatures. 
            Understanding age-specific biology and molecular features of AML is important to provide patients with adequate and 
            most appropriate targeted treatments.</p>
        -->
        </div>
    </div>
</div>

${Base.footer()}

<script>
var vm = new Vue({
    el: '.content',
    data: {	
        selectedInfo: null,
    },
})
</script>

</body>
</html>