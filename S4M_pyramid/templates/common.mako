<!-- 
     This template contains html the elements which are shared by many templates, so we can keep these in one place. 
     In the long-term, we can use this file to build any new functionality into the site, and refactor old/unused legacy code. 
 -->

<!-- Elements that should be included in all pages. -->
<%def name="header_elements()">

    <!-- This base title was previosuly generated using a c-variable. -->
    <meta name="google-site-verification" content="qwBqw8TJZD8h6pJpWNUwA7YisNdxtHbLLWkeN5A_0Js" />
	
    <!-- <meta http-equiv="content-language" content="en-us" /> -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Centre for Stem Cell Systems, The University of Melbourne" />
    <meta name="description" content="Gene expression data portal containing curated datasets across a comprehensive list of tissues and cell types." />
    <meta name="keywords" content="gene expression, stem cells, bioinformatics, blood cells, genomics" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Task #426 Safari iPhone issue -->
    <meta name="format-detection" content="telephone=no"/>

    <!-- The following CSS will need to be manually referenced to work with the environment on which it is located. -->
    <link href="/static_css/screen.css" type="text/css" rel="stylesheet"/>
    <link href="/static_css/v-tooltip.css" type="text/css" rel="stylesheet"/>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/js/vuejs/v-tooltip.min.js"></script>
    <script>
    Vue.use(VTooltip);
    </script>


	<script type="text/javascript" >

      var _gaq = _gaq || [];

      _gaq.push(['_setAccount', 'UA-20041624-1']);  // The tracking id is obtained from the analytics site. Previously this was stored in config. (See old base.mako file)
      _gaq.push(['_setDomainName', 'none']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
 

</%def>
<!-- End common_elements -->

<!-- banner area for each page. Required variables from python:
user: {'user':'guest@stemformatics.org',...}
-->
<%def name="banner()">
    <div id="header">
        <div class="limitedHeader">
            <a class="logo" href="/" data_hover_text="Click for home page"></a>
            <div v-if="user.user==''" class="rightHeader">
                <div class="login-tasks">
                    <a class="no_padding_right" href="/auth/login">Login</a><br/>
                    <a class="no_padding_right" href="/auth/register">Register</a><br/>
                    <a href="/auth/forgot_password">Forgot pass phrase</a>
                </div>
            </div>
            <div v-if="user.user!=''" class="rightHeader logged_in">
                <div class="user-info">
                    <div class="full_name" id="full_name">{{user.full_name}}</div>
                </div>
                <div class="login-tasks">
                    <a id="logout_link" href="/auth/logout">Logout </a>|
                    <a href="/auth/history/show">History</a>|
                    <a class="no_padding_right" href="/auth/update_details">My account</a><br/><a href="/ensembl_upgrade/index">Ensembl upgrade</a>|
                    <a class="no_padding_right" href="/auth/show_private_datasets">My datasets</a>
                </div>
            </div>
            <!-- Help icon -->
            <div class="help_icon"><a href="#" onclick="showHelpPage(); return false;"><img src="/images/icons/help_icon.png"/></a></div>
        </div>

        <!-- Header menu options/buttons. -->
        <div id="menus">
            <div id="main_headers" >
                <div class="header"><a id="header-genesearch-button" href="/genes/search">GENES ></a></div>
                <div class="header"><a id="header-datasetsearch-button" href="/datasets/search">DATASETS ></a></div>
                <div class="header"><a id="header-graphs-button" href="/expressions/index">GRAPHS ></a></div>
                <div class="header"><a id="header-analyses-button" href="/workbench/index">ANALYSES ></a></div>
                <div class="header"><a id="header-analyses-button" href="/workbench/jobs_index">MY JOBS ></a></div>
                <div class="header"><a id="header-about-us-button" href="/contents/about_us">ABOUT US ></a></div>
                <div class="header"><a id="header-analyses-button" href="/contents/faq#faq">FAQ ></a></div>
            </div>
        </div>
    </div>
    
    <!-- logged in user -->
    <%
    from S4M_pyramid.views import user_views
    user = user_views.currentUser(request)
    import json
    %>
    <script>
    var vueHeader = new Vue({
        el: '#header',
        data: {	
            user: ${json.dumps(user) |n}
        },
        mounted() {
        }
    });
    </script>

    <!-- consent banner -->
    <div class="consent_message" id="consent_message" style="display:none">
        <a class="modalCloseImg simplemodal-close" title="Close" onclick="get_user_cookie_consent()"></a> 
        The site uses cookies to deliver our services. By using our site, you acknowledge that you have read and understood our 
        <a id="consent_message_a" href="/contents/disclaimer">Privacy policy</a>.          
    </div>
    <script>
    function get_user_cookie_consent() {
        localStorage.setItem("user_consent","yes");
        document.getElementById("consent_message").style.display='none';
    }
    if (localStorage.getItem("user_consent")!="yes")
        document.getElementById("consent_message").style.display='inherit';
    </script>

    <!-- modal dialog component -->
    <script type="text/x-template" id="modal-template">
        <div class="">
            <slot name="content">
            default content
            </slot>
        </div>
    </script>
    <script>
    Vue.component("modal", {
        template: "#modal-template",
    });
    </script>
</%def>


<!-- Begin Footer -->
<%def name="footer()">
    <%
    import socket
    from S4M_pyramid.lib.deprecated_pylons_globals import config
    version = config['stemformatics_version']
    hostname = socket.gethostname()
    %>
    <div class="clear"></div>
        <div id="footer">
            <div class="footer_central">
                <div class="leftfooterText">
                    <span>© 2010-2020</span>
                    <span>${version}</span>
                    <span><a href="/contents/disclaimer">Disclaimer</a></span>
                    <span><a href="/contents/disclaimer">Privacy Policy</a></span>
                    <span>${hostname}</span>
                </div>
                <div class="rightfooterText">
                    <span class="website">Website hosted by </span>
                    <a class="nectar" href="http://nectar.org.au" target="_blank"></a>
                </div>
                <div id='guide-wrapper'></div>
                <div id='generic_tooltip'></div>
            </div>
        </div>
    </div>
</%def>
<!-- End Footer -->
