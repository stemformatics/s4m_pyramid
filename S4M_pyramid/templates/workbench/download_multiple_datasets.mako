<%inherit file="../default.html"/>
<%namespace name="Base" file="../base.mako"/>

<%def name="includes()">
    <script type="text/javascript" src="${h.url('/js/workbench/download_multiple_datasets.js')}"></script>
    <link rel="stylesheet" type="text/css" href="${h.url('/css/workbench/download_multiple_datasets.css')}" >
</%def>

<div class="content">
    <div id="search_value" class="hidden" >${c.search_value}</div>
    <div id="all_dataset_metadata_json" class="hidden" >${c.detailed_datasets_summary_table_json}</div>
    <div id="all_sample_metadata_json" class="hidden" >${c.all_samples_by_ds_id_json} </div>

    <div class="landing_page_analyses" >
        <div class="hidden" id="show_help_flag">NO</div>
        <div class="clear"></div>
   
    </div>

    <div class="title">
        Multi Dataset Downloader
    </div>

    <div class="basic_help">
        Use this page to download data from one or more datasets in Stemformatics. You can download sample data as well as expression data files. Use the search term to narrow down your list, then use the checkboxes to select datasets of interest before using one of the download buttons.
    </br>
    </br>
        You can restrict the search term to be just one column of the table by using the search field option. Note that when search field is "all", it extends the search to all sample data.
    </div>
    

    <div id="mdd_search_form_div">
        <form id="mdd_search_form">
            Search term:
            <input type="text" name="filter" class="form_fields" id="filter" placeholder="enter a search term" >
            Search field:
            <select name="field" class="form_fields" id="field">
                <option value="">all</option>
                <option value="Description">description</option>
                <option value="Organism">species</option>
                <option value="project">project</option>
                <option value="Platform">platform</option>
                <option value="sampleTypeDisplayOrder">sample types</option>
            </select>
            <input type="submit" name="submit">
            <div class="generic_orange_button" id="reset"><a href="${h.url('/workbench/download_multiple_datasets')}">Reset</a></div>

        </form>

    </div>

    
    <div class="export_buttons">
        <div class="generic_orange_button metadata_dataset_export"><a class="export" href="${h.url('/workbench/download_multiple_datasets?filter=')}${c.search}&export=datasets">Export selected dataset metadata</a></div>
        <div class="generic_orange_button metadata_sample_export margin_bottom_large"><a class="export" href="${h.url('/workbench/download_multiple_datasets?filter=')}${c.search}&export=samples">Export selected sample metadata</a></div>
        <div class="generic_orange_button margin_bottom_large script_to_download"><a class="export" href="${h.url('/workbench/download_multiple_datasets?filter=')}${c.search}&export=download_script">Export download script for selected datasets</a></div>
        <div class="generic_orange_button probe_mappings"><a href="${h.url('/contents/download_mappings')}">Link to probe mappings</a></div>
    </div>
    

    
    %if c.detailed_datasets_summary_table is not None:
        <% number_of_datasets = len(c.detailed_datasets_summary_table) %>  
        <div class="title">${number_of_datasets} datasets out of ${c.total_datasets}</div>
        <div class="clear"></div>
        <div class="dataset_results">
        <table cellpadding="1" cellspacing="1" border="1" id="datasets_summary_table" >
            
            <thead>
            <tr>
                <th class="toggle_select_all">all / none</th>
                <th>Name</th>
                <th>Description</th>
                %if c.sample_count is not None:
                    <th>Matched/Total Samples</th>
                %else:
                    <th>Total Samples</th>
                %endif
                <th>Sample Types</th>
                <th>Platform</th>
                <th>Project</th>
                <th>Species</th>              
                <th>External links</th>
            </tr>
            </thead>
            <tbody>
            % for ds_id in c.detailed_datasets_summary_table:
                <tr id=${ds_id}>
                    <td><input name="${ds_id}" type="checkbox" checked></input></td>
                    <td onclick="show_summary_of_dataset(${ds_id})" class="name-dataset"> ${c.detailed_datasets_summary_table[ds_id]['handle'].split("_")[0]}  
                    </td>
                    <td>
                    %if len(c.detailed_datasets_summary_table[ds_id]['description']) > 150:
                        <% desc = c.detailed_datasets_summary_table[ds_id]['description'][0:100] %>
                        ${desc} ...
                    %else:
                        ${c.detailed_datasets_summary_table[ds_id]['description']}
                    %endif
                    </td>
                    <td>
                        %if c.sample_count is not None:
                            %if ds_id in c.sample_count:
                                ${c.sample_count[ds_id]}/${c.detailed_datasets_summary_table[ds_id]['number_of_samples']}
                            %else:
                                1/${c.detailed_datasets_summary_table[ds_id]['number_of_samples']}
                            %endif
                        %else:
                            ${c.detailed_datasets_summary_table[ds_id]['number_of_samples']}
                        %endif
                     </td>
                    <td>
                    %if len(c.detailed_datasets_summary_table[ds_id]['sampleTypeDisplayOrder']) > 150:
                        <% stdp = c.detailed_datasets_summary_table[ds_id]['sampleTypeDisplayOrder'][0:70] %>
                        ${stdp} ...
                    %else:
                        ${c.detailed_datasets_summary_table[ds_id]['sampleTypeDisplayOrder']}
                    %endif
                    </td>
                    <td>${c.detailed_datasets_summary_table[ds_id]['data_type_id']} 
                        (${c.detailed_datasets_summary_table[ds_id]['assay_manufacturer']})
                     

                    </td>
                    <td>
                    % if c.detailed_datasets_summary_table[ds_id]['project'] == {} or c.detailed_datasets_summary_table[ds_id]['project'] == 'NULL' or c.detailed_datasets_summary_table[ds_id]['project'] == 'null' :
                        <% project_name = '' %>
                    %else:
                        <% project_name = c.detailed_datasets_summary_table[ds_id]['project'] %>
                    %endif

                    ${project_name}
                    </td>
                    
                    <td>${c.detailed_datasets_summary_table[ds_id]['organism']}</td>
                    <td>${h.setup_accession_ids_for_viewing(c.detailed_datasets_summary_table[ds_id],"yes")}</td>
                </tr>
            %endfor


            </tbody>
        </table>
    </div>
    %endif

   
</div>
