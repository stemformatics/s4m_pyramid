<%inherit file="../default.html"/>
<%namespace name="Base" file="../base.mako"/>

<%def name="includes()">
    <link rel="stylesheet" type="text/css" href="${h.url('/css/workbench/gene_set_index.css')}" >
    <link rel="stylesheet" type="text/css" href="${h.url('/css/workbench/choose_gene_set.css')}" >

</%def>




<div id="wb_background" class="wb_background_divs">
    <div id="wb_background_inner_div">

        ${Base.wb_breadcrumbs()}


        <div class="wb_question_groups_selected">
            <div class="hidden" id="analysis">${c.analysis}</div>


            <div class="wb_main_menu_expanded">
                    <div class="wb_sub_menu_container">
                        <div class="wb_sub_menu wb_menu_items">
                            <div class="wb_sub_menu_inner_div">

                                % if c.analysis == 2:
                                   Gene Neighbourhood- Choose Number of Genes >> <div class="hidden_url"></div>
                                % endif
                                % if c.analysis == 7:
                                   User Defined Expression Profile - Choose Number of Genes >> <div class="hidden_url"></div>
                                % endif


                            </div>
                        </div>
                        <div class="wb_help_bar wb_menu_items">
                            <div class="wb_help_bar_inner_div">
                                Show help information
                            </div>
                        </div>
                        <div class="wb_help wb_menu_items">
                            <div class="wb_help_inner_div">

                                <p>Please select the number of genes you want to look at from the dropdown menu. The genes will be displayed in the order of rank. The gene at the top is more similar to your chosen gene compared to genes at the bottom of the list. </p>


                            </div>
                        </div>



                    </div>
                    <div class="clear"></div>
                </div>

            </div>




            <form action="${c.url}" method="post">
                <table id="choose_p_value">
                    <thead>
                        <tr>
                            <th id="original">Information</th>
                            <th id="status">Number of Genes</th>
                        </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td>The number of genes you want to have a look at ranked in the order of most correlated to least correlated.</td>
                                <td>
                                    <select name="p_value">
                                            <option selected value="25" >Top 25 Genes (Default) </option>
                                            <option value="50" >Top 50 Genes</option>
                                            <option value="100" >Top 100 Genes</option>
                                            <option value="250" >Top 250 Genes</option>
                                    </select>
                                </td>
                            </tr>




                    </tbody>
                </table>

                <div class="clear" > </div>
                <input class="smallMarginTop" name="p_value_submit" type="Submit" value="Submit"/>
            </form>


        </div>

        <div class="clear" > </div>

</div>
