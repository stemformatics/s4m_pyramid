<%namespace name="Base" file="common.mako"/>

<!DOCTYPE html>
<html lang="en">
<head>
${Base.header_elements()}

<title>Stemformatics Integrated Data Atlas</title>    
<style>
.content {
    font-family: arial, sans-serif;
}
.content a {
    color: #0080ff;
}
.pagetitle {
    font-size: 24px;
    margin: auto;
    width: 370px;
}
.pagetitle img:hover {
    opacity: 0.3;
}
table.noborder, table.noborder td {
    border:0    !important;
}
select {
  font-size: 12px;
}
div.sampleInfo {
    position: absolute;
    background:#f0f0f0; 
    font-size: 14px;
    opacity: 0.9;
    max-width: 300px;
}
div.sampleInfo li {
    margin-top: 5px;
}
.js-plotly-plot .plotly .modebar {
    left: 30px;
}

.introjs-tooltiptext {
    font-size: 13px;
    line-height: 15px;
}

/* See https://medium.com/vuejs-tips/v-cloak-45a05da28dc4 */
[v-cloak] > * { display:none; }
[v-cloak]::before { 
  content: " ";
  display: block;
  width: 36px;
  height: 36px;
  background-image: url('data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==');
}

/* For autocomplete of gene symbols for expression lookup */
.autocomplete {
  position: absolute;
  display: inline-block;
  width: 140px;
  opacity: 0.9;
  vertical-align:top;
  z-index:10;
}

.autocomplete input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  height: 8px;
  font-size: 12px;
  width: 130px;
}

.autocomplete-results {
    padding: 0;
    margin: 0;
    border: 1px solid #eeeeee;
    height: 120px;
    overflow: auto;
}
.autocomplete-result {
    list-style: none;
    text-align: left;
    padding: 4px 2px;
    cursor: pointer;
    font-size: 12px;
    background: #f1f1f1;
}
.autocomplete-result:hover {
    background: lightgrey;
}

/* -----------------------
Modal Content/Box - calling it new-modal to not intefere with an existing style
------------------------*/
div.new-modal {
    position: fixed; /* Stay in place */
    z-index: 20; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    line-height: 20px;
    color: #787878;
    font-size: 14px;
}
div.new-modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 60%; /* Could be more or less, depending on screen size */
}
div.new-modal p {
    margin-top: 10px;
}
div.new-modal li {
    margin-left:5px;
}

/* -----------------------
Draggable div, also resizable
------------------------*/
div.draggable {
    position:absolute; 
    background:white; 
    top:50%; 
    left:50%; 
    transform:translate(-50%,-50%); 
    padding:20px; 
    box-shadow: 0 0 4px rgba(0,0,0,1); 
    resize:both; 
    overflow:auto
}

/* -----------------------
Projected points have this shape
------------------------*/
#diamond {
    width: 0;
    height: 0;
    border: 5px solid transparent;
    border-bottom-color: green;
    position: relative;
    top: -5px;
}
#diamond:after {
    content: '';
    position: absolute;
    left: -5px;
    top: 5px;
    width: 0;
    height: 0;
    border: 5px solid transparent;
    border-top-color: green;
}

</style>

<link href="/static_css/introjs.min.css" type="text/css" rel="stylesheet"/>

<script src="https://unpkg.com/http-vue-loader"></script>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script type="application/javascript" src="/static_js/jscolor.js"></script>
<script type="application/javascript" src="/static_js/intro.min.js"></script>

<script>
// Define all the variables which come from python. Even though it's possible to inject these variables anywhere
// using the template variable syntax, mapping them all to one javascript variable here is recommended, 
// as it makes it easier for long term maintenance - because these are effectively global variables which can
// get out of control and hard to keep track of, if too many end up all over this script.
<%
import json
%>
var dataFromPython =  {
    atlasType: ${json.dumps(atlasType) |n},
    coords: ${json.dumps(coords) |n},
    sampleIds: ${json.dumps(sampleIds) |n},
    sampleTable: ${json.dumps(sampleTable) |n},
    columnsToShow: ${json.dumps(columnsToShow) |n},
    sampleTypeColours: ${json.dumps(sampleTypeColours) |n},
    sampleTypeOrdering: ${json.dumps(sampleTypeOrdering) |n},
    sampleInfo: ${json.dumps(sampleInfo) |n},
    datasetInfo: ${json.dumps(datasetInfo) |n},
};

// This function can be called from banner. So provide an appropriate function that works on this page.
function showHelpPage() {
    //vm.helpDialog.show = true;
    introJs(".content").setOption("showStepNumbers", false).start();
}
</script>

</head>


<body>
${Base.banner()}
<br><br><br><br>

<div class="content" v-cloak>
    <div class="pagetitle">
        <a href="#" onclick="return false" @click="helpDialog.show=true" v-tooltip="tooltip.atlasType"
         data-step="6" data-intro="Click here for more details including a vignette for projecting external data.">
            Integrated Atlas: {{atlasType=="blood"? "Blood" : "Myeloid"}}
        </a>
        <a :href="atlasType=='blood'? '/atlas/myeloid': '/atlas/blood'" style="font-size:14px; margin-left:10px;" 
         v-tooltip="tooltip.atlasToggle">(show {{atlasType=="blood"? "myeloid" : "blood"}})</a>
    </div>

    <!-- Area for controls. -->
    <div style="margin:20px auto 10px auto;" v-bind:style="selectedPlotBy=='sample type'? 'width:600px' : 'width:900px'">
        plot by: 
        <select v-model="selectedPlotBy" @change="changePlotBy" class="dropdown-select" 
         data-step="3" data-intro="You can look up gene expression by colour gradient.">
            <option v-for="item in plotBy">{{item}}</option>
        </select>
        <span v-if="selectedPlotBy=='gene expression'" style="margin-left:20px;">
            gene:
            <div class="autocomplete">
                <input type="text" v-model="selectedGene" @keyup="getPossibleGenes" @keyup.enter="showGeneExpression(selectedGene)" 
                    placeholder="[gene symbol]" v-tooltip="tooltip.selectedGene">
                <ul v-show="showPossibleGenes" class="autocomplete-results">
                    <li v-for="(item,i) in possibleGenes" class="autocomplete-result"
                        :key="i" :class="{ 'is-active': i === 2 }"
                        @click="showGeneExpression(item.symbol);"
                        v-bind:style="item.inclusion? 'color:black' : 'color:grey'">{{item.symbol}}
                    </li>
                </ul>
            </div>
            <button @click="showGeneExpression(selectedGene)" style="margin-left:150px; margin-right:20px;">go</button>&nbsp;
        </span>
        <select v-model="selectedPlotFunction" @change="selectPlotFunction" style="margin-left:20px;"
         data-step="4" data-intro="More plotting options: including side by side plots and gene expression as a violin or box plot.">
            <option value="null">plot functions...</option><option v-for="item in plotFunctions">{{item}}</option>
        </select>
        <select v-model="selectedOtherFunction" @change="selectOtherFunction"
         data-step="5" data-intro="You can download data or plots, project your own data, and more.">
            <option value="null">other functions...</option><option v-for="item in otherFunctions">{{item}}</option>
        </select>
        <a v-show="datasetInfo.sampleIdsMatchingDatasets.length>0" href="#" onclick="return false" @click="clearDataset" style="margin-left:10px; font-size:14px;">
            clear dataset
        </a>
    </div>
    
    <!-- Plot area - try to centre the mainPlotDiv if on its own, while we centre rightPlotDiv more when both are shown. -->
    <div style="display:-webkit-flex; -webkit-flex-flow:row;" @mousemove="updateMousePosition">
        <div v-bind:style="showTwoPlots? 'width:100px' : 'width:300px'"></div>
        <div id="mainPlotDiv" v-on:mouseenter="" v-on:mouseleave="sampleInfo.show=false" style="display:inline-block;"></div>
        <div id="rightPlotDiv" v-on:mouseenter="" v-on:mouseleave="sampleInfo.show=false" style="display:inline-block;"></div>

        <!-- Legend area, only shown if selectedPlot is sample type. -->
        <div v-if="showTwoPlots || selectedPlotBy=='sample type'" style="display:inline-block; max-width:300px; vertical-align:top;">
            colour by: 
            <select v-model="selectedColourBy" @change="updateLegends(); updatePlot()" 
             data-step="1" data-intro="Colour each sample by a sample group here.">
                <option v-for="item in colourBy">{{item}}</option>
            </select>
            <a href="#" onclick="return false;" @click="editLegend.show=true;" v-tooltip="tooltip.editLegend">
                <img src="/static_img/EditIcon.png" style="width:17px; vertical-align:bottom; margin-left:5px;">
            </a>
            <!-- Disable dendrogram for now - still work in progress. Use v-if="uploadData.projectedSampleIds.length>0" to introduce it again later -->
            <a href="#" onclick="return false;" v-if="false" 
                @click="uploadData.showProjectionFunctions=true;" v-tooltip="tooltip.showProjectionFunctions">
                <img src="/static_img/DendrogramIcon.png" style="height:20px; vertical-align:bottom;">
            </a>
            <ul class="legend" style="margin-top:20px;" data-step="2" data-intro="Click on a legend to show/hide samples in the plot.">
                <li v-for="(legend,i) in legends" v-bind:style="sampleTypeBreakPoint[selectedColourBy].indexOf(legend.value)!=-1? 'margin-top:10px' : ''">
                <a href="#" onclick="return false;" @click="updateLegends(i); updatePlot();" style="font-size:13px;">
                <span style="width: 0.5em; vertical-align:middle;" 
                      v-bind:style="[uploadData.projectedSampleIds.indexOf(legend.sampleIds[0])==-1? {'font-size':'2em'} : {'font-size':'1.5em'}, {'color':legend.colour}]">
                    {{uploadData.projectedSampleIds.indexOf(legend.sampleIds[0])==-1? "&bull;" : "&#9671;"}}</span>
                <span v-bind:style="legend.visible? 'color:black' : 'color:#a7a7a7'">{{legend.value}} ({{legend.number}})</span>
                </a>
            </li></ul>
        </div>
    </div>

    <!-- Sample info div -->
    <div v-show="sampleInfo.show" class="sampleInfo" :style="{top:sampleInfoDivPosition('y'), left:sampleInfoDivPosition('x')}"
         v-on:mouseenter="sampleInfo.show=true">
        <div style="background-color:#F99820; padding:10px;">
            <span style="font-size:16px;" v-tooltip="'Details of last double-clicked sample'">Sample Info</span>
            <a href="#" onclick="return false" @click="sampleInfo.show=false" style="float:right; font-weight:bold; color:black;">X</a>
        </div>
        <ul style="padding:10px; line-height:18px;"><li v-for="item in sampleInfo.shownData">
            <span style="font-weight:bold;">{{item.key}}</span><br/>
            <span style="margin-left:5px; display:block;">
                <a v-if="item.key=='dataset'" :href="'/datasets/search?ds_id='+item.datasetId" target="_blank">{{item.value}}</a>
                <span v-else>{{item.value}}</span>
            </span>
        </li></ul>
    </div>

    <!-- Modal for general info and help text. -->
    <div class="new-modal" v-if="helpDialog.show">
        <div class="new-modal-content" style="padding:20px; width:550px;">
            <help-dialog :atlas-type="atlasType"></help-dialog>
            <button @click="helpDialog.show=false" style="margin-top:10px; margin-left:35px;">close</button>
        </div>
    </div>

    <!-- Modal for find dataset dialog. -->
    <div class="new-modal" v-if="datasetInfo.show">
        <div class="new-modal-content" style="padding:20px; width:450px;">
            <find-dataset :all-data="datasetInfo.allData" :sample-ids="sampleIds" :sample-table="sampleTable"
                @highlight-dataset="showDatasetInPlot"></find-dataset>
            <button @click="datasetInfo.show=false" style="margin-top:10px;">close</button>
        </div>
    </div>

    <!-- Modal for reminding user about black coloured dots after find dataset has just been done. -->
    <div class="new-modal" v-if="datasetInfo.showFindDatasetReminder">
        <div class="new-modal-content" style="padding:20px; width:400px;">
            <p>The samples which came from the dataset you clicked on ({{datasetInfo.selectedDatasetInfo}}) are coloured black on the plot.
            Click on "clear dataset" to clear this colour.</p>
            <p><input type="checkbox" v-model="datasetInfo.hideFindDatasetReminder" @change="setFindDatasetReminder" 
                style="vertical-align:middle"> Don't show this message again.</p>
        <button @click="datasetInfo.showFindDatasetReminder=false" style="margin-top:30px;">close</button>
        </div>
    </div>

    <!-- Modal for download data or images dialog. -->
    <div class="new-modal" v-if="downloadDialog.show">
        <div class="new-modal-content" style="padding:20px; width:450px;">
        <download-dialog :atlas-type="atlasType" :main-plot-div="mainPlotDiv" @cancel="downloadDialog.show=false"></download-dialog>
        </div>
    </div>

    <!-- Modal for edit legend dialog. -->
    <div class="new-modal" v-if="editLegend.show">
        <div class="new-modal-content" style="padding:20px; width:450px;">
        <edit-legend :legends="legends" :original-colours="sampleTypeColoursOriginal[selectedColourBy]" @cancel="editLegend.show=false" @apply="applyEditLegend"></edit-legend>
        </div>
    </div>

    <!-- Draggable div for gene expression (scatter plot) div -->
    <draggable-div v-if="geneExpressionDialog.show" class="draggable">
        <div slot="header" style="z-index:11;">
            <div style="width:95%; text-align:center; float:left;">
                <h1><a href="#" onclick="return false" v-tooltip="tooltip.geneExpressionPlot">Gene expression as violin or box plot</a></h1>
            </div>
            <div style="float:right;">
                <a href="#" onclick="return false" @click="geneExpressionDialog.show=false" v-tooltip="tooltip.close">x</a>
            </div>
        </div>
        <div slot="main">
        <gene-expression :selected-gene="selectedGene" :possible-genes="possibleGenes" :current-gene-expression="geneExpression"
            :sample-table="sampleTable" :sample-ids="sampleIds" :sample-type-colours="sampleTypeColours"
            :colour-by="colourBy" :selected-colour-by="selectedColourBy" :sample-type-ordering="sampleTypeOrdering"
            @close="geneExpressionDialog.show=false"></gene-expression>
        <button slot="footer" @click="geneExpressionDialog.show=false" style="margin-top:30px;">close</button>
        </div>
    </draggable-div>
    
    <!-- Modal for upload data dialog and draggable div for projection functions after projection. -->
    <div class="new-modal" v-if="uploadData.show">
        <div class="new-modal-content" style="padding:20px; width:450px;">
            <data-upload @project-data="projectData"></data-upload>
        <button @click="uploadData.show=false" style="margin-top:30px;">close</button>
        </div>
    </div>

    <draggable-div v-if="uploadData.showProjectionFunctions" class="draggable">
        <div slot="header" style="z-index:11;">
            <div style="width:95%; text-align:center; float:left;">
                <h1><a href="#" onclick="return false" v-tooltip="tooltip.projectionFunctions">Nearest neighbours of projected data</a></h1>
            </div>
            <div style="float:right;">
                <a href="#" onclick="return false" @click="uploadData.showProjectionFunctions=false" v-tooltip="tooltip.close">x</a>
            </div>
        </div>
        <div slot="main">
            <projection-functions :projected-samples="uploadData.projectedSampleIds" :name="uploadData.name"
                                  :sample-table="sampleTable" :colour-by="colourBy" :selected-colour-by="selectedColourBy"
                                  :sample-type-colours="sampleTypeColours"></projection-functions>
        <button slot="footer" @click="uploadData.showProjectionFunctions=false" style="margin-top:30px;">close</button>
        </div>
    </draggable-div>

    <!-- Draggable div for custom sample grouping div -->
    <draggable-div v-if="customSampleGroupDialog.show" class="draggable">
        <div slot="header" style="z-index:11;">
            <div style="width:95%; text-align:center; float:left;">
                <h1><a href="#" onclick="return false" v-tooltip="tooltip.customSampleGroup">Combine sample groups</a></h1>
            </div>
            <div style="float:right;">
                <a href="#" onclick="return false" @click="customSampleGroupDialog.show=false" v-tooltip="tooltip.close">x</a>
            </div>
        </div>
        <div slot="main">
        <custom-sample-group :sample-table="sampleTable" :sample-ids="sampleIds" :sample-type-colours="sampleTypeColours"
            :sample-groups="colourBy" :sample-type-ordering="sampleTypeOrdering" :custom-group-name="customSampleGroupDialog.groupName"
            @save="applyCustomSampleGroup" @close="customSampleGroupDialog.show=false"></custom-sample-group>
        </div>
    </draggable-div>

    <!-- Modal for reminding user about projection functions available after projection has just been done. -->
    <div class="new-modal" v-if="uploadData.showFeatures">
        <div class="new-modal-content" style="padding:20px; width:400px;">
            <h1 style="font-weight:bold">Projection complete</h1>
            <p>You can edit colours of projected points on the PCA plot by clicking on the edit colours icon next to the 'colour by' selection.</p>
            <p>You can also see how close your projected samples are to other samples by clicking on the dendrogram icon next to the 'colour by' selection.</p>
            <p><input type="checkbox" v-model="uploadData.hideFeaturesMessage" @change="setUploadDataHideFeaturesMessage" 
                style="vertical-align:middle"> Don't show this message again.</p>
        <button @click="uploadData.showFeatures=false" style="margin-top:30px;">close</button>
        </div>
    </div>

</div>

${Base.footer()}

<script>
// Define Vue instance, which handles all the interactions within the page.
// Note that mainPlotDiv and rightPlotDiv behave differently - mainPlotDiv is always on,
// whereas rightPlotDiv may be turned off and it also only shows one type of plot.
var vm = new Vue({
    el: '.content',
    components: {
        'download-dialog': window.httpVueLoader('/static_js/AtlasDownload.vue'),
        'help-dialog': window.httpVueLoader('/static_js/AtlasHelp.vue'),
        'find-dataset': window.httpVueLoader('/static_js/AtlasFindDataset.vue'),
        'data-upload': window.httpVueLoader('/static_js/AtlasDataUpload.vue'),
        'edit-legend': window.httpVueLoader('/static_js/AtlasEditLegend.vue'),
        'gene-expression': window.httpVueLoader('/static_js/AtlasGeneExpression.vue'),
        'custom-sample-group': window.httpVueLoader('/static_js/AtlasCustomSampleGroup.vue'),
        'projection-functions': window.httpVueLoader('/static_js/AtlasProjection.vue'),
        'jscolor': window.httpVueLoader('/static_js/JSColor.vue'),
        'draggable-div': window.httpVueLoader('/static_js/DraggableDiv.vue'),
    },
    data: {	
        // map data from server to vue variables
        atlasType: dataFromPython.atlasType,    // 'myeloid' or 'blood'
        coords: dataFromPython.coords,  // {col: {row: val, ...}}
        sampleIds: dataFromPython.sampleIds,    // ['sample1', ...]
        sampleTable: dataFromPython.sampleTable,    // {'celltype':{'sample1':'HPC', ...}, ...}

        colourBy: dataFromPython.columnsToShow,
        selectedColourBy: dataFromPython.columnsToShow[0],
        sampleTypeColoursOriginal: dataFromPython.sampleTypeColours,    // colours may change, so we keep original colours stored here
        sampleTypeColours: JSON.parse(JSON.stringify(dataFromPython.sampleTypeColours)),    // note this may be empty - then use default plotly colours
        sampleTypeOrdering: dataFromPython.sampleTypeOrdering,  // may be empty - then use alphabetical ordering

        // variables used by the sample info div which is shown when a sample is double-clicked
        sampleInfo: {
            allData: dataFromPython.sampleInfo, // {'GSM2064216;6731': {'Sample Type': 'Clec4e-/- microglia I/R', 'FACS profile': '', ...}, ...}
            show: false,
            shownData: [],
            sampleId: null,
            lastClickTime: 0,
            mouseX: 0,
            mouseY: 0,
            divX: 0,
            divY: 0
        },

        // variables used by the find dataset div which can be used to show a table of datasets
        datasetInfo: {
            allData: dataFromPython.datasetInfo, // [{"datasetId":7268,"author":"Abud","year":"2017","pubmedId":"28426964","platform":"RNAseq","numberOfSamples":43},...]
            sampleIdsMatchingDatasets: [],    // sample ids returned by the search
            show: false,
            selectedDatasetInfo: "",
            showFindDatasetReminder: false, // controls modal show/hide after dataset has been clicked on in the find dialog
            hideInfoMessage: localStorage.getItem('Stemformatics_atlas_datasetInfo_findDatasetReminder') || false,   // whether to hide the modal div after finding dataset
        },

        // edit legend dialog
        editLegend: {
            show: false,
        },

        // download data/plots dialog
        downloadDialog: {
            show: false
        },

        // gene expression (scatter plot) dialog
        geneExpressionDialog: {
            show: false
        },

        // custom sample group dialog
        customSampleGroupDialog: {
            show: false,
            groupName: 'custom_sample_group',   // used as sample group name
        },

        // upload data dialog
        uploadData: {
            show: false,
            showProjectionFunctions: false, // shows div where projection functions are
            projectedSampleIds: [],  // record sample ids which have been projected
            name: null, // name of the dataset used for projection - will be the prefix for projected samples
            showFeatures: false,    // shows modal div reminding user about projection functions
            hideFeaturesMessage: localStorage.getItem('Stemformatics_atlas_hideFeaturesMessage') || false,   // whether to hide the modal div about projection functions
        },

        // general info dialog
        helpDialog: {
            show: false
        },

        // Do our own legends instead of using plotly's, in order to customise spacing between groups
        // Note we don't have the double-click functionality that plotly provides.
        legends: [],

        plotTypes: ["3d", "2d"],
        selectedPlotType: "3d",

        plotBy: ["sample type", "gene expression"],
        selectedPlotBy: "sample type",

        otherFunctions: ["download data/plots","find dataset","project other data","combine sample groups"],
        selectedOtherFunction: null,

        // gene expression related
        selectedGene: "",
        possibleGenes: [],  // gene ids and symbols used to populate the autocomplete field
        showPossibleGenes: false,
        geneExpression: [], // flat list of values, in same order as sampleIds, to be fetched when requested

        selectedPlotFunction: null,

        // plotly requires id of div where it will plot, so set them as vars here
        mainPlotDiv: "mainPlotDiv",
        rightPlotDiv: "rightPlotDiv",

        // default camera angle for a 3d plot in plotly
        camera: {up: {x:0, y:0, z:1}, center: {x:0, y:0, z:0}, eye: {x:1.25, y:1.25, z:1.25}},
        showTwoPlots: false,

        // tooltip texts
        tooltip: {atlasType: "<p class='tooltiptext'>show information about this page</p>",
                  atlasToggle: "<p class='tooltiptext'>toggle atlas</p>",
                  selectedGene: "<div class='tooltiptext'><p>Select a gene from suggestions and press go to show its expression.</p>" +
                                "<p>The genes with grey font colours were filtered out before creating this PCA.</p></div>",
                  geneExpressionPlot: "<p class='tooltiptext'>This plot shows rank normalised values of the gene in the atlas as "+
                                      "either a violin or a box plot. The values are in the range [0,1].</p>" +
                                      "<p class='tooltiptext'>You can drag this plot overlay by grabbing it near the title.</p>",
                  close: "<p class='tooltiptext'>close dialog</p>",
                  editLegend: "<p class='tooltiptext'>Edit colours of points</p>",
                  showProjectionFunctions: "<p class='tooltiptext'>Show nearest neighbours of projected points.</p>",
                  projectionFunctions: "<p class='tooltiptext'>You can drag this plot overlay by grabbing it near the title..</p>",
                  customSampleGroup: "<p class='tooltiptext'>You can drag this dialog overlay by grabbing it near the title..</p>",
                  },
    },
    computed: {
        plotFunctions: function() {
            if (this.showTwoPlots)
                return ["toggle 3d/2d", "hide sample colour plot", "gene expression box plot"];
            else
                return ["toggle 3d/2d", "show sample colour plot", "gene expression box plot"];
        },

        // For a long list of sample types, it's good to place a gap between groups of them as a visual aid.
        // This returns the items where breaks should occur.
        sampleTypeBreakPoint: function() {
            var itemsWithBreaks = {}
            for (var i=0; i<this.colourBy.length; i++) {
                var key = this.colourBy[i];
                itemsWithBreaks[key] = [];
                if (!(key in this.sampleTypeOrdering)) continue;
                for (var j=1; j<this.sampleTypeOrdering[key].length; j++) {
                    if (this.sampleTypeOrdering[key][j-1]=="")
                        itemsWithBreaks[key].push(this.sampleTypeOrdering[key][j]);
                }
            }
            return itemsWithBreaks;
        },

    },
    methods: { 
        // ------------ Data update methods ---------------

        // Colours for sample groups may not be pre-defined, and samples groups also may be created dynamically (eg. custom sample group).
        // Hence run this function to ensure all sample group colours have been populated.
        updateSampleTypeColours: function() {
            let self = this;

            // If colours weren't specified we set them here from this list
            let exampleColours = ['#64edbc', '#6495ed', '#ed6495', '#edbc64', '#8b8b00', '#008b00', '#8b008b', '#00008b', 
                                    '#708090', '#908070', '#907080', '#709080', '#008080', '#008000', '#800000', '#bca68f', 
                                    '#bc8fa6', '#bc8f8f', '#008160', '#816000', '#600081', '#ff1493', '#14ff80'];
            for (let i=0; i<self.colourBy.length; i++) {
                if (!(self.colourBy[i] in self.sampleTypeColours)) {
                    self.sampleTypeColours[self.colourBy[i]] = {}

                    let column = self.sampleTable[self.colourBy[i]]; // dict of selected column from sampleTable
                    let availableValues = [];  // will store the unique values in this column
                    for (let sampleId in column) {
                        if (column[sampleId]=="") continue; // skip null valued rows
                        if (availableValues.indexOf(column[sampleId])==-1) {    // first time seen
                            availableValues.push(column[sampleId]);
                        }
                    }

                    for (let j=0; j<availableValues.length; j++)
                        self.sampleTypeColours[self.colourBy[i]][availableValues[j]] = exampleColours[j % exampleColours.length];
                }
            }
        },

        // Some sample groups may come without sample type ordering, but this is used throughout
        // the page, so this function will define this based on alphabetical ordering.
        updateSampleTypeOrdering: function() {
            let self = this;
            for (let i=0; i<self.colourBy.length; i++) {
                if (!(self.colourBy[i] in self.sampleTypeOrdering)) {
                    self.sampleTypeOrdering[self.colourBy[i]] = {}

                    let column = self.sampleTable[self.colourBy[i]]; // dict of selected column from sampleTable
                    let availableValues = [];  // will store the unique values in this column
                    for (let sampleId in column) {
                        if (column[sampleId]=="") continue; // skip null valued rows
                        if (availableValues.indexOf(column[sampleId])==-1) {    // first time seen
                            availableValues.push(column[sampleId]);
                        }
                    }

                    for (let j=0; j<availableValues.length; j++)
                        self.sampleTypeOrdering[self.colourBy[i]] = availableValues;
                }
            }
        },

        // Run before sample group plot to populate the legends array, and when a legend is clicked to show/hide a trace
        updateLegends: function(clickedLegendIndex) {
            var self = this;

            // Work out what values are available for selected colour by:
            var column = self.sampleTable[self.selectedColourBy]; // dict of selected column from sampleTable
            var availableValues = [];  // will store the unique values in this column
            var sampleIds = {}; // dict to store list of matching sample ids for each item of availableValues
            for (var sampleId in column) {
                if (column[sampleId]=="") continue; // skip null valued rows
                if (availableValues.indexOf(column[sampleId])==-1) {    // first time seen
                    availableValues.push(column[sampleId]);
                    sampleIds[column[sampleId]] = [];
                }
                sampleIds[column[sampleId]].push(sampleId);
            }

            // Now we order these based on sampleTypeOrdering if this is available
            var ordering = self.selectedColourBy in self.sampleTypeOrdering? self.sampleTypeOrdering[self.selectedColourBy] : null;
            availableValues.sort(function(a, b) {
                return ordering==null? a>b : ordering.indexOf(a) - ordering.indexOf(b);
            });
            // Might as well set ordering using this alphabetical ordering
            if (!(self.selectedColourBy in self.sampleTypeOrdering)) {
                self.sampleTypeOrdering[self.selectedColourBy] = availableValues;
            }

            var legends = [];
            for (var i=0; i<availableValues.length; i++) {
                // Work out the colour for this trace. This is either from sampleTypeColours or from following list
                //var colour = (self.selectedColourBy in self.sampleTypeColours && availableValues[i] in self.sampleTypeColours[self.selectedColourBy])?
                //        self.sampleTypeColours[self.selectedColourBy][availableValues[i]] : exampleColours[i % exampleColours.length];
                var legend  = {'value': availableValues[i], 
                               'number': sampleIds[availableValues[i]].length,
                               'sampleIds': sampleIds[availableValues[i]],
                               'colour': self.sampleTypeColours[self.selectedColourBy][availableValues[i]]};
                legend.visible = (self.legends.length==availableValues.length && self.legends[i].value==availableValues[i])? self.legends[i].visible : true;
                if (clickedLegendIndex==i)
                    legend.visible = !legend.visible;
                legends.push(legend);
            }
            self.legends = legends;
        },

        // ------------ Plotting methods ---------------
        // Note that you can access traces of a plot by document.getElementById("mainPlotDiv").data with plotly js.
        // (.data is an array, so .data[0] is the first trace, etc)

        // Layout dict used by plotly - can control size, camera, etc.
        // type should be either "sample type" or "gene expression" (null defaults to sample type) 
        // - this changes the title of the plot
        layout: function(type) {
            if (type==null) type = "sample type";

            // work out title based on type
            var title = "Coloured by " + this.selectedColourBy;
            if (type=="gene expression")
                title = "Rank normalised expression of " + this.selectedGene;
            if (this.showTwoPlots)  // title interferes with hover icons and too busy anyway
                title = "";

            //var width = 1100;
            //var height = 600;
            //if (this.showTwoPlots) {
            //    width = type=="sample type"? 400 : 800;
            //    height = type=="sample type"? 400 : 600;
            //}
            return { 
                //title: title, 
                //showlegend: this.selectedColourBy=="gene expression",
                showlegend: false,
                height: 600,   // height of the plot in pixels
                width: this.showTwoPlots? 600 : 900,
                //height: height,
                //width: width,
                margin: {t:20, l:0, r:0, b:0},
                xaxis: {title: "PC1"},
                yaxis: {title: "PC2"},
                uirevision: true,
                hovermode: 'closest',
                scene: {camera: this.camera,
                        xaxis: {title: "PC1"},
                        yaxis: {title: "PC2"},
                        zaxis: {title: "PC3"}}
            };
        },

        // Return a list of traces to use for plots.
        traces: function(type="sample type") {
            var self = this;	// this refers to the Vue instance, and it's safer to map it to another variable

            // Define elements common to all traces and return them - if we simply define a variable here, it will
            // only be referenced when I try to use it below. Using {...template} or Array.from(template) will clone
            // the array instead of referencing, but won't do a deep copy so have to deal with template.markers
            // separately, for example. So it's easier to define a function that will return a new object.
            function traceTemplate() {
                var template = {
                    marker: {},
                    mode: "markers",
                    hoverinfo: "text",
                };

                if (self.selectedPlotType.indexOf("3d")!=-1) {  // specify parameters specific to 3d plot
                    template.type = "scatter3d";
                    template.marker.size = 5;
                } else {
                    template.type = "scatter";
                }
                return template;
            }

            // What hover text do we want to show
            var hovertext = {};
            for (var i=0; i<self.sampleIds.length; i++) {
                // show values from all columns of sampleTable
                //var values = self.colourBy.slice(0).reverse().map(function(item) { return self.sampleTable[item][self.sampleIds[i]]; });
                // show currently selected value + help text
                var values = [self.sampleTable[self.selectedColourBy][self.sampleIds[i]], "(double-click for more info)"];
                // also add sample and dataset ids
                //values.push("(" + self.sampleIds[i] + ")");
                hovertext[self.sampleIds[i]] = values.map(function(item) { return "<span style='font-size:14px'>" + item + "</span>"}).join(" ");
            }

            // create traces
            var traces = [];
            if (type=="sample type") {
                // one trace per item of availableValues
                for (var i=0; i<self.legends.length; i++) {
                    var legend = self.legends[i];
                    if (!legend.visible) continue;

                    var trace = traceTemplate();
                    trace.x = legend.sampleIds.map(function(item) { return self.coords['x'][item]});
                    trace.y = legend.sampleIds.map(function(item) { return self.coords['y'][item]});
                    trace.z = legend.sampleIds.map(function(item) { return self.coords['z'][item]});
                    trace.text = legend.sampleIds.map(function(item){ return hovertext[item]});
                    trace.name = legend.value;
                    trace.marker.color = legend.sampleIds.map(function(item) { 
                        return self.datasetInfo.sampleIdsMatchingDatasets.indexOf(item)!=-1? "black": legend.colour});
                    trace.marker.symbol = legend.sampleIds.map(function(item) {
                        return self.uploadData.projectedSampleIds.indexOf(item)==-1? "circle" : "diamond-open";
                    })
                    trace.sampleIds = legend.sampleIds;
                    traces.push(trace);
                }
            } else {    // gene expression plot - one trace only
                var trace = traceTemplate();
                trace.x = self.sampleIds.map(function(item) { return self.coords['x'][item]});
                trace.y = self.sampleIds.map(function(item) { return self.coords['y'][item]});
                trace.z = self.sampleIds.map(function(item) { return self.coords['z'][item]});
                trace.text = self.sampleIds.map(function(item) { return hovertext[item]});
                // If 
                trace.marker.color = self.geneExpression.map(function(item,i) { 
                    return self.datasetInfo.sampleIdsMatchingDatasets.indexOf(self.sampleIds[i])!=-1? "black": item});
                trace.marker.colorbar = { title: self.selectedGene };
                if (self.showTwoPlots) {    
                    // It's possible that some traces are hidden from rightPlotDiv, in which case we want to match that here.
                    // For each sampleId, set marker size to zero if it belongs to the hidden legend.
                    // Note that there's a bug in plotly where size specified as an array in marker is rendered differently to
                    // when specified as a number, even for the same size.
                    var defaultSize = self.selectedPlotType=="3d"? 11 : 6;
                    var visibleLegends = self.legends.filter(function(item) { return item.visible; }).map(function(item) { return item.value; });
                    trace.marker.size = self.sampleIds.map(function(item,i) { 
                        return visibleLegends.indexOf(self.sampleTable[self.selectedColourBy][self.sampleIds[i]])==-1? 0 : defaultSize;
                    });
                    trace.marker.line = {width: 0}; // plotly also seems to add stroke after array is specified as size
                }
                trace.sampleIds = self.sampleIds;
                traces.push(trace);
            }
            return traces;
        },

        // Function to perform the plot for the first time - use updatePlot after the first time
        mainPlot: function() {
            var self = this;
            var div = document.getElementById(self.mainPlotDiv);
            
            // plot in the mainDiv
            Plotly.newPlot(div, self.traces(), self.layout());

            // set up synchronisation with rightPlotDiv by listening for events on mainPlotDiv
            div.on('plotly_relayout',
                function(eventdata) { 
                    self.camera = eventdata["scene.camera"];    // update camera values with this
                    if (self.showTwoPlots)
                        Plotly.react(self.rightPlotDiv, self.traces(), self.layout());
            });

            // Set up double click event, where sampleInfo.shownData is populated with info about the sample double clicked.
            // Note that plotly doesn't really have double click event detection, so we're going to measure the interval between
            // two single clicks if it's on the sample id.
            div.on('plotly_click', function(data) { self.handlePlotlyClick(data) });
        },

        // Function to update the plot
        updatePlot: function() {
            let self = this;
            let div = document.getElementById(self.rightPlotDiv);

            if (self.selectedPlotBy=="sample type") // always show one plot for sample type
                self.showTwoPlots = false;

            if (self.showTwoPlots) {
                if (div.layout==null) {    // no plot yet - we plot both and set up sync
                    self.mainPlot();
                    Plotly.newPlot(div, self.traces(), self.layout());
                    div.on('plotly_relayout',
                        function(eventdata){ 
                            self.camera = eventdata["scene.camera"];    // update camera values with this
                            if (self.showTwoPlots)
                                Plotly.react(self.mainPlotDiv, self.traces(self.selectedPlotBy), self.layout());
                    });
                    div.on('plotly_click', function(data) { self.handlePlotlyClick(data, "sample type") });
                } else  // there's already a plot in rightPlotDiv, so just update it
                    Plotly.react(div, self.traces(), self.layout());
            } else {
                if (div.layout!=null)   // showing only one plot but rightPlotDiv contains a plot, so purge it
                    Plotly.purge(div);
            }
            // always update mainPlotDiv
            Plotly.react(self.mainPlotDiv, self.traces(self.selectedPlotBy), self.layout(self.selectedPlotBy));

        },

        // Run when plotBy changes between "sample type" and "gene expression"
        changePlotBy: function() {
            if (this.selectedPlotBy=="sample type") // going back to sample type after showing expression
                this.updatePlot();
            else    // going to gene expression after showing sample type - update only if previously an expression was shown
                if (this.selectedGene!="") this.updatePlot();
        },

        // ------------ Gene expression related methods ---------------

        // Show autocomplete on gene expression by fetching all possible entries
        getPossibleGenes: function() {
            var self = this;
            if (self.selectedGene.length<=1) return;    // ignore 1 or less characters entered
            axios.get('/atlas/geneinfo?type=' + self.atlasType + '&queryString=' + self.selectedGene)
                .then(function (response) {
                    var genes = response.data.result;
                    if (genes.length>0) {
                        self.possibleGenes = genes;
                        self.showPossibleGenes = true;
                    }
            });
        },

        // Hide possible genes when user clicks outside that area
        handleClickOutside(evt) {
            //console.log(this.$el.classList);
            //if (!this.$el.contains(evt.target)) {
                this.showPossibleGenes = false;
            //}
        },

        // Show gene expression - fetch values from server and save them, then run updatePlot
        showGeneExpression: function(geneSymbol) {
            var self = this;
            if (geneSymbol!=null)
                self.selectedGene = geneSymbol; 
            self.showPossibleGenes=false; 
            var matchingGenes = self.possibleGenes.filter(function(item) { return item.symbol==self.selectedGene});
            if (matchingGenes.length>0) {
                geneId = matchingGenes[0].ensembl;
                axios.get('/atlas/expression?type=' + self.atlasType + '&geneId=' + geneId)
                    .then(function (response) {
                        var expressionValues = response.data.expressionValues;
                        if (expressionValues.length>0) {
                            self.geneExpression = expressionValues;
                            self.updatePlot();
                        } else {
                            alert("Could not find expression values for this gene");
                        }
                });
            } else
                alert("No expression values exist in this atlas for the specified gene");
        },

        // Run when user changes selection on gene exp functions
        selectPlotFunction: function() {
            if (this.selectedPlotFunction=="toggle 3d/2d") {
                this.selectedPlotType = this.selectedPlotType=="2d"? "3d":"2d";
                this.updatePlot();
            }
            else if (this.selectedPlotFunction.indexOf("sample colour plot")!=-1) {
                if (this.geneExpression.length==0) {    // no gene selected so can't show two plots
                    alert("Use this function to show the plot by sample type side by side after you are seeing expression plot.");
                    this.showTwoPlots = false;
                }
                else {
                    this.showTwoPlots = !this.showTwoPlots;
                    this.updatePlot();
                }
            }
            else if (this.selectedPlotFunction=="gene expression box plot") {
                if (this.geneExpression.length==0)  // no gene selected so can't show two plots
                    alert("Use this function to show gene expression as a violin/box plot after selecting a gene (plot by >> gene expression).");
                else
                    this.geneExpressionDialog.show = true;
            }
            this.selectedPlotFunction = null;
        },

        // ------------ sampleInfo methods ---------------
        // Should run when user clicks on a point in the plot. Since there's no double-click event detection in plotly
        // we measure the time interval between clicks to define double click.
        handlePlotlyClick: function(data, plotBy) {
            var self = this;
            if (plotBy==null) plotBy = self.selectedPlotBy;
            var sampleId = plotBy=="sample type"? self.traces()[data.points[0].curveNumber].sampleIds[data.points[0].pointNumber] : self.sampleIds[data.points[0].pointNumber];
            
            if (self.sampleInfo.sampleId==sampleId && performance.now() - self.sampleInfo.lastClickTime < 600) {   
                // same sample id clicked and its internval since last click is short enough to define as double-click
                self.sampleInfo.shownData = [];
                for (i=self.colourBy.length-1; i>=0; i--)
                    self.sampleInfo.shownData.push({'key':self.colourBy[i], 'value':self.sampleTable[self.colourBy[i]][sampleId]});
                for (var key in self.sampleInfo.allData[sampleId])
                    if (self.sampleInfo.allData[sampleId][key]!="")
                    self.sampleInfo.shownData.push({'key':key, 'value':self.sampleInfo.allData[sampleId][key]});
                // Last key is 'dataset', which will have dataset id value, but display will be author and year
                var datasetId = sampleId.split(";")[1];
                var matchingDataset = self.datasetInfo.allData.filter(function(item) { return item.datasetId==parseInt(datasetId); });

                // For projected points, they don't have the same sample id structure so we can't matching dataset this way
                // We can fetch last value but note that this won't work after multiple projections.
                if (matchingDataset.length==0) { // assume this is because we clicked on a projected point and they exist
                    matchingDataset = [self.datasetInfo.allData[self.datasetInfo.allData.length-1]];
                    datasetId = matchingDataset[0].datasetId;
                }
                self.sampleInfo.shownData.push({'key':'dataset', 
                                                'value':matchingDataset[0].author + " (" + matchingDataset[0].year + ")", 
                                                'datasetId':datasetId});
                self.sampleInfo.divX = self.sampleInfo.mouseX;
                self.sampleInfo.divY = self.sampleInfo.mouseY;
                self.sampleInfo.show = true;
            } else {   // set this as last sampleId clicked, and record time
                self.sampleInfo.sampleId = sampleId;
                self.sampleInfo.lastClickTime = performance.now();
                self.sampleInfo.shownData = [];
                self.sampleInfo.show = false;
            }
        },

        // In order to know where to show the sampleInfoDiv on double-click, we need to keep track
        // of mouse position and save this.
        updateMousePosition: function(event) {
            this.sampleInfo.mouseX = event.clientX;
            this.sampleInfo.mouseY = event.clientY;
        },

        // Return the position where the sample info div should appear - just a wrapper for sampleInfo.divX and divY
        sampleInfoDivPosition: function(axis) {            
            return axis=='x'? this.sampleInfo.divX + 20 + "px" : this.sampleInfo.divY + "px";
        },

        // ------------ datasetInfo methods ---------------
        // Run when user clicks on the dataset (author) in datasetInfo dialog
        showDatasetInPlot: function(data) {
            this.datasetInfo.sampleIdsMatchingDatasets = data.sampleIdsMatchingDatasets;
            this.datasetInfo.selectedDatasetInfo = data.selectedDatasetInfo;
            this.updatePlot();
            this.datasetInfo.show = false;
            // note that localStorage stores strings, so careful with returned value
            this.datasetInfo.showFindDatasetReminder = localStorage.getItem("Stemformatics_atlas_datasetInfo_hideFindDatasetReminder")==null || localStorage.getItem("Stemformatics_atlas_datasetInfo_hideFindDatasetReminder")!='true';
        },

        clearDataset: function() {
            this.datasetInfo.sampleIdsMatchingDatasets = [];
            this.updatePlot();
        },

        setFindDatasetReminder: function() {
            localStorage.setItem("Stemformatics_atlas_datasetInfo_hideFindDatasetReminder", this.datasetInfo.hideFindDatasetReminder);
        },

        // ------------ Other methods ---------------

        // Should run when selecting various functions
        selectOtherFunction: function() {
            if (this.selectedOtherFunction=="download data/plots")
                this.downloadDialog.show = true;
            else if (this.selectedOtherFunction=="find dataset")
                this.datasetInfo.show = true;
            else if (this.selectedOtherFunction=="project other data")
                this.uploadData.show = true;
            else if (this.selectedOtherFunction=="combine sample groups")
                this.customSampleGroupDialog.show = true;
            this.selectedOtherFunction = null;
        },

        // Adds projected points to all relevant data variables. Note that we should be able to
        // remove these points later - for now, reload page.
        addProjectedPoints: function(name, coords, sampleIds, sampleTypes, datasetAttributes) {
            var self = this;
            self.uploadData.name = name;
            for (var item in self.coords) {
                for (var i=0; i<coords.length; i++)
                    self.coords[item][sampleIds[i]] = coords[i][item];
            }
            for (var item in self.sampleTable) {
                self.sampleTypeOrdering[item].push("");
            }
            for (var i=0; i<sampleTypes.length; i++) {
                self.sampleInfo.allData[sampleIds[i]] = {};
                self.uploadData.projectedSampleIds.push(sampleIds[i]);
                for (var item in self.sampleTable) {
                    self.sampleTable[item][sampleIds[i]] = sampleTypes[i];
                    self.sampleTypeColours[item][sampleTypes[i]] = "green";
                    self.sampleTypeOrdering[item].push(sampleTypes[i]);
                    self.sampleInfo.allData[sampleIds[i]][item] = sampleTypes[i];
                }
            }
            for (var i=0; i<sampleIds.length; i++)
                self.sampleIds.push(sampleIds[i]);

            self.datasetInfo.allData.push(datasetAttributes);
        },

        // Actually run projection. data here is FormData object in order to handle user's own file uploads.
        projectData: function(data) {
            // Add some other info to data
            var self = this;
            data.append("atlasType", self.atlasType);
            axios.post('/atlas/project', data, { headers: {'Content-Type': 'multipart/form-data'}})
                .then(function(response) {
                    if (response.data.error=="") {

                        // Map a particular sample field from test data to all of the sample fields in the atlas
                        var column = "Sample Type"; // would be this if we were projecting a Stemformatics dataset
                        if (data.get("dataSource")!="Stemformatics") {  // Try to use user provided field if it exists, otherwise use first column
                            column = Object.keys(response.data.samples[0]).indexOf(data.get("sampleColumn"))!=-1? data.get("sampleColumn") 
                                : Object.keys(response.data.samples[0])[0];
                        }
                        // This is a list of projected items that will be displayed in the legend. ["Ang_iPSC","Ang_HSC","Ang_iPSC",...]
                        // Note that this list is same length as projected sample ids. Also these don't change when colour by changes.
                        var sampleTypes = response.data.samples.map(function(item) {return data.get("name") + "_" + item[column]});

                        // Create some dataset attributes needed
                        var datasetAttributes = {"datasetId":data.get("datasetId"), "author":"[author]", "year":"[year]"};
                        
                        self.addProjectedPoints(data.get("name"), response.data.coords, response.data.sampleIds, sampleTypes, datasetAttributes);
                        self.updateLegends();
                        self.updatePlot();
                    } else {
                        alert("There was an error with this: " + response.data.error);
                    }
                    self.uploadData.show = false;
                    // Still working on this. Hide for now
                    ///self.uploadData.showFeatures = localStorage.getItem("Stemformatics_atlas_hideFeaturesMessage")==null || !localStorage.getItem("Stemformatics_atlas_hideFeaturesMessage");
                    self.uploadData.showFeatures = false;
                });
        },

        // Legend colours and shapes can be updated here. data should have same structure as legends.
        applyEditLegend: function(data) {
            // Update sampleTypeColours
            for (var i=0; i<data.length; i++) {
                var value = data[i].value;  // lengend label, eg. "myeloid"
                // Set colour of this label
                this.sampleTypeColours[this.selectedColourBy][value] = data[i].colour;
            }
            this.updateLegends();
            this.updatePlot();
            this.editLegend.show = false;
        },

        setUploadDataHideFeaturesMessage: function() {
            localStorage.setItem("Stemformatics_atlas_hideFeaturesMessage", this.uploadData.hideFeaturesMessage);
        },

        // Apply custom sample group defined. data looks like [{sampleGroup:'B Cell_in vitro', sampleIds:['s1',...]}, ...]
        applyCustomSampleGroup: function(data) {
            let self = this;
            if (data.length==0) {
                self.customSampleGroupDialog.show = false;
                return;
            }
            var groupName = self.customSampleGroupDialog.groupName;
            if (self.colourBy.indexOf(groupName)==-1)
                self.colourBy.push(groupName);
            self.sampleTable[groupName] = {};
            self.sampleTypeOrdering[groupName] = [];
            for (var i=0; i<data.length; i++) {
                self.sampleTypeOrdering[groupName].push(data[i].sampleGroup);
                for (var j=0; j<data[i].sampleIds.length; j++)
                    self.sampleTable[groupName][data[i].sampleIds[j]] = data[i].sampleGroup;
            }
            self.customSampleGroupDialog.show = false;

            // Show sample type plot and custom sample group defined 
            // - otherwise it's not obvious to the user that the changes have been saved
            self.updateSampleTypeColours();
            self.selectedColourBy = self.customSampleGroupDialog.groupName;
            self.selectedPlotBy = "sample type";
            self.updateLegends();
            self.updatePlot();
        }
    },
    
    mounted() {	// Vue runs this section after loading the page
        this.updateSampleTypeColours();
        this.updateSampleTypeOrdering();
        this.updateLegends();
        this.mainPlot();
        document.addEventListener('click', this.handleClickOutside);
    },
    destroyed() {
        document.removeEventListener('click', this.handleClickOutside);
    }
})
</script>

</body>
</html>