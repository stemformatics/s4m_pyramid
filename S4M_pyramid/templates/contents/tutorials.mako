<%inherit file="../default.html"/>\
<%namespace name="Base" file="../base.mako"/>
<%def name="includes()">
    <link href="${h.url('/css/contents/privacy_policy.css')}" type="text/css" rel="stylesheet">
    <link href="${h.url('/css/sass/stylesheets/screen.css')}" type="text/css" rel="stylesheet">
</%def>


<div class="content">
    <div class="content_left_column">
        ${Base.content_menu(url.environ['pylons.routes_dict']['action'])}
    </div>
    <div class="content_right_column">
        <div class="content_box">
            <div class="header_1">
This is the Help and Frequently Asked Questions page for ${c.site_name}
            </div>
            <div class="text">
            <p>This provides you with hands-on tutorials and frequently asked questions to help you get started or answer questions in ${c.site_name}.</p>
            </div>
        </div>
        <div class="content_box">
            <div class="content_links">
                <a href="#citation">How to Cite Us ></a>
                <a href="#tutorials">Tutorials to get started ></a>
                <div class="clear"></div>
            </div>
       </div>
       ${Base.citation_content()}
       <div id="more_tutorials" class="content_box">
            <a id="tutorials"></a>
            <div class="header_2">
                Tutorials
            </div>
            <div class="text">
                <ul class="tutorialList">
                % for tutorial, start_page in c.tutorials.items():
                    <li><a href="${h.url(start_page + '#tutorial=' + tutorial)}" onclick="return audit_help_log ('${tutorial}', 'help_tutorial'); ">${tutorial.replace("_", " ").capitalize()}</a></li>
                % endfor
                </ul>
            </div>


       </div>


    </div>
</div>
