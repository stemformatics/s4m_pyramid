<%inherit file="../default.html"/>\
<%namespace name="Base" file="../base.mako"/>
<%def name="includes()">
</%def>
<script>
	$.ajax({
	  url: "${c.url_for_qc_failure_rates}",
	  cache: false
	})
        .done(function( response ) {
            var obj = JSON.parse(response);
            $('#no_raw_data').html(obj['Failed No Raw Data %']); 
            $('#qc_experiment_design').html(obj['Failed QC Experiment Design %']); 
            $('#qc_normalisation').html(obj['Failed QC Normalisation %']); 
            $('#annotation').html(obj['Failed Annotation QC %']); 
            $('#total').html(obj['Failed %']); 
        });

	$.ajax({
	  url: "/datasets/breakdown",
	  cache: false
	})
        .done(function( response ) {
            var final_obj = JSON.parse(response);

            var sortable = [];
            for (var sample_type in final_obj) {
                sortable.push([sample_type, final_obj[sample_type]]);
            }

            sortable.sort(function(a, b) {
                return b[1] - a[1];
            });

            var table_obj = $('#breakdown_table');
            for (let value of sortable) {
                var name = value[0];
                var total = value[1];
                table_obj.find('tbody').append('<tr><td>'+name+'</td><td>'+total+'</td></tr>');
            }
            table_obj.DataTable({

                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": false,
                "aaSorting": [[ 1, "desc" ]],
                "bAutoWidth": false } );

        });


</script>

<div class="content">
    <div class="content_left_column">
        ${Base.content_menu(url.environ['pylons.routes_dict']['action'])}
    </div>
    <div class="content_right_column">
        <div class="content_box">
            <div class="header_1">
All of the data on this site has been handpicked, checked for experimental reproducibility and design quality, and normalized in-house.

            </div>
            <div class="text">
                <p>
                We share all of our published datasets with ArrayExpress and all links to publications are provided through PubMed. Our backend analyses use a mixture of our own code and GenePattern from the Broad Institute.
                </p>
                <p>
                    You can find documentation on all of our data processing, QC and analysis on this page.
                </p>
            </div>
        </div>
        <div class="content_box">
            <div class="content_links">
                <a href="#breakdown">Sample Breakdown ></a>
                <a href="#failure">Failure Statistics ></a>
                <a href="#data">Data Methods  ></a>
                <a href="#formats">Formats and Standards ></a>
                <div class="clear"></div>
            </div>
        </div>

        <div class="content_box">
            <a id="breakdown"></a>
            <div class="header_2">
                Sample Breakdown
            </div>
            <div class="text">
                <p>
                    This is the sample breakdown of all the public data in ${c.site_name}. 
                </p>
                <p id="breakdown_table_paragraph">
                    <table id="breakdown_table">
                    <thead>
                        <th>Name</th><th>Number</th>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </p>
                <div class="clear"></div>

            </div>

        </div>


        <div class="content_box">
            <a id="failure"></a>
            <div class="header_2">
                Failure Statistics
            </div>
            <div class="text">
                <p>
                The failure statistics demonstrate the percentage of datasets have failed the ${c.site_name} QC process.
                </p>
                <p>
		<table>
                    <thead>
                        <th>Name</th> <th>Percentage %</th>
                    </thead>
                    <tbody>
                        <tr><td>No Raw Data</td><td id="no_raw_data"></td></tr>
                        <tr><td>Failed QC Experiment Design</td><td id="qc_experiment_design"></td></tr>
                        <tr><td>Failed QC Normalisation</td><td id="qc_normalisation"></td></tr>
                        <tr><td>Failed Annotation QC</td><td id="annotation" ></td></tr>
                        <tr><td>Total</td><td id="total"></td></tr>
                    </tbody>
		</table>
                </p>
                <div class="clear"></div>

            </div>

        </div>
        <div class="content_box">
            <a id="data"></a>
            <div class="header_2">
                Data Methods
            </div>
            <div class="text">
                <p>
                    This is the documentation that explains how data is imported into ${c.site_name}. It includes the normalisation methods, mappings and database imports like Kegg.
                </p>
                <p>
                    <ul>
                        <li><a target = "_blank" href="${h.url('/'+str(c.site_name)+'_data_methods.pdf')}">${c.site_name} Data Methods (pdf)</a></li>

                    </ul>
                </p>
                <div class="clear"></div>

            </div>

        </div>
        <div class="content_box">
            <a id="formats"></a>
            <div class="header_2">
                Formats and Standards
            </div>
            <div class="text">
                <p>
                    These are some of the formats and standards we have based ${c.site_name} on.
                </p>
                <p>
                    <ul>
                    	<li><a target = "_blank" href="http://obi-ontology.org/">The Ontology for Biomedical Investigations project</a></li>

                    </ul>
                </p>
                <div class="clear"></div>

            </div>

        </div>
    </div>
</div>


