<%inherit file="../default.html"/>\
<%namespace name="Base" file="../base.mako"/>
<%def name="includes()">
    <link href="${h.url('/css/contents/privacy_policy.css')}" type="text/css" rel="stylesheet">
    <link href="${h.url('/css/sass/stylesheets/screen.css')}" type="text/css" rel="stylesheet">
</%def>
<div class="content">
    <div class="content_left_column">
        ${Base.content_menu(url.environ['pylons.routes_dict']['action'])}
    </div>
    <div class="content_right_column">
        <div class="content_box">
            <div class="header_1">
${c.site_name} is a collaboration between the stem cell and bioinformatics community.
            </div>
            <div class="text">
 <p>We were motivated by the plethora of exciting cell models in the public and private domains, and the realisation that for many biologists these were mostly inaccessible. We wanted a fast way to find and visualise interesting genes in these exemplar stem cell datasets. We'd like you to explore. You'll find data from leading stem cell laboratories in a format that is easy to search, easy to visualise and easy to export.</p>
<p>${c.site_name} is not a substitute for good collaboration between bioinformaticians and stem cell biologists. We think of it as a stepping stone towards that collaboration.</p>
            </div>
        </div>
        <div class="content_box">
            <div class="content_links">
                <a href="#team">Our Core Team > </a>
                <a href="#annotators">Our Annotators ></a>
                <a href="#students">Our Students ></a>
                <a href="#citation">How to Cite Us ></a>
                <a href="#funding">Funding ></a>
                <a href="#suggest">Suggest a Dataset ></a>
                <a href="#partners">Partners ></a>
                <a href="#past_partners">Past Partners ></a>
                <div class="clear"></div>
            </div>
        </div>
        <div class="content_box">
            <a id="team"></a>
            <div class="header_2">
                Our Core Team
            </div>
            <div>
                <div class="team">
                    <img src="/images/contents/team_photos/christine_s.jpg">
                    <div class="team_member">Christine Wells<br/>Project Leader</div>
                    <div class="clear"></div>
                </div>
                <div class="team">
                    <img src="/static_img/Jack_s.jpg">
                    <div class="team_member">Jack Bransfield<br/>Systems software engineer</div>
                    <div class="clear"></div>
                </div>
                <div class="team">
                    <img src="/images/contents/team_photos/jarny_s.png">
                    <div class="team_member">Jarny Choi<br/>Project Manager</div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="content_box">
            <a id="annotators"></a>
            <div class="header_2">
                Our Annotators (former)
            </div>
            <div>
               <div class="text">
                    <div class="margin_top_small team_member">Chris Pacheco Rivera, Jessica Schwarber, Suzanne Butcher,<br/>
                    Elizabeth Mason, Alejandro Vitale, Jill Shepherd</div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="content_box">
            <a id="students"></a>
            <div class="header_2">
                Our Students
            </div>
            <div>
               <div class="text">
                    <h4>Current</h4>
                        <div class="margin_top_small team_member">Isaac Virshup, Catherine Song, Xinly Pan, Yidi Deng, Thanushi Peiris
                        </div>
                    <h4>Former</h4>
                        <div class="margin_top_small team_member">Yifan Wang, Lu Chen, Palash, Luxin, JingCheng Wang, Wu Yan,<br/>
                        Ariane Mora, Sadia Waleem, Huan Wang, Melinda Wang, Peter Zhang
                        </div>
                </div>
            </div>
        </div>
        ${Base.citation_content()}
        <div class="content_box">
            <a id="funding"></a>
            <div class="header_2">
                Funding
            </div>
            <div class="text">
                <p>Our current list of funders include:
                <ul>
                    <li>NHMRC Synergy grant 1186371</li>
                    <li>ARC Future Fellowship (FT150100330)</li>
                    <li>JEM Research Foundation philanthropic funding</li>
                    <li>University of Melbourne Centre for Stem Cell Systems</li>
                </ul>
                </p>
                <p>Past funders include:
                <ul>
                    <li>Stemformatics was established as part of the ARC Special Research Initiative to Stem Cells Australia (SR1101002)</li>
                    <li>QLD Government Smart Futures Fellowship</li>
                </ul>
                </p>
            </div>
        </div>
        <div class="content_box">
            <a id="suggest"></a>
            <div class="header_2">
                Suggest a Dataset
            </div>
            <div class="text">

                <p>You can suggest a dataset that goes straight into our dataset queue, Agile_org.
                <ul><li>
                    <a target="_blank" href=${c.agile_org}>Click here to go to Agile_org to add a new dataset</a>
                </ul></li>
                </p>
            </div>
        </div>
        <div class="content_box partners">
            <a id="partners"></a>
            <div class="header_2">
                Partners
            </div>
            <div class="vertical_align">
                <a target="_blank" href="http://www.stemcellsaustralia.edu.au/">
                    <img src="/images/logos/STE_SCA.png"></img>
                </a>
            </div>
            <div class="vertical_align">
                <a target="_blank" href="http://nectar.org.au/">
                <img src="/images/logos/STE_NECTAR.png"></img>
                </a>
            </div>
            <div class="vertical_align">
                <a href="#partners">
                <img src="/images/logos/STE_JEM.png"></img>
                </a>
            </div>
             <div class="clear"></div>
        </div>
        <div class="content_box partners">
            <a id="past_partners"></a>
            <div class="header_2">
                Past Partners
            </div>
            <div class="vertical_align">
                <a target="_blank" href="http://www.qcif.edu.au/">
                <img src="/images/logos/STE_QCIF_about_us.png"></img>
                </a>
            </div>
            <div class="vertical_align">
                <a target="_blank" href="http://www.qfab.org/">
                <img src="/images/logos/STE_QFAB.png"></img>
                </a>
            </div>
            <div class="vertical_align">
                <a target="_blank" href="http://www.wehi.edu.au/">
                <img src="/images/logos/STE_WEHI.png"></img>
                </a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
