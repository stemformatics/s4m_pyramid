<%inherit file="../default.html"/>\
<%namespace name="Base" file="../base.mako"/>

<%def name="includes()">
    <link href="${h.url('/css/contents/privacy_policy.css')}" type="text/css" rel="stylesheet">
</%def>


    <!-- links on the leftColumn -->
   <div class="content">
    <div class="content_left_column">
        ${Base.content_menu(url.environ['pylons.routes_dict']['action'])}
    </div>
    <div class="content_right_column">
        <div class="content_box">
            <div class="header_1">
                ${c.site_name} provides links to other Internet sites only for the
                convenience of its users.
            </div>
            <div class="text">
                <p>
                ${c.site_name} is not responsible for the availability or content of these external sites, nor do we endorse, warrant or guarantee the products, services or information described or offered.
                </p>
                <p>
                Information obtained on the ${c.site_name} website is not intended to take the place of medical advice from a fully qualified health professional.
                </p>
                <p>
                The views and opinions of authors expressed on ${c.site_name} website do not necessarily reflect those of the contributing organisations, and they may not be used for advertising or product endorsement purposes without the author's explicit written permission.
                </p>
            </div>
        </br>
            <div class="header_1">
                ${c.site_name}  is subject to the laws governing privacy in Australia: The Commonwealth Privacy Act 1988.
            </div>
        </br>
            <div class="header_2">Information That You Provide Us</div>
            <div class="text">
                <p>
                To register with ${c.site_name}, a user must provide a valid email address and a password, the name of the organisation they are affiliated with, and give ${c.site_name} permission to send them notifications.
                </p>
            </div>
            <div class="header_2">Information That We Collect & How We Use It</div>
            <div class="text">
                </br>
                <ul>
                    <li>Information from cookies & other technologies
                        <p>
                            Each time a user logs into our website, the cookies are stored on their web browser. ${c.site_name} uses these cookies to authenticate the user and allow them access to appropriate public and private datasets. We also use cookies to keep the user logged into the website or to automatically log in the user when they return to our website. ${c.site_name} uses Google Analytics to track user numbers, pages and access preferences to help us improve our services.
                        </p>
                    </li>
                    <li>Auditing the data file downloads
                        <p>Each time a user downloads a data file from the dataset summary page, we log this information in our database. We store your user ID, along with the dataset ID, date and time the information was accessed. We use this information to understand the dataset popularity and determine the dataset trends to help us improve the user experience.</p>
                    </li>
                    <li>Auditing the navigation
                        <p>Each time a user views a ${c.site_name} graph, uses our analysis tools, uses the help function, or any other page of our website, we record the page usage in our database. We analyse this page usage to understand which species, datasets, or genes are the most popular and to improve the user experience.</p>
                    </li>
                    <li>Personal information of registered users
                        <p>We keep a database of registered users for communication purposes, e.g. to let the user know when their analysis has finished. </p>
                        <p>${c.site_name} does not pass any information to another third party without the knowledge and prior consent of the user.
                        </p>
                    </li>
                </ul>
                
            </div>
            

            <div class="header_2">Any Questions?</div>
            <div class="text">
                <p>
                Please contact us on ${c.feedback_email} if you have any question regarding our privacy policy.
                </p>
            </div>
            
        </div>

    </div>
</div>



