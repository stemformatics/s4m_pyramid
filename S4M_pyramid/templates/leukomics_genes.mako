<%namespace name="Base" file="common.mako"/>

<!DOCTYPE html>
<html lang="en">
<head>
${Base.header_elements()}

<title>Leukomics - Molecular Profiles</title>    
<style>
.content {
    font-family: arial, sans-serif;
}
.content a {
    color: #0080ff;
}
.pagetitle {
    font-family: Avenir,Helvetica,Arial,sans-serif;
    font-size: 2rem;
    margin: auto;
    width: 350px;
    color: rgb(235, 149, 52);
}
#info {
    width:200px;
    padding:15px;
    font-size:14px;
    color:white;
    background:#68228B;
    line-height:1.3em;
    opacity:0.8;
    border-radius:10px;
}
#info a {
    color: #ffffd2;
}
#info a:hover {
    color: white;
}
div.hover {
    position:absolute;
    top:130px;
    left:80px;
    width:120px;
    padding:15px;
    font-size:14px;
    color:black;
    background:#F99820;
    line-height:1.3em;
    opacity:0.8;
    border-radius:10px;
}
</style>

</head>

<body>
${Base.banner()}
<br><br><br><br>

<div class="content" v-cloak>
    <div style="display:flex; width:1000px; margin:auto;">
        <div style="margin:auto; flex:60%; text-align:center; position:relative; display:table">
            <h2 class="pagetitle" style="">About Leukomics</h2>
            <p style="background:lightyellow; padding:20px;">Leukaemias arise from normal blood cells that have gained the ability for uncontrollable self-renewal and defective differentiation. 
            Haematopoietic stem cells (HSCs) self-renew and differentiate into all blood lineages. Leukaemic stem cells (LSCs) share the capacity 
            for the extensive self-renewal and survival with normal HSCs, but instead of differentiating into blood lineages they give rise to leukaemia.</p>
            <p style="background:lightyellow; padding:20px;">Chronic myeloid leukaemia (CML) is caused by the constitutive expression of the chimeric oncogene BCR-ABL1 in HSCs. 
            Huge improvement in patient care was achieved when BCR-ABL1 tyrosine kinase inhibitors were discovered. 
            However, resistance in those patients is still an issue, and therefore the development of novel therapies specifically targeting the LSC, 
            which are responsible for relapse, is very important.</p>
            <p style="background:lightyellow; padding:20px;">Acute myeloid leukaemia (AML) is the most common form of adult leukaemia and also the most aggressive haematological malignancy. 
            It is frequently associated with relapse which is usually fatal. The standard of care in AML patients has not improved outcomes in decades. 
            Childhood and adult AML have very different survival rates and also molecular signatures. 
            Understanding age-specific biology and molecular features of AML is important to provide patients with adequate and 
            most appropriate targeted treatments.</p>
        </div>
    </div>
</div>

${Base.footer()}

<script>
var vm = new Vue({
    el: '.content',
    data: {	
        selectedInfo: null,
    },
})
</script>

</body>
</html>