from pyramid.view import view_config
from S4M_pyramid.model.stemformatics_data import atlas, genes, datasets
from pyramid import testing
from S4M_pyramid.views import user_views
from S4M_pyramid.views import redis_socket, atlas_dir, sql_connection_string, gct_dir, params

def _atlasDataFromConfig(request, atlasType):
    return atlas.AtlasData(redis_socket(request), atlas_dir(request), atlasType)

@view_config(route_name="/atlas", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/blood", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/blood/", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/myeloid", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/myeloid/", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/imac", renderer="S4M_pyramid:templates/atlas.mako")
@view_config(route_name="/atlas/imac/", renderer="S4M_pyramid:templates/atlas.mako")
def showAtlasPage(request):
    """Show the main atlas page.
    atlasType can be either "blood" or "myeloid".

    Note that request.tmpl_context is the global c variable which was previously used extensively.
    But request.tmpl_context may not have all variables assigned, as controllers used to inherit from lib/base.py:BaseController,
    where many of the variables were set. So look in there to find how to set specific variables directly without invoking base.py
    """
    # Determine atlas type based on route
    routePath = request.current_route_path()
    if routePath.endswith("/"):
        routePath = routePath[:-1]
    atlasType = routePath.split("/")[-1]
    if atlasType not in ["blood","myeloid"]:
        atlasType = request.params.get("type", "myeloid")

    atlasData = _atlasDataFromConfig(request, atlasType)
    coords = atlasData.pcaCoordinates()    # [[1,2,3],[4,5,6],...], n x 3 data frame
    coords.columns = ['x','y','z']  # in case this isn't set by the model
    sampleTable = atlasData.sampleTable()
    colours,orders = atlasData.coloursAndOrders()

    # Get extra sample info for the sampleId-datasetId combo in the atlas
    ds = datasets.Datasets(sql_connection_string(request))
    sampleInfo = ds.sampleInfoFromSampleIdDatasetId(sampleIdDatasetId=sampleTable.index)

    # Get dataset info for all datasets in the atlas
    
    datasetIds = [item.split(";")[1] for item in sampleTable.index]
    datasetInfo = ds.datasetInfo(datasetIds=set(datasetIds)).sort_values("author").reset_index()

    # Add dataset as another column in sampleTable
    df = datasetInfo.set_index("datasetId")  # pandas will assign int to these datasetIds
    sampleTable["Dataset"] = ["%s_%s_%s" % (df.at[int(dsId),"author"], df.at[int(dsId),"year"], df.at[int(dsId),"platform"]) for dsId in datasetIds]

    # We could provide ordering of this Dataset column based on platform, then year. 
    # Colour by platform - note current code ASSUMES either "RNAseq" or "microarray" for platform.
    # Also insert spacing between platforms.
    df = df.sort_values(["platform","year"])
    orders["Dataset"] = []
    colours["Dataset"] = {}
    currentPlatform = df["platform"].tolist()[0]
    for index,row in df.iterrows():
        key = "%s_%s_%s" % (row["author"], row["year"], row["platform"])  # need to be the same as what was used in the Dataset column values of sampleTable
        if row["platform"]!=currentPlatform:  # insert blank
            orders["Dataset"].append("")
            currentPlatform = row["platform"]
        orders["Dataset"].append(key)
        colours["Dataset"][key] = "#00008B" if df.at[index,"platform"]=="RNAseq" else "#ED6495"

    return {'atlasType': atlasType,
            'coords': coords.to_dict(), 
            'sampleIds': coords.index.tolist(), 
            'sampleTable': sampleTable.to_dict(),
            'columnsToShow': sampleTable.columns.tolist(),
            'sampleTypeColours': colours,
            'sampleTypeOrdering': orders,
            'sampleInfo': sampleInfo,
            'datasetInfo': datasetInfo.to_dict(orient="records")}

def atlasPageRedirect(request):
    from pyramid.httpexceptions import HTTPFound
    #path = config.add_route("/atlas/redirect", "/atlas/blood")
    if path in ["blood","imac","myeloid"]:
        return HTTPFound(location='/atlas?type=%s' % path)
    else:
        return HTTPFound(location='/atlas/%s' % path)


@view_config(route_name="/atlas/expression", renderer="json")
def getAtlasExpression(request):
    """Return gene expression values for the atlas page as a list. These values are separate from
    normal expression values (rank normalised values), and come from local files.
    atlasType can be either "blood" or "myeloid"
    """
    atlasType = request.params.get("type")
    geneId = request.params.get("geneId")

    atlasData = _atlasDataFromConfig(request, atlasType)
    expressionValues = atlasData.expressionValues([geneId])
    return {"expressionValues": expressionValues}

@view_config(route_name="/atlas/geneinfo", renderer="json")
def getGeneInfo(request):
    atlasType = request.params.get("type", "myeloid")
    queryString = request.params.get("queryString")
    error = ""

    if queryString is None:
        error = "No queryString specified."
        result = {}
    else:
        atlasData = _atlasDataFromConfig(request, atlasType)
        df = atlasData.geneInfo().fillna("")
        df = df[df["symbol"].str.lower().str.contains(queryString.lower())]
        result = df.sort_values(["inclusion","symbol"], ascending=[False,True]).reset_index().to_dict(orient="records")
        #gs = genes.Geneset(request.registry.settings.get("psycopg2_conn_string"))
        #result = gs.geneInfo(queryString, species="human").reset_index().to_dict(orient="records")
    return {"error":error, "queryString":queryString, "result":result}

@view_config(route_name='/atlas/download')
def downloadFile(request):
    """Return a request.response object corresponding to a file download.
    Note that for static files, simply returning a response object from: response = FileResponse('haemosphere/models/data/Genes.txt') works.
    """
    import tempfile, json
    from pyramid.response import Response, FileResponse

    # Parse input
    atlasType = request.params.get("type", "myeloid")
    filename = request.params.get('filename', "samples")

    atlasData = _atlasDataFromConfig(request, atlasType)
    f = tempfile.NamedTemporaryFile(prefix=filename, suffix='.txt')
    downloadFilename = "%s_atlas_%s_v7.1.tsv" % (atlasType, filename)

    if filename=='samples':	# requesting sample table
        atlasData.sampleTable().to_csv(f.name, sep="\t")
    elif filename=='genes': # genes table
        atlasData.geneInfo().to_csv(f.name, sep="\t")
    elif filename=='colours': # colours file
        colours,orders = atlasData.coloursAndOrders()
        open(f.name, 'w').write(json.dumps({'colours':colours, 'ordering':orders}, indent=4))
    #elif filename=='expression': # expression table 
        # - because this is a very large file, we will host it as a static resource from pipe (served by apache)
        # so the link to this is directly at the mako file.
        ##atlasData.expressionTable(atlasType).to_csv(f.name, sep="\t")  # instead of doing this
    else:
        return request.response	# should really return error response once I work it out

    f.seek(0,0)
    response = FileResponse(f.name)
    response.headers['Content-Disposition'] = ("attachment; filename=%s" % str(downloadFilename))	# str() needed to avoid 'u' attached to some filename strings

    return response

@view_config(route_name='/atlas/datasetinfo', renderer="json")
def getDatasetInfo(request):
    """Return a summary information on Stemformatics datasets based on query string, which could be used to project onto the atlas.
    """
    queryString = request.params.get("queryString")
    ds = datasets.Datasets(sql_connection_string(request))
    datasetInfo = ds.datasetInfo(queryString=queryString, extraColumns=["Title"], species="Homo sapiens").sort_values("author").reset_index()
    return datasetInfo.to_dict(orient="records")

@view_config(route_name='/atlas/project', renderer="json")
def projectData(request):
    """Project either Stemformatics dataset or user dataset onto the atlas.
    """
    # Parse input
    dataSource = params(request, 'dataSource')
    atlasType = params(request, "atlasType", "blood")
    name = params(request, "name")
    platform = params(request, "platform")

    import os, pandas
    try:
        if dataSource=="Stemformatics":
            # Read in gct file for selected Stemformatics dataset
            datasetId = params(request, 'datasetId', {})
            filepath = os.path.join(gct_dir(request), "dataset%s.gct" % datasetId)
            if not os.path.exists(filepath):
                return {'error': "Couldn't find matching gct file for the selected dataset."}
            df = pandas.read_csv(filepath, sep="\t", skiprows=2, index_col=0).drop('Description', axis=1)

            # Also fetch sample information on this dataset and align index
            samples = datasets.Datasets(sql_connection_string(request)).sampleTable(datasetIds=[datasetId]).loc[df.columns]
            
        else:   # user supplied file
            filetypes = ["expression","samples"]
            files = dict([(filetype, request.POST.get(filetype).file) for filetype in filetypes if request.POST.get(filetype)!=b''])
            df = pandas.read_csv(files["expression"], sep='\t', index_col=0)
            samples = pandas.read_csv(files["samples"], sep='\t', index_col=0).loc[df.columns]

            # Perform some validation on user supplied files
            if len(df)==0:
                return {"error": "Data to project appears to have 0 rows. Check format of the file."}
            elif df.shape[1]==0:
                return {"error": "Data to project appears to have 0 columns. Check format of the file. Is it tab separated?"}
            elif not set(df.columns).issubset(set(samples.index)):
                return {"error": "Not all columns of expression matrix are found as row ids of sample matrix."}


        # Note that if a data frame contains nan values, the returned object to the client after to_dict() function is not a dictionary!
        # It actually converts the dictionary into a jsonified string. To avoid this, fillna with a string.
        df = df.fillna(0)  # This isn't great if a Stemformatics dataset contains -ve values, as these genes do not end up at the bottom of rankings.
        samples = samples.fillna("Null")

        # Create atlas data instance
        atlasData = _atlasDataFromConfig(request, atlasType)

        if platform=="microarray":  # we need to substitute ensembl ids for probe ids
            ds = datasets.Datasets(sql_connection_string(request))
            geneInfo = atlasData.geneInfo()
            df = ds.convertProbeIdsToGeneIds(df, geneIdsSubset=geneInfo[geneInfo["inclusion"]].index)

        # Perform projection
        ## Disable hierarchy calculation for now, which relies on combined coords being present
        ##result = atlasData.projection(name, df)
        result = atlasData.projection(name, df, includeCombinedCoords=False)
        if result["error"] !="": # Returning empty data frame causes exception when trying to parse as json, so just return error string
            return {"error": result["error"]}

        # Prepare the dictionary to return - each object must be JSON serializable (so don't return data frame).
        # Note result may also contain 'combinedCoords' key, which we will store in session.
        result["coords"] = result["coords"].to_dict(orient="records")
        result["samples"] = samples.to_dict(orient="records")
        result["sampleIds"] = ["%s_%s" % (name, item) for item in samples.index]
        if "combinedCoords" in result:
            result["combinedCoords"] = result["combinedCoords"].to_json(orient="split")
            request.session['combinedCoords'] = result["combinedCoords"]
        return result
    except:
        return {"error": "There was an unforeseen error with this process. It may be due to the file format. Note that projection functionality is in beta, and we're still working to improve it. Please email the development team with some details of the problem you've encountered."}

@view_config(route_name='/atlas/hierarchy', renderer="json")
def hierarchyOfProjections(request):
    """Return hierarchy object from projected point. Uses session['combinedCoords'].
    """
    import pandas, matplotlib
    from scipy.spatial.distance import euclidean
    from scipy.cluster import hierarchy

    df = pandas.read_json(request.session.get("combinedCoords"), orient="split")
    sampleId = request.params.get("sampleId", df.index[0])
    n = request.params.get("n", 20)
    
    # Calculate euclidean distance between sampleId and all other samples, and choose n closest
    dist = pandas.Series([euclidean(df.loc[sampleId],row) for index,row in df.iterrows()], index=df.index)
    dist = dist.sort_values()[:n]

    # Calculate hierarchy on this subset of df
    df = df.loc[dist.index]
    link = hierarchy.linkage(df, 'ward')
    h = hierarchy.dendrogram(link, labels=df.index.tolist(), orientation='right', no_plot=True)

    # Convert colours in h to hex
    h['color_list'] = [matplotlib.colors.to_hex(item) for item in h['color_list']]

    return h

###############################

def test_getAtlasExpression():
    request = testing.DummyRequest()
    request.params["type"] = "myeloid"
    request.params["geneId"] = "ENSG00000157404"
    print(getAtlasExpression(request)["expressionValues"][:3])

def test_getGeneInfo():
    request = testing.DummyRequest()
    request.params["type"] = "myeloid"
    request.params["queryString"] = "kit"
    print(getGeneInfo(request))
