"""Controller for handling user related requests.
"""
from S4M_pyramid.model.stemformatics import db_deprecated_pylons_orm, Stemformatics_Auth

def currentUser(request):
    """Return the currently logged in user as a dictionary. None if not logged in.
    """
    user = {}
    if 'user' not in request.session:
        #check cookie
        username = request.cookies.get('stay_signed_in')
        user_and_pwd_md5 = request.cookies.get('stay_signed_in_md5')
        cookie_user = Stemformatics_Auth.check_stay_signed_in_md5(db_deprecated_pylons_orm, username, user_and_pwd_md5)

        if cookie_user is not None:
            # Store user into session
            request.session['user'] = cookie_user.username
            request.session['uid'] = cookie_user.uid
            request.session['full_name'] = cookie_user.full_name
            request.session['role'] = Stemformatics_Auth.get_user_role(db_deprecated_pylons_orm,cookie_user.uid)
            request.session.save()

    return {'user': request.session.get('user',''),
            'uid': request.session.get('uid',0),
            'full_name': request.session.get('full_name',''),
            'role': request.session.get('role'),
            'notifications': 1 if request.session.get('user','')!='' else 0}
