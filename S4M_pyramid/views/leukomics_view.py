from pyramid.view import view_config
'''
@view_config(route_name="/leukomics", renderer="S4M_pyramid:templates/leukomics.mako")
def showLeukomicsMainPage(request):
    """Show the main leukomics page.
    """
    return {}
'''
@view_config(route_name="/leukomics_about", renderer="S4M_pyramid:templates/leukomics_about.mako")
def showAboutLeukomicsPage(request):
    """Show about leukomics page.
    """
    return {}

@view_config(route_name="/leukomics_genes", renderer="S4M_pyramid:templates/leukomics_genes.mako")
def showLeukomicsGenesPage(request):
    """Show leukomics genes page.
    """
    return {}

