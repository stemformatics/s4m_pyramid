def redis_socket(request):
    return request.registry.settings.get("redis_socket")

def atlas_dir(request):
    return request.registry.settings.get("atlas_dir")

def sql_connection_string(request):
    return request.registry.settings.get("psycopg2_conn_string")

def gct_dir(request):
    return request.registry.settings.get("gct_files_dir")

def params(request, key, default=None):
    """
    Return parameter value attached to the key of request.params. Just a shortcut to avoid writing the following code
    for methods accepting various ways of parameter submission. 
    Eg: params(request, 'filename', 'myfile.txt') may be equivalent to request.params.get('filename', 'myfile.txt')
    """	
    # first try get
    value = request.params.get(key)

    if value is None: # check post
        value = request.POST.get(value)

    if value is None: # check json body
        # I used to use hasattr(request, 'json_body') to check for json_body in request object, but it now seems to
        # raise an exception even when the request object seems to have this attribute, so just use try/except block here.
        try:
            value = request.json_body.get(key)
        except:
            value = default

    return value if value is not None else default
