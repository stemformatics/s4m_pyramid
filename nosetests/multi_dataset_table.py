import pyramid
from pyramid.paster import get_appsettings
from pyramid.config import Configurator
from S4M_pyramid.model.stemformatics.stemformatics_dataset import Stemformatics_Dataset

app = pyramid.paster.get_app('/home/isha/s4m_pyramid/production.ini')

def test_dataset_downloader_data():	
	uid = ''
	role= "None"
	result = Stemformatics_Dataset.get_detailed_datasets_summary_table_data(uid,role)
	print(result)
	assert result is None

def test_dataset_downloader_data():	
	uid = 169
	role= "Normal"
	result = Stemformatics_Dataset.get_detailed_datasets_summary_table_data(uid,role)
	assert result is not None

def test_dataset_downloader_data():	
	uid = 629
	role= "Admin"
	result = Stemformatics_Dataset.get_detailed_datasets_summary_table_data(uid,role)
	assert result is not None

def test_dataset_downloader_data():	
	uid = 0
	role= "None"
	result = Stemformatics_Dataset.get_detailed_datasets_summary_table_data(uid,role)
	assert result is not None