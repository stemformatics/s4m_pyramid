#!/bin/bash

psql -U portaladmin portal_prod -c "insert into stemformatics.configs (ref_type,ref_id) values('validation_regex', '(?=^.{12,}$)(?=.*\s+).*$');"
psql -U portaladmin portal_prod -c "insert into stemformatics.configs (ref_type,ref_id) values('from_email', 'noreply@stemformatics.org');"
psql -U portaladmin portal_prod -c "insert into stemformatics.configs (ref_type,ref_id) values('secret_hash_parameter_for_unsubscribe', 'I LOVE WY');"
psql -U portaladmin portal_prod -c "insert into stemformatics.configs (ref_type,ref_id) values('publish_gene_set_email_address', 'fake_email');"

psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('twitter_app_key','xRPPHXqi49Wwde2B8dTlXw');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('twitter_app_secret','l0d48qRQ0gSOLXQI4v2xP3a7vT1TgiOg8H3YVvDEo');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('twitter_oauth_token','456310432-JTqWAijgvpuJuFIxcIGJ5mStek78bCRjXlvSzSMV');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('twitter_oauth_token_secret','0Rftaoi6uPb9A5x9vNVcfdwLTUMa196LELBPpoZz1w');"

psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('GPQueue','/var/www/pylons-data/prod/jobs/GPQueue/');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('StemformaticsQueue','/var/www/pylons-data/prod/jobs/StemformaticsQueue/');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetGCTFiles','/var/www/pylons-data/prod/GCTFiles/');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetCLSFiles','/var/www/pylons-data/prod/CLSFiles/');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('email_to','test@mailinator.com');"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_69' where ref_type = 'cdn_base_url';"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('debug','false');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('gene_mapping_raw_file_base_name','/var/www/pylons-data/prod/gene_mapping.raw');"
psql -U portaladmin portal_prod -c "INSERT INTO stemformatics.configs (ref_type,ref_id) values('feature_mapping_raw_file_base_name','/var/www/pylons-data/prod/feature_mapping.raw');"

psql -U portaladmin portal_prod -c "alter table annotation_databases add ucsc_reference text;"

psql -U portaladmin portal_prod -c "update annotation_databases  set ucsc_reference = 'hg38' where an_database_id = 59;"
psql -U portaladmin portal_prod -c "update annotation_databases  set ucsc_reference = 'mm10' where an_database_id = 60;"
psql -U portaladmin portal_prod -c "update annotation_databases  set ucsc_reference = 'mm9' where an_database_id = 46;"
psql -U portaladmin portal_prod -c "update annotation_databases  set ucsc_reference = 'hg19' where an_database_id = 56;"
# psql -U portaladmin portal_prod -c "UPDATE stemformatics.configs set ref_id = 'false' where ref_type = 'production';"
# galaxy server url and key are not included(they are confidential); the information can be found on agile.org or by consulting Isha

psql -U portaladmin portal_prod -c "insert into stemformatics.configs values(default,'url_for_qc_failure_rates','https://agile.stemformatics.org/agile_org/datasets/failure_rates');"
