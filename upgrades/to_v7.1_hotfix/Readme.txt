# Two of the atla files have been updated for this hotfix 
# imac_atlas_annotations.tsv (one sample in the atlas changed from "ex vivo" to "in vitro"):
scp portaladmin@prod-dp.stemformatics.org:/var/www/stemformaticsdataportal/data/atlas/imac_atlas_annotations.tsv /var/www/pylons-data/prod/PCAFiles/atlas

# imac_atlas_colours.tsv: Copy and paste the following line into the corresponding lines in this file.
neutrophil	#5e2f0d
granulocyte	#5e2f0d
myelocyte	#8b4513
metamyelocyte	#8b4513
promyelocyte	#8b4513

# No other data changed for this update. Only code changes for the atlas page to make a few minor adjustments to the user-friendliess.
