# This version introduces the imac atlas. This requires new text files.
# The sources of these files are the data portal at prod-dp.stemformatics.org, and this script will copy these file from there.
#
# So before running this script, ensure that these files exist at prod-dp. In particular,run the script which saves atlas annotation files
# from sql tables:
# 
# [jarny@prod-dp stemformaticsdataportal]$ cd /var/www/stemformaticsdataportal
# [jarny@prod-dp stemformaticsdataportal]$ source /mnt/data/opt/conda/bin/activate dataportal
# (dataportal) [jarny@prod-dp stemformaticsdataportal]$ python dataportal/scripts/save_atlas_annotation_files.py
#
# Then run the following commands as user portaladmin 
# (could just run this script but it's probably safer to run individual command here in case errors are encountered).
mkdir -p /var/www/pylons-data/prod/PCAFiles/atlas
scp portaladmin@prod-dp.stemformatics.org:/var/www/stemformaticsdataportal/data/atlas/*.tsv /var/www/pylons-data/prod/PCAFiles/atlas

# Now we need to perform some checks and run functions that create redis entries as well as save coordinate files
source /var/www/pyramid/virtualenv/bin/activate
cd /data/repo/git-working/s4m_pyramid/
nosetests -s S4M_pyramid/model/stemformatics_data/atlas.py:checkInputFiles
nosetests -s S4M_pyramid/model/stemformatics_data/atlas.py:setExpressionValuesInRedis
nosetests -s S4M_pyramid/model/stemformatics_data/atlas.py:saveCoordinateFiles
