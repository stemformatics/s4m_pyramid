We also had to install pandas into the virtual env - ensure that it's this version:
(virtualenv) [portaladmin@w2-s4m jarny]$ pip install --upgrade pip
(virtualenv) [portaladmin@w2-s4m jarny]$ pip install pandas==0.20.3
(virtualenv) [portaladmin@w2-s4m s4m_pyramid]$ pip install sklearn

