#!/bin/sh

## This DB update prevents Human haplotype genes (Ens v91) from coming back in gene autocomplete
## and other searches in Stemformatics.
## This does not prevent users from looking up genes directly by their Ensembl ID, so these genes
## are still accessible.

## Dump genes to check that we got the right ones (can be redirected to a file)
psql -U portaladmin portal_prod -c "copy (select chromosome_name,gene_id,associated_gene_name,description from genome_annotations where db_id=59 and chromosome_name like '%MHC%' order by associated_gene_name) to stdout" | wc -l
## 2022

## Remove fts_lexemes for those genes
psql -U portaladmin portal_prod -c "update genome_annotations set fts_lexemes='' where db_id=59 and chromosome_name like '%MHC%'"
## 2022
