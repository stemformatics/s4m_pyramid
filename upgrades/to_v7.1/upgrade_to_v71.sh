#!/bin/bash

## NOTE: Must run the other scripts in this directory BEFORE this one i.e. do this one last!

psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.1' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
## NOTE: We did not modify any assets for this release, so we can re-use v7.0.1 CDN bucket. This is intentional.
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_701' where ref_type = 'cdn_base_url';"

wget http://localhost:5000/api/trigger_config_update -O /dev/null

echo "Now you will need to upgrade the help to v7.1"

