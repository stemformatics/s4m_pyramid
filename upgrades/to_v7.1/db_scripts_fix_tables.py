# In this release, we are updating biosamples_metadata table in s4m with those that come from the "dataportal" db at prod-dp.stemformatics.org.
# (Initially I was thinking that datasets and dataset_metadata also had to be updated, but realised they were never changed in the data portal - Jarny).
# The following steps are used to achieve this.
#
# 1. Make a copy of biosamples_metadata table to another temporary table and dump it.
# [jarny@prod-dp ~]$ psql dataportal -U portaladmin
# dataportal=# create table biosamples_metadata_from_dp as select * from biosamples_metadata ;
# 
# [jarny@prod-dp ~]$ pg_dump dataportal -U portaladmin -t biosamples_metadata_from_dp > /var/www/stemformaticsdataportal/data/sql_dumps/biosamples_metadata_from_dp.sql
#
# 2. Transfer this file (as portaladmin user, as ssh keys have been set up) and recreate them locally.
# scp portaladmin@prod-dp.stemformatics.org:/var/www/stemformaticsdataportal/data/sql_dumps/biosamples_metadata_from_dp.sql /tmp
# psql -U portaladmin portal_prod < /tmp/biosamples_metadata_from_dp.sql
#
# 3. Delete all rows from this new table where it's common with biosamples_metadata.
# [portaladmin@w1-s4m admin]$ psql portal_prod -U portaladmin
# portal_prod=# delete from biosamples_metadata_from_dp t1 using (select * from biosamples_metadata_from_dp intersect select * from biosamples_metadata) t2 where t1.chip_type=t2.chip_type and t1.chip_id=t2.chip_id and t1.md_name=t2.md_name and t1.md_value=t2.md_value and t1.ds_id=t2.ds_id;
# 
# So this table now contains information about the rows which will be changed. Eg: datasets which will be affected:
# portal_prod=# select count(distinct ds_id) from biosamples_metadata_from_dp ;
# 
# 4. Update biosamples_metadata using this table.
# Make a copy of biosamples_metadata in current state in case we want to make comparisons, etc:
# portal_prod=# create table biosamples_metadata_copy as table biosamples_metadata;
#
# Now update matching rows
# portal_prod=# update biosamples_metadata t1 set md_value=t2.md_value from biosamples_metadata_from_dp t2  where t1.chip_type=t2.chip_type and t1.chip_id=t2.chip_id and t1.md_name=t2.md_name and t1.ds_id=t2.ds_id;
# 
# Also need to add new rows
# portal_prod=# insert into biosamples_metadata select * from biosamples_metadata_from_dp except select * from biosamples_metadata;
# 
# 5. Fix some bad data entries in biosamples_metadata by running fixBadData() function below
#
# 6. Fix dataset_metadata table by running fixDatasetMetadata() function below.

import psycopg2, subprocess, json

# ----------------------------------------------------------
# Private functions
# ----------------------------------------------------------
def _runSql(sql, data=None, type="select", printSql=False):
    """Run sql statement.

    Example:
    > result = _runSql("select * from users where email=%s", (email,))
    > [('jarnyc@unimelb.edu.au', 'dofdlfjlejjce', 'admin')]

    data should be a tuple, even if one element.
    type should be one of {"select","update"}

    Returns a list of tuples corresponding to the columns of the selection if type=="select".
    If type=="update", returns the number of rows affected by the update.

    If printSql is True, then the actual sql being executed will be printed
    """
    conn = psycopg2.connect("dbname=portal_prod user=portaladmin")

    cursor = conn.cursor()

    if printSql:  # To see the actual sql executed, use mogrify:
        print(cursor.mogrify(sql, data))
        
    cursor.execute(sql, data)

    if type=="select":
        result = cursor.fetchall()
    elif type=="update":
        result = cursor.rowcount
        conn.commit()  # doesn't update the database permanently without this

    cursor.close()
    conn.close()
    return result

def create_biosamples_metadata_from_dp():
    # Don't run this if you have manually run steps 1 to 4 above
    subprocess.call("psql -U portaladmin portal_prod < /tmp/biosamples_metadata_from_dp.sql", shell=True)
    _runSql("delete from biosamples_metadata_from_dp t1 using (select * from biosamples_metadata_from_dp intersect select * from biosamples_metadata) t2 where t1.chip_type=t2.chip_type and t1.chip_id=t2.chip_id and t1.md_name=t2.md_name and t1.md_value=t2.md_value and t1.ds_id=t2.ds_id", type="update")
    _runSql("create table biosamples_metadata_copy as table biosamples_metadata", type="update")
    _runSql("update biosamples_metadata t1 set md_value=t2.md_value from biosamples_metadata_from_dp t2  where t1.chip_type=t2.chip_type and t1.chip_id=t2.chip_id and t1.md_name=t2.md_name and t1.ds_id=t2.ds_id", type="update")
    _runSql("insert into biosamples_metadata select * from biosamples_metadata_from_dp except select * from biosamples_metadata", type="update")

def fixBadData():
    # Found some bad entries in biosamples_metadata_from_dp - not crucial but could run this to fix
    # There were only 6 matching rows for this field in dataset_metadata + no reference in the code, so safe to delete these rows:
    sqls = """delete from dataset_metadata where ds_name='lineGraphSampleOrder';
    update biosamples_metadata_from_dp set md_value=E'Advanced DMEM, 10% FCS, 50 ng/mL M-CSF, 25 ng/mL IL-3, 2 mM L-glutamine, 100 U/mL penicillin, 100 μg/mL streptomycin, 0.055 mM β-mercaptoethanol' where md_value like 'Advanced DMEM,%' and ds_id=6003;
    delete from biosamples_metadata_from_dp where ds_id=1;
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=6003 and md_value like 'CL%';
    update biosamples_metadata_from_dp set md_value='null' where md_name='Generic sample type' and ds_id=6037;
    update biosamples_metadata_from_dp set md_value='B cell' where md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6213 and md_name='Generic sample type long' and md_value='B cell');
    update biosamples_metadata_from_dp set md_value='T cell' where md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6213 and md_name='Generic sample type long' and md_value='T cell');
    update biosamples_metadata_from_dp set md_value='monocyte' where md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6213 and md_name='Generic sample type long' and md_value='monocyte');
    update biosamples_metadata_from_dp set md_value='natural killer cell' where ds_id=1000 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=1000 and md_name='Generic sample type' and md_value='natural killer cell');
    update biosamples_metadata_from_dp set md_value='lymphocyte' where ds_id=1000 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=1000 and md_name='Generic sample type' and md_value='lymphocyte');
    update biosamples_metadata_from_dp set md_value='megakaryocyte' where ds_id=1000 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=1000 and md_name='Generic sample type' and md_value='megakaryocyte');
    update biosamples_metadata_from_dp set md_value='granulocyte' where ds_id=1000 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=1000 and md_name='Generic sample type' and md_value='granulocyte');
    update biosamples_metadata_from_dp set md_value='erythroblast' where ds_id=1000 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=1000 and md_name='Generic sample type' and md_value='erythroblast');
    update biosamples_metadata_from_dp set md_value='monocyte' where ds_id=1000 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=1000 and md_name='Generic sample type' and md_value='monocyte');
    update biosamples_metadata_from_dp set md_value='stem progenitor cell' where ds_id=6326 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6326 and md_name='Generic sample type' and md_value='stem progenitor cell');
    update biosamples_metadata_from_dp set md_value='stem progenitor cell' where ds_id=6358 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6358 and md_name='Generic sample type' and md_value='stem progenitor cell');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=6358 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6358 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=6463 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6463 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=6612 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6612 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=6638 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6638 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=6741 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=6741 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=7002 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=7002 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=7003 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=7003 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=7268 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=7268 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=7283 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=7283 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='myeloid' where ds_id=7288 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=7288 and md_name='Generic sample type' and md_value='myeloid');
    update biosamples_metadata_from_dp set md_value='T cell' where ds_id=7379 and md_name='Generic sample type' and chip_id in (select chip_id from biosamples_metadata where ds_id=7379 and md_name='Generic sample type' and md_value='T cell');""".split("\n")
    for sql in sqls:
        _runSql(sql, type="update")

def fixDatasetMetadata():
    # First get a list of all dataset ids affected
    datasets = [item[0] for item in _runSql("select distinct ds_id from biosamples_metadata_from_dp order by ds_id")]
    print("Datasets affected", len(datasets))

    # All fields affected
    #print("All fields affected", [item[0] for item in _runSql("select distinct md_name from biosamples_metadata_from_dp order by md_name")], "\n")
    # It's not likely that many of these fields are used anywhere else in the application. So just focus on these
    # md_names = ['Final cell type', 'Generic sample type', 'Organism', 'Parental cell type', 'Replicate Group ID', 'Sample Type']
    md_names = ['Sample Type']
    
    # All dataset_metadata fields
    #print("All dataset_metadata fields", [item[0] for item in _runSql("select distinct ds_name from dataset_metadata order by ds_name")])
    # We don't need to look through all these fields, so make a subset of fields that we should look inside:
    dataset_metadata_fields = ['lineGraphOrdering', 'sampleTypeDisplayGroups', 'sampleTypeDisplayOrder']

    for datasetIndex,ds_id in enumerate(datasets):
        # Loop through all md_names and find all matching rows for each field
        for md_name in md_names:
            changes = set()
            # find all combination of chip_id and new value for this md_name
            for item in _runSql("select chip_id,md_value from biosamples_metadata_from_dp where ds_id=%s and md_name=%s order by md_value", (ds_id,md_name)):
                chip_id, newValue = item[0], item[1]
                # what was the old value?
                result = _runSql("select md_value from biosamples_metadata_copy where ds_id=%s and md_name=%s and chip_id=%s", (ds_id,md_name,chip_id))
                if len(result)==0:  # new value created?
                    print("\t".join(map(str, ["### New value not matching old", ds_id, datasetIndex, chip_id, md_name, newValue])))
                    return
                oldValue = result[0][0]
                # so we just want a unique set of old to new
                changes.add((oldValue, newValue))  # eg. {('CD166b+', 'CD66b+'),...}

            if (len(dict(changes))==len(changes)):
                newValue = dict(changes)

                # Now for each possibly affected field of dataset_metadata, see if any of the values feature oldValues
                for field in dataset_metadata_fields:
                    result = _runSql("select ds_value from dataset_metadata where ds_id=%s and ds_name=%s", (ds_id, field))
                    if len(result)==0 or result[0][0]=='' or result[0][0].lower()=="null": continue
                    value = result[0][0]
                    updateValue = None
                    if field in ["lineGraphOrdering", "lineGraphSampleOrder", "sampleTypeDisplayGroups"]:  # in dictionary form
                        try:
                            value = json.loads(value)
                        except:
                            print('\t'.join(map(str, ["### Trouble decoding JSON", ds_id, field])))
                            continue
                        # change any of the key to the new value
                        newVals = dict(zip([newValue.get(key,key) for key in value.keys()],value.values()))
                        if value!=newVals: # update this field
                            updateValue = json.dumps(newVals)
                    elif field=="sampleTypeDisplayOrder": # comma separated form
                        newVals = [newValue.get(item,item) for item in value.split(",")]
                        if newVals!=value.split(","):
                            updateValue = ",".join(newVals)
                    else:   # no other fields for now
                        continue
                    if updateValue is not None:
                        print('\t'.join(map(str, [ds_id, field, md_name, value, updateValue])))
                        print("### updating ", ds_id, field, _runSql("update dataset_metadata set ds_value=%s where ds_id=%s and ds_name=%s", (updateValue, ds_id, field), type="update"))
            else:
                print('\t'.join(map(str, ["### Duplicate changes found, ds_id=", ds_id, datasetIndex, md_name])))
                print('\n'.join(['%s\t%s' % item for item in sorted(changes)]))

def main():
    #create_biosamples_metadata_from_dp()
    fixBadData()
    fixDatasetMetadata()


if __name__ == "__main__":
    main()