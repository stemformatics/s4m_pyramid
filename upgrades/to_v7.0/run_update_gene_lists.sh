#!/bin/bash
source /var/www/pylons/virtualenv/bin/activate

psql -U portaladmin portal_prod -c "truncate stemformatics.private_gene_sets_update_archive;"

psql -U portaladmin portal_prod -c "alter table stemformatics.private_gene_sets_update_archive add new_db_id text;"

psql -U portaladmin portal_prod -c "alter table stemformatics.private_gene_sets_update_archive add old_db_id text;"

python update_gene_lists_in_pyramid.py "homo_sapiens_core_69_37_to_homo_sapiens_core_91_38_final_mapping.tsv" 56 59 > update_gene_lists_in_pyramid_human.log

# python update_gene_lists_in_pyramid.py "mus_musculus_core_67_37_to_mus_musculus_core_91_38_final_mapping.tsv" 46 60 > update_gene_lists_in_pyramid_mouse.log
