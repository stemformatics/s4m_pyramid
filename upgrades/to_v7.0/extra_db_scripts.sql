
INSERT INTO stemformatics.configs (ref_type,ref_id) values('debug','false');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('gene_mapping_raw_file_base_name','/var/www/pylons-data/prod/gene_mapping.raw');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('feature_mapping_raw_file_base_name','/var/www/pylons-data/prod/feature_mapping.raw');
Insert into stemformatics.configs (ref_type,ref_id) values('secret_hash_parameter_for_unsubscribe', 'I LOVE WY');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('StemformaticsQueue','/var/www/pylons-data/prod/jobs/StemformaticsQueue/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetGCTFiles','/var/www/pylons-data/prod/GCTFiles/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetCLSFiles','/var/www/pylons-data/prod/CLSFiles/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('GPQueue','/var/www/pylons-data/prod/jobs/GPQueue/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('url_for_qc_failure_rates','https://agile.stemformatics.org/agile_org/datasets/failure_rates');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetTempGCTFiles','/mnt/data/tmp/tmp_dataset_download/');

Update stemformatics.configs set ref_id = 59 where ref_type = 'human_db';
Update stemformatics.configs set ref_id = 'Ensembl v69 - GRCh37' where ref_type = 'old_human_ensembl_version_text';
Update stemformatics.configs set ref_id = 'Ensembl v91 - GRCh38' where ref_type = 'current_human_ensembl_version_text';
Update datasets set db_id=59 where data_type_id=1 and mapping_id in (6,7,16,21,36,46,51,56,61,66,71,73,114,119,120,127,134,136,142,143,178,184,199,204,225,234);

update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7217;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7201;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 6612;
update dataset_metadata set ds_name = 'ShowIscandarLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7179;
update dataset_metadata set ds_name = 'ShowIscandarLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7298;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7132;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7073;
update dataset_metadata set ds_name = 'ShowIscandarLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7297;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7218;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7209;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7087;
update dataset_metadata set ds_name = 'ShowIscandarLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7182;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7260;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 7134;
update dataset_metadata set ds_name = 'ShowGlimmaLinksOnDatasetSummaryPage' where (ds_name = 'showReportOnDatasetSummaryPage' ) and ds_id = 6943 and ds_value ilike '%DEG%';

update datasets set mapping_id=239 where id=7073;
update datasets set db_id=57 where id=7073;
update datasets set db_id=57 where id=6995;
update datasets set mapping_id=239 where id=7087;
update datasets set db_id=57 where id=7087;
update datasets set mapping_id=239 where id=7134;
update datasets set db_id=57 where id=7134;
update datasets set mapping_id=239 where id=7179;
update datasets set db_id=57 where id=7179;
update datasets set mapping_id=239 where id=7132;
update datasets set db_id=57 where id=7132;


--This is to update Parental cell type and Final cell type fields

-- to check different values for Parental cell type

Select substring(md_value for 100),md_name,count(*) from biosamples_metadata where md_name = 'Parental cell type' group by md_value, md_name order by count desc;

-- To combine all null values together for Parental cell type under ‘null’
Update biosamples_metadata SET md_value='null' WHERE md_name='Parental cell type' and md_value='NULL';

-- To combine all monocyte values together for Parental cell type
Update biosamples_metadata SET md_value='monocyte' WHERE md_name='Parental cell type' and(md_value='moncyte' or md_value='Monocyte');

-- To combine all fibroblast values together for Parental cell type under
Update biosamples_metadata SET md_value='fibroblast' WHERE  md_name='Parental cell type' and (md_value ilike 'Fibroblast%' or md_value like 'tail tip%');

-- To combine all macrophage values together for Parental cell type under ‘macrophage’
Update biosamples_metadata SET md_value='macrophage' WHERE  md_name='Parental cell type' and md_value='Macrophage';

-- To combine all embryonic stem cell values together for Parental cell type
Update biosamples_metadata SET md_value='embryonic stem cell' WHERE md_name='Parental cell type' and (md_value='ESC' or md_value='hESC' or md_value ilike 'hESC%' or md_value like 'embryonic stem cell line%');

-- To combine all induced pluripotent stem cell values together for Parental cell type
Update biosamples_metadata SET md_value='induced pluripotent stem cell' WHERE  md_name='Parental cell type' and (md_value='iPSC' or md_value ilike 'hiPSC%');

-- To combine all granulocyte for Parental cell type
Update biosamples_metadata SET md_value='granulocyte' WHERE md_name='Parental cell type' and md_value='Granulocyte';

-- To combine all mesenchymal stromal cell

Update biosamples_metadata SET md_value='mesenchymal stromal cell' WHERE md_name='Parental cell type' and md_value='MSC';

-- To combine all B cell for Parental cell type
Update biosamples_metadata SET md_value='B cell'  WHERE md_name='Parental cell type' and md_value='B-cell';

--To combine all T cell for parental cell type
Update biosamples_metadata SET md_value='T cell' WHERE  md_name='Parental cell type' and (md_value='T-cell');

-- To combine all bone marrow cell

Update biosamples_metadata SET md_value='bone marrow cell' WHERE md_name='Parental cell type' and (md_value='bone-marrow' or md_value='bone marrow' or md_value='Bone marrow');

-- To combine all peripheral blood mononuclear cell

Update biosamples_metadata SET md_value='peripheral blood mononuclear cell' WHERE md_name='Parental cell type' and (md_value='Peripheral blood monocytes');

--To combine all microglial cell

Update biosamples_metadata SET md_value='microglial cell' WHERE md_name='Parental cell type' and (md_value='microglia');

--To verify the changes after combing values for parental cell type
Select substring(md_value for 100),md_name,count(*) from biosamples_metadata where md_name = 'Parental cell type' group by md_value, md_name order by count desc;

-- to check different values for Final cell type

Select substring(md_value for 100),md_name,count(*) from biosamples_metadata where md_name = 'Final cell type' group by md_value, md_name order by count desc;


-- To combine all null values together for Final cell type under ‘null’
Update biosamples_metadata SET md_value='null' WHERE md_name='Final cell type' and md_value='NULL';

-- To combine all mesenchymal stromal cell for Final cell type

Update biosamples_metadata SET md_value='mesenchymal stromal cell' WHERE md_name='Final cell type' and md_value='MSC';

-- To combine all induced pluripotent stem cell values together for Final cell type
Update biosamples_metadata SET md_value='induced pluripotent stem cell' WHERE  md_name='Final cell type' and (md_value='iPSC');

-- To combine all endothelial cell values together for Final cell type
Update biosamples_metadata SET md_value='endothelial cell' WHERE  md_name='Final cell type' and (md_value='Endothelial cells');

-- To combine all blood cell values together for Final cell type
Update biosamples_metadata SET md_value='blood cell' WHERE  md_name='Final cell type' and (md_value='blood' or md_value='Peripheral blood');

-- To combine all embryoid body values together for Final cell type
Update biosamples_metadata SET md_value='embryoid body' WHERE  md_name='Final cell type' and (md_value='EB');

-- To combine all neuron values together for Final cell type
Update biosamples_metadata SET md_value='neuron' WHERE  md_name='Final cell type' and (md_value='neurons');

-- To combine all bone marrow cell values together for Final cell type
Update biosamples_metadata SET md_value='bone marrow cell' WHERE  md_name='Final cell type' and (md_value='Bone Marrow');

-- To combine all hematopoietic stem cell values together for Final cell type
Update biosamples_metadata SET md_value='hematopoietic stem cell' WHERE  md_name='Final cell type' and (md_value='HSC');


--To verify the changes after combing values
Select substring(md_value for 100),md_name,count(*) from biosamples_metadata where md_name = 'Final cell type' group by md_value, md_name order by count desc;


--to clean Generic sample type

-- To get list of Generic sample type when the value of Final cell type is null

select md_value,md_name, count(*) from biosamples_metadata where md_name = 'Generic sample type' and (chip_id,ds_id) in (select chip_id,ds_id from biosamples_metadata where md_name = 'Final cell type' and md_value ='null') group by md_value, md_name order by count desc;

--To combine all ‘mesenchymal stromal cell’ for Generic sample type

Update biosamples_metadata SET md_value='mesenchymal stromal cell' WHERE md_name='Generic sample type' and md_value='MSC';

-- To combine all induced pluripotent stem cell values together for Generic sample type
Update biosamples_metadata SET md_value='induced pluripotent stem cell' WHERE  md_name='Generic sample type' and (md_value='iPSC' or md_value='ipsc');

-- To combine all embryonic stem cell values together for Generic sample type
Update biosamples_metadata SET md_value='embryonic stem cell' WHERE  md_name='Generic sample type' and (md_value='ESC');

-- To combine all T-cell values together for Generic sample type
Update biosamples_metadata SET md_value='T cell' WHERE  md_name='Generic sample type' and (md_value='T-cell');

-- To combine all blood cell values together for Generic sample type
Update biosamples_metadata SET md_value='blood cell' WHERE  md_name='Generic sample type' and (md_value='Blood' or md_value='blood');

-- To combine all dendritic cell values together for Generic sample type
Update biosamples_metadata SET md_value='dendritic cell' WHERE  md_name='Generic sample type' and (md_value='DC');

-- To combine all hematopoietic stem cell values together for Generic sample type
Update biosamples_metadata SET md_value='hematopoietic stem cell' WHERE  md_name='Generic sample type' and (md_value='HSC');

-- To combine all hematopoietic stem cell values together for Generic sample type
Update biosamples_metadata SET md_value='peripheral blood mononuclear cell' WHERE  md_name='Generic sample type' and (md_value='PBMC');

-- To combine all hematopoietic stem cell values together for Generic sample type
Update biosamples_metadata SET md_value='acute myeloid leukemia cell' WHERE  md_name='Generic sample type' and (md_value='AML');

-- To see changes in Generic sample type when the value of Final cell type is null

select md_value,md_name, count(*) from biosamples_metadata where md_name = 'Generic sample type' and (chip_id,ds_id) in (select chip_id,ds_id from biosamples_metadata where md_name = 'Final cell type' and md_value ='null') group by md_value, md_name order by count desc;


