import sys
import psycopg2
import psycopg2.extras



# python update_gene_lists_in_pyramid.py <input_file> <old_db_id> <new_db_id>
# python update_gene_lists_in_pyramid.py "homo_sapiens_core_69_37_to_homo_sapiens_core_91_38_final_mapping.tsv" 56 59
# python update_gene_lists_in_pyramid.py "mus_musculus_core_67_37_to_mus_musculus_core_91_38_final_mapping.tsv" 46 60


# Takes 26 minutes without creating a archive record or mapping a replacement.
# Takes 46 minutes when creating new mappings and creating archives of non-mapped
# Just need to delete old gene_ids from gene_set_items
# Also need to test using mouse

mapping_dictionary = {}
conn_string="host='localhost' dbname='portal_prod' user='portaladmin'"

input_file ='homo_sapiens_core_69_37_to_homo_sapiens_core_91_38_final_mapping.tsv'
old_db_id = 56 #46 for mouse
new_db_id = 59 # 60 for mouse


input_file ='mus_musculus_core_67_37_to_mus_musculus_core_91_38_final_mapping.tsv'
old_db_id = 46 
new_db_id = 60 

input_file = sys.argv[1]
old_db_id = sys.argv[2]
new_db_id = sys.argv[3]



f = open(input_file, 'r')


for line in f:
    temp_line = line.replace("\n","")
    temp_list = temp_line.split("\t") 
    old_gene_id = temp_list[0]
    new_gene_id = temp_list[1]

    # ignore genes that stay the same
    if old_gene_id == new_gene_id:
        continue

    if old_gene_id not in mapping_dictionary:
        mapping_dictionary[old_gene_id] = []
   
    if new_gene_id != '':
        mapping_dictionary[old_gene_id].append(new_gene_id)

    #print(mapping_dictionary[old_gene_id]) 

f.close()

for old_gene_id in mapping_dictionary:

    number_of_mappings = len(mapping_dictionary[old_gene_id])

    if number_of_mappings == 0:
        print("empty",number_of_mappings,old_gene_id,mapping_dictionary[old_gene_id])
        print("get all gene_list_items that are using this old_gene_id")

        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute("select * from stemformatics.gene_set_items where gene_id = %(gene_id)s;",{'gene_id':old_gene_id})
        # retrieve the records from the database
        result = cursor.fetchall()
        cursor.close()
        conn.close()

        if len(result) > 0:
            conn = psycopg2.connect(conn_string)
            cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cursor.execute("select associated_gene_name from genome_annotations where gene_id = %(gene_id)s and db_id = %(db_id)s;",{'gene_id':old_gene_id,'db_id':old_db_id})
            # retrieve the records from the database
            gene_result = cursor.fetchall()
            cursor.close()
            conn.close()

            try:
                old_ogs = gene_result[0][0] 
            except:
                old_ogs = 'Error in finding ogs'


        for gene_set_item in result:
            gene_set_item_id = gene_set_item[0]
            gene_set_id = gene_set_item[1]
            gene_id = gene_set_item[2]

            # old_db_id and new_db_id  set at top
            status = 'retired'
            uid = 0
            print(gene_set_item)


            # Not going to be used anymore
            gene_set_name = 'to be removed'
            user_name = 'to be removed'
            species_db_id = new_db_id
            old_ens_id = gene_id
            new_ens_id = 'to be removed'
            old_ens_version = 0
            new_ens_version = 0
            new_ogs = 'to be removed'
            old_entrez_id = 0
            new_entrez_id = 0
            s4m_update_version = 0

            conn = psycopg2.connect(conn_string)
            cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

            sql = "select gene_set_name from stemformatics.gene_sets where id = %(gene_set_id)s;"
            variables = {'gene_set_id':gene_set_id}

            cursor.execute(sql,variables)
            gene_set_name_raw = cursor.fetchall()

            gene_set_name = gene_set_name_raw[0][0]

            sql = "insert into stemformatics.private_gene_sets_update_archive values (DEFAULT,%(gene_set_id)s,%(gene_set_name)s,%(user_id)s,%(user_name)s,%(species_db_id)s,%(old_ens_id)s,%(new_ens_id)s,%(ens_id_status)s,%(old_ens_version)s,%(new_ens_version)s,%(old_ogs)s,%(new_ogs)s,%(old_entrez_id)s,%(new_entrez_id)s,%(s4m_update_version)s,%(new_db_id)s,%(old_db_id)s);"

            variables = {'gene_set_id':gene_set_id,'gene_set_name':gene_set_name,'user_id':uid,'user_name':user_name,'species_db_id':species_db_id,'old_ens_id':old_ens_id,'new_ens_id':new_ens_id,'ens_id_status':status,'old_ens_version':old_ens_version,'new_ens_version':new_ens_version,'old_ogs':old_ogs,'new_ogs':new_ogs,'old_entrez_id':old_entrez_id,'new_entrez_id':new_entrez_id,'s4m_update_version':s4m_update_version,'new_db_id':new_db_id,'old_db_id':old_db_id}

            raw_sql = cursor.mogrify(sql,variables)
            print(raw_sql)
            cursor.execute(sql,variables)

            sql = "update stemformatics.gene_sets set needs_attention = True where id = %(gene_set_id)s;"
            variables = {'gene_set_id':gene_set_id}

            cursor.execute(sql,variables)

            conn.commit()
            cursor.close()
            conn.close()

            print("create a private_gene_sets_update_archive for all the gene_list_items")
            print("delete original gene_list_item")
            print("record the details in a log file")
    else:
        print("lots-",number_of_mappings,old_gene_id,mapping_dictionary[old_gene_id])


        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute("select * from stemformatics.gene_set_items where gene_id = %(gene_id)s;",{'gene_id':old_gene_id})
        # retrieve the records from the database
        result = cursor.fetchall()
        cursor.close()
        conn.close()

        for gene_set_item in result:
            gene_set_item_id = gene_set_item[0]
            gene_set_id = gene_set_item[1]
            old_gene_id = gene_set_item[2]
    
            for new_gene_id in mapping_dictionary[old_gene_id]:
                print(new_gene_id)

                try:
                    conn = psycopg2.connect(conn_string)
                    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
                    sql="insert into stemformatics.gene_set_items values (DEFAULT,%(gene_set_id)s,%(gene_id)s);"
                    variables = {'gene_set_id':gene_set_id,'gene_id':new_gene_id}
                    print(cursor.mogrify(sql,variables))
                    cursor.execute(sql,variables)
                    conn.commit()
                    cursor.close()
                    conn.close()
                    print("create new gene_list_item for new_gene_id")
                except:
                    print("error in duplication occured most likely. due to the finicky nature of this situation.")
            print("delete original gene_list_item")
        print("record the details in a log file")


try:
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql="update stemformatics.gene_sets set db_id = %(new_db_id)s where db_id = %(old_db_id)s;"
    variables = {'new_db_id':new_db_id,'old_db_id':old_db_id}
    cursor.execute(sql,variables)
    conn.commit()
    cursor.close()
    conn.close()
except:
    print("Error in updating db_id for gene_sets")

print("Right at the end")

