#!/bin/bash

cd /data/repo/git-working/s4m_pyramid/upgrades/to_v7.0
./collapse_v70_scripts.sh

psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.0' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_70' where ref_type = 'cdn_base_url';"

psql -U portaladmin -d portal_prod < /tmp/genome_annotations_old.dump
./run_update_gene_lists.sh
psql -U portaladmin -d portal_prod < /tmp/genome_annotations_new.dump

wget http://localhost:5000/api/trigger_config_update -O /dev/null

echo "Now you will need to upgrade the help to v7.0"
