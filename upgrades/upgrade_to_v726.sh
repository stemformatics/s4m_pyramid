#!/bin/bash
# Run from git repo dir.

source /var/www/pyramid/virtualenv/bin/activate

# Version change
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.2.6' where ref_type = 'stemformatics_version';"

# Copy the v7.2 of myeloid atlas annotations file from www4
cd /var/www/pylons-data/prod/PCAFiles/atlas
mv myeloid_atlas_annotations.tsv myeloid_atlas_annotations.v7.1.tsv
scp www4.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/atlas/myeloid_atlas_annotations.v7.2.tsv .
ln -s myeloid_atlas_annotations.v7.2.tsv myeloid_atlas_annotations.tsv