
-- Story #159 for annotating gene sets
CREATE TABLE stemformatics.transcript_annotations
(
   db_id INT REFERENCES annotation_databases (an_database_id),
   transcript_id text NOT NULL,
   gene_id text NOT NULL,
   transcript_name text NOT NULL,
   transcript_start integer NOT NULL,
   transcript_end integer NOT NULL,
   protein_length integer NOT NULL default 0,
   signal_peptide boolean default False,
   tm_domain boolean default False,
   targeted_miRNA boolean default False,
   CONSTRAINT transcript_annotations_pkey PRIMARY KEY (db_id, transcript_id)
);


CREATE INDEX transcript_annotations_db_id_gene_id ON stemformatics.transcript_annotations (db_id, gene_id);


-- Story #159 for saving filters for annotations
CREATE TABLE stemformatics.annotation_filters
(
   id serial NOT NULL,
   uid INT NOT NULL,
   name text NOT NULL,
   json_filter text NOT NULL,
   CONSTRAINT annotation_filters_pkey PRIMARY KEY (id)
);

CREATE INDEX annotation_filters_uid ON stemformatics.annotation_filters (uid);



update stemformatics.transcript_annotations set size = transcript_end - transcript_start;




-- ensure gene set items has index on gene id
CREATE INDEX gene_set_items_gene_id ON stemformatics.gene_set_items (gene_id);


-- have to remove transcript start/end/size details as they are useless
alter table stemformatics.transcript_annotations drop column transcript_start;
alter table stemformatics.transcript_annotations drop column transcript_end;


-- gene sets adding the type 
ALTER TABLE stemformatics.gene_sets ADD COLUMN gene_set_type text NOT NULL default '';



-- speeding up the annotation page
CREATE INDEX gene_sets_uid_id ON stemformatics.gene_sets (uid,id);
