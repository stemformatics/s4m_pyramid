ALTER TABLE stemformatics.gene_sets DROP CONSTRAINT unique_gene_sets;
ALTER TABLE stemformatics.gene_sets ADD CONSTRAINT unique_gene_sets UNIQUE (gene_set_name,uid);
