-- Task #37

ALTER TABLE stemformatics.probe_expressions_avg_replicates ADD COLUMN sample_size integer default 1;

-- 2000 should be the hONS data, every other dataset should have a sample size of 1
update stemformatics.probe_expressions_avg_replicates pe set sample_size = 1 where ds_id != 2000;

--  update dataset 2000
update stemformatics.probe_expressions_avg_replicates pe set sample_size = (select count(md_value) from biosamples_metadata where md_value = pe.replicate_group_id) where ds_id = 2000 and chip_type = 21;
