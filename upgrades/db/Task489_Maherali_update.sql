update biosamples_metadata set md_value = replace(md_value,';',' ') where ds_id = 5027 and md_name = 'Replicate Group ID';
update stemformatics.probe_expressions_avg_replicates set replicate_group_id = replace(replicate_group_id,';',' ') where ds_id = 5027; 
