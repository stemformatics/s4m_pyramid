#!/bin/bash
# Run from git repo dir.

source /var/www/pyramid/virtualenv/bin/activate

# Version change
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.2.5' where ref_type = 'stemformatics_version';"
