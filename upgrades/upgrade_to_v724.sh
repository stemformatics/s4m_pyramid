#!/bin/bash
# Run from git repo dir.

source /var/www/pyramid/virtualenv/bin/activate

# Install matplotlib
# pip install matplotlib

# Change filenames
mv /var/www/pylons-data/prod/PCAFiles/atlas/imac_atlas_annotations.tsv /var/www/pylons-data/prod/PCAFiles/atlas/myeloid_atlas_annotations.tsv
mv /var/www/pylons-data/prod/PCAFiles/atlas/imac_atlas_colours.tsv /var/www/pylons-data/prod/PCAFiles/atlas/myeloid_atlas_colours.tsv
mv /var/www/pylons-data/prod/PCAFiles/atlas/imac_atlas_coordinates.tsv /var/www/pylons-data/prod/PCAFiles/atlas/myeloid_atlas_coordinates.tsv
mv /var/www/pylons-data/prod/PCAFiles/atlas/imac_atlas_expression.tsv /var/www/pylons-data/prod/PCAFiles/atlas/myeloid_atlas_expression.tsv
mv /var/www/pylons-data/prod/PCAFiles/atlas/imac_atlas_genes.tsv /var/www/pylons-data/prod/PCAFiles/atlas/myeloid_atlas_genes.tsv

# Create filtered version of the atlas expression files.
python -m S4M_pyramid.model.stemformatics_data.atlas saveFilteredExpressionFiles /data/redis/redis.sock /var/www/pylons-data/prod/PCAFiles/atlas

# Add values to redis under new key name
python -m S4M_pyramid.model.stemformatics_data.atlas setExpressionValuesInRedis /data/redis/redis.sock /var/www/pylons-data/prod/PCAFiles/atlas
# Remove existing values under the old key
redis-cli -s /mnt/data/redis/redis.sock --scan --pattern "expression|atlas|imac|*" | xargs redis-cli del

# Change ordering of sample columns so that Cell Type comes first
python -m S4M_pyramid.model.stemformatics_data.atlas changeSampleGroupOrdering /data/redis/redis.sock /var/www/pylons-data/prod/PCAFiles/atlas

# Version change
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.2.4' where ref_type = 'stemformatics_version';"
