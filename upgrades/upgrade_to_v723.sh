#!/bin/bash
# Note that redeploy script will overwrite changes to the sql tables unless failover is switched off
# by touch /tmp/stop_failover.txt. So run this script after that, then run redeploy script, followed
# by any changes through the front-end admin interface.

# Copy these files from pipe, rather than getting them processed from the data portal for now
mkdir -p /data/db/new_datasets
scp -r pipe.stemformatics.org:new_datasets/7415 /data/db/new_datasets

# Version change
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.2.3' where ref_type = 'stemformatics_version';"

# Update Jarny's role
psql -U portaladmin portal_prod -c "update stemformatics.users set is_admin='t',role='admin' where full_name like 'Jarny%';"

# Run add_new_dataset.py after being in the right env
source /var/www/pyramid/virtualenv/bin/activate
python scripts/add_new_dataset.py 7415
