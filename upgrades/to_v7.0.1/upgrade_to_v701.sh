#!/bin/bash

cd /data/repo/git-working/s4m_pyramid/upgrades/to_v7.0.1/
./db_scripts.sh


psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v7.0.1' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_701' where ref_type = 'cdn_base_url';"


wget http://localhost:5000/api/trigger_config_update -O /dev/null


echo "Now you will need to upgrade the help to v7.0.1"
