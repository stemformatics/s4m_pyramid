psql -U portaladmin portal_prod -c "update stemformatics.audit_log set uid = 0 where uid not in (select uid from stemformatics.users where role != 'normal');"
psql -U portaladmin portal_prod -c "update stemformatics.dataset_download_audits set uid = 0 where ds_id not in (select id from datasets where private = True);"
psql -U portaladmin portal_prod -c "update stemformatics.dataset_download_audits set ip_address = '' ;"
psql -U portaladmin portal_prod -c "update stemformatics.audit_log set ip_address = ''";

