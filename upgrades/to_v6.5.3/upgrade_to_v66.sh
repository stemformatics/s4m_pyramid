#!/bin/bash

cd /data/repo/git-working/stemformatics/upgrades/to_v6.5.3/
./2626_copy_over_assay_platform_fields_to_metastore.sh
./2626_collapse_platforms.sh


psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v6.6' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_66' where ref_type = 'cdn_base_url';"

wget http://localhost:5000/api/trigger_config_update


echo "Now you will need to upgrade the help to v6.6"
