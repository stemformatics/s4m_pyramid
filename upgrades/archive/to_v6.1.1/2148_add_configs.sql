-- This is to add configuration table to stemformatics.
drop table stemformatics.configs;
create table stemformatics.configs (id integer, ref_type text, ref_id text);
create unique index config_id on stemformatics.configs (id);
create index config_ref_type_ref_id on stemformatics.configs (ref_type,ref_id);

CREATE SEQUENCE stemformatics.config_id_seq;
ALTER TABLE stemformatics.configs alter column id set default nextval('stemformatics.config_id_seq');
ALTER SEQUENCE stemformatics.config_id_seq OWNED BY stemformatics.configs.id;

