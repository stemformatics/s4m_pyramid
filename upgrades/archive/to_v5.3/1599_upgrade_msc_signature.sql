-- this is to create RohartMSCAccess for anything that is yugene

delete from dataset_metadata where ds_name = 'RohartMSCAccess';
insert into dataset_metadata (select id,'RohartMSCAccess','True' from datasets where show_yugene is True and db_id = 56);
insert into dataset_metadata (select id,'RohartMSCAccess','False' from datasets where show_yugene is False);
insert into dataset_metadata (select id,'RohartMSCAccess','False' from datasets where show_yugene is True and db_id = 46);
