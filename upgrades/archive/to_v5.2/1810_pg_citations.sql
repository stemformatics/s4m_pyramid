-- This SQL file supercedes file "1843_pg_citations.sql" which has been removed from the source tree.
-- All SQL from that file was ported to this SQL update file and subsequently includes updates
-- which make that previous file for Task #1843 redundant.
--

-- OK: Not sure why the following SQL update statement is here - I'm guessing it's just for historical reasons?

-- This can also update the Title to be the Publication Title for all datasets
-- update dataset_metadata as dsm1 set ds_value = dsm2.ds_value from dataset_metadata as dsm2 where dsm2.ds_id = dsm1.ds_id and dsm1.ds_name = 'Title' and dsm2.ds_name = 'Publication Title';


---- IGNORE EVERYTHING ABOVE THIS LINE

----BEGIN CONSISTENCY CHECK, NOTHING ACTUALLY GETS UPDATED
--we check consistency of pubmed ids and publication citations
--  pubmed IDs should be blank
--  publication citations should contain the expected DOIs
--  SELECT * 
--  FROM public.dataset_metadata 
--  WHERE ds_id 
--  IN (6126,6198,6482,6483,6131,6368,6197,6127,6128,6130,6151,6150,6149) 
--  AND (ds_name = 'Publication Citation' OR ds_name = 'PubMed ID');

--we check consistency of cellsSamplesAssayed field in PG datasets
--  SELECT * 
--  FROM public.dataset_metadata 
--  WHERE ds_id 
--  IN (6126,6198,6482,6483,6131,6368,6197,6127,6128,6130,6151,6150,6149)
--  AND (ds_name = 'cellsSamplesAssayed');

----END CONSISTENCY CHECK


----BEGIN CLEANUP HISTORICAL DATASETS
--  psql updates for all PG-associated datasets
--  pseudo-nuke all legacy PG datasets
--  we just add the prefix [RETIRED]
UPDATE public.dataset_metadata 
    SET ds_value = '[RETIRED] Project Grandiose -  An instruction manual of somatic cell reprogramming' 
    WHERE ds_id = 5037 
    AND ds_name = 'Title';

UPDATE public.dataset_metadata 
    SET ds_value = '[RETIRED] Project Grandiose miRNA' 
    WHERE ds_id = 6082 
    AND ds_name = 'Title';

UPDATE public.dataset_metadata 
    SET ds_value = '[RETIRED] Project Grandiose Total Proteomics' 
    WHERE ds_id = 6084 
    AND ds_name = 'Title';

UPDATE public.dataset_metadata 
    SET ds_value = '[RETIRED] Project Grandiose RNASeq (Gene)' 
    WHERE ds_id = 6107 
    AND ds_name = 'Title';

UPDATE public.dataset_metadata 
    SET ds_value = '[RETIRED] Project Grandiose RNASeq (Transcript)'
    WHERE ds_id = 6118
    AND ds_name = 'Title';
    
UPDATE public.dataset_metadata 
    SET ds_value = '[RETIRED] Project Grandiose - Routes to induced pluripotency - RNASeq (transcript)'
    WHERE ds_id = 6138
    AND ds_name = 'Title';
    
UPDATE public.dataset_metadata
    SET ds_value = '[RETIRED] Project Grandiose - Routes to induced pluripotency - RNASeq (gene)'
    WHERE ds_id = 6139
    AND ds_name = 'Title';

----END CLEANUP HISTORICAL DATASETS
--  global fix for nuking PubMed Ids from all datasets
--  update PubMed Id
UPDATE public.dataset_metadata 
    SET ds_value = '' 
    WHERE ds_id 
    IN (6126,6198,6482,6483,6131,6368,6197,6127,6128,6130,6151,6150,6149) 
    AND ds_name = 'PubMed ID';


----BEGIN EDITS FOR PG DATASETS ASSOCIATED WITH CELL STATE PAPER (TONGE ET AL)
--  6126, [6198], 6482, 6483

--  i think this line nukes all first author fields? didnt write this bit
--insert into dataset_metadata (ds_id, ds_name,ds_value) select ds_id,'First Authors','' from dataset_metadata where ds_name = 'Description';

--  update date
UPDATE public.dataset_metadata 
    SET ds_value = '2014-12-30' 
    WHERE ds_id IN (6126,6198,6482,6483) 
    AND ds_name = 'Publication Date';
    
--  update first authors
UPDATE public.dataset_metadata 
    SET ds_value = 'Tonge, P. D.' 
    WHERE ds_id IN (6126,6198,6482,6483) 
    AND ds_name = 'First Authors';

--  update Publication Citation
UPDATE public.dataset_metadata 
    SET ds_value = 'Nature. DOI: 10.1038/nature14047' 
    WHERE ds_id IN (6126,6198,6482,6483) 
    AND ds_name = 'Publication Citation';

--  update Publication Title
UPDATE public.dataset_metadata 
    SET ds_value = 'Divergent reprogramming routes lead to alternative stem-cell states.' 
    WHERE ds_id IN (6126,6198,6482,6483)
    AND ds_name = 'Publication Title';

--  update affiliations
UPDATE public.dataset_metadata 
    SET ds_value = 'Lunenfeld-Tanenbaum Research Institute, Mount Sinai Hospital Joseph and Wolf Lebovic Health Complex, 600 University Avenue, Toronto, Ontario, Canada  M5G 1X5' 
    WHERE ds_id IN (6126,6198,6482,6483,6149,6150,6151)
    AND ds_name = 'Affiliation';

--  update authors
UPDATE public.dataset_metadata 
    SET ds_value = 'Peter D. Tonge, Andrew J. Corso, Claudio Monetti, Samer M. I. Hussein, Mira C. Puri, Iacovos P. Michael, Mira Li, Dong-Sung Lee, Jessica C. Mar, Nicole Cloonan, David L. Wood, Maely E. Gauthier, Othmar Korn, Jennifer L. Clancy, Thomas Preiss, Sean M. Grimmond, Jong-Yeon Shin, Jeong-Sun Seo, Christine A. Wells, Ian M. Rogers and Andras Nagy' 
    WHERE ds_id IN (6126,6198,6482,6483) 
    AND ds_name = 'Authors';

--  update DOI (the original abstract was untouched, just appended DOI)
UPDATE public.dataset_metadata 
    SET ds_value = 'Pluripotency is defined by the ability of  a cell to differentiate to the derivatives of all the three embryonic germ layers: ectoderm, mesoderm and endoderm. Pluripotent cells can be captured via the archetypal derivation of embryonic stem cells (ESCs) or via somatic cell reprogramming. Somatic cells are induced to acquire a pluripotent stem cell (iPSC) state through the forced expression of key transcription factors and in the mouse these cells can fulfill the strictest of all developmental assays for pluripotent cells by generating completely iPSC-derived embryos and mice. However, it is not known whether there are additional classes of pluripotent cells, or what the spectrum of reprogrammed phenotypes encompasses. Here, we explore alternative outcomes of somatic reprogramming by fully characterising reprogrammed cells independent of preconceived definitions of iPSC states. We demonstrate that by maintaining elevated reprogramming factor expression, mouse embryonic fibroblasts go through unique epigenetic modifications to arrive at a stable, Nanog positive, alternative pluripotent state. In doing so, we prove that the pluripotent spectrum can encompass multiple, unique cell states. DOI: 10.1038/nature14047' 
    WHERE ds_id IN (6126,6198,6482,6483) 
    AND ds_name = 'Description';

--  update cell types
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id IN (6126,6198)
    AND ds_name = 'cellsSamplesAssayed';

--6126-specific stuff
--  update title
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Divergent reprogramming routes lead to alternative stem cell states. Global expression changes during reprogramming under high or low transgene expression' 
    WHERE ds_id = 6126 
    AND ds_name = 'Title';
    
--  update handle
UPDATE public.datasets 
    SET handle = 'Tonge_2014_Microarray_timecourse' 
    WHERE id = 6126;
    
--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6126
    AND md_name = 'LineGraphGroup'
    AND chip_id IN (
        '6033111110_A', '6033111110_B', '6033111110_C', '6033111110_D', 
        '6033111110_E', '6033111110_F', '6033111110_G'
    );
    
--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id = 6126
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('6033111113_E', '6033111113_G', '6033111113_H');
    
--6198-specific stuff
----6198
--  enable show on PG landing page
UPDATE public.dataset_metadata 
    SET ds_value = 'True' 
    WHERE ds_id = 6198 
    AND ds_name = 'showAsPublication';

-- Tie dataset to Cell State microarray publication for dataset search on unique DS metadata term
DELETE FROM dataset_metadata WHERE ds_id=6198 and ds_name='PG_type';
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (6198, 'PG_type', 'PGCellState');

--  update title
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Divergent reprogramming routes lead to alternative stem cell states. A comparison of F-class and C-class reprogrammed fibroblasts' 
    WHERE ds_id = 6198 
    AND ds_name = 'Title';

--  update handle
UPDATE public.datasets 
    SET handle = 'Tonge_2014_Microarray_Fclass' 
    WHERE id = 6198;

--6482-specific stuff
-- Tie dataset to Cell State microarray publication for dataset search on unique DS metadata term
DELETE FROM dataset_metadata WHERE ds_id=6482 and ds_name='PG_type';
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (6482, 'PG_type', 'PGCellState');

--  update handle
UPDATE public.datasets 
    SET handle = 'Tonge_2014_HDACi_microarray' 
    WHERE id = 6482;
    
--  update lineGraphOrdering
UPDATE public.dataset_metadata
    SET ds_value = '{"0 hours":1, "24 hours":2, "48 hours":3}'
    WHERE ds_id = 6482 
    AND ds_name = 'lineGraphOrdering';
    
--  update title
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Divergent reprogramming routes lead to alternative stem cell states. Temporal effect of HDACi' 
    WHERE ds_id = 6482 
    AND ds_name = 'Title';
    
--  update cell types
UPDATE public.dataset_metadata 
    SET ds_value = 'HDACi treated F-class cells' 
    WHERE ds_id = 6482
    AND ds_name = 'cellsSamplesAssayed';

--  we update annotations to match Christine's edits
--  Sample Type
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi_48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_48h' AND 
        md_name = 'Sample Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi_24h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_24h' AND 
        md_name = 'Sample Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi_0h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_0h' AND 
        md_name = 'Sample Type'
    );

UPDATE public.biosamples_metadata
    SET md_value = 'HDACi_0h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_0h' AND 
        md_name = 'Sample Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi_24h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_24h' AND 
        md_name = 'Sample Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi_48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_48h' AND 
        md_name = 'Sample Type'
    );

    
--  Replicate Group ID
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi1_48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_48h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi1_24h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_24h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi1_0h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_0h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi2_0h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_0h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi2_24h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_24h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi2_48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_48h' AND 
        md_name = 'Replicate Group ID'
    );
    
--  Sample Description
UPDATE public.biosamples_metadata
    SET md_value = 'Clone 1 subline1 plus dox 48h of 10nM TSA rep1' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_48h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Clone 1 subline1 plus dox 48h of 10nM TSA rep2' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_48h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Clone 1 subline1 plus dox 24h of 10nM TSA rep1' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_24h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Clone 1 subline1 plus dox 24h of 10nM TSA rep2' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_24h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Clone 1 subline1 plus dox no HDACi rep1' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi1_0h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Clone 1 subline1 plus dox no HDACi rep2' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id = 'HDACi2_0h' AND 
        md_name = 'Replicate Group ID'
    );
    
--  Tissue
UPDATE public.biosamples_metadata
    SET md_value = 'NULL' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN (
            'HDACi1_48h', 'HDACi2_48h', 
            'HDACi1_24h', 'HDACi2_24h', 
            'HDACi1_0h', 'HDACi2_0h'
        ) AND 
        md_name = 'Tissue'
    );
    
--  Cell Type
UPDATE public.biosamples_metadata
    SET md_value = 'HDACi treated iPSC' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_48h', 'HDACi2_48h', 'HDACi1_24h', 'HDACi2_24h') AND 
        md_name = 'Cell Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'F-class iPSC' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_0h', 'HDACi2_0h') AND 
        md_name = 'Cell Type'
    );
    
--  Organism
UPDATE public.biosamples_metadata
    SET md_value = 'Mus musculus' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN (
            'HDACi1_48h', 'HDACi2_48h', 
            'HDACi1_24h', 'HDACi2_24h', 
            'HDACi1_0h', 'HDACi2_0h'
        ) AND 
        md_name = 'Organism'
    );
    
--  Day
UPDATE public.biosamples_metadata
    SET md_value = '48 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_48h', 'HDACi2_48h') AND 
        md_name = 'Day'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = '24 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_24h', 'HDACi2_24h') AND 
        md_name = 'Day'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = '0 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_0h', 'HDACi2_0h') AND 
        md_name = 'Day'
    );
    
--  Disease State
UPDATE public.biosamples_metadata
    SET md_value = 'Normal' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN (
            'HDACi1_48h', 'HDACi2_48h', 
            'HDACi1_24h', 'HDACi2_24h', 
            'HDACi1_0h', 'HDACi2_0h'
        ) AND 
        md_name = 'Disease State'
    );
    
--  Labelling
UPDATE public.biosamples_metadata
    SET md_value = 'biotin' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN (
            'HDACi1_48h', 'HDACi2_48h', 
            'HDACi1_24h', 'HDACi2_24h', 
            'HDACi1_0h', 'HDACi2_0h'
        ) AND 
        md_name = 'Labelling'
    );
    
--  LineGraphGroup
UPDATE public.biosamples_metadata
    SET md_value = '48 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_48h', 'HDACi2_48h') AND 
        md_name = 'LineGraphGroup'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = '24 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_24h', 'HDACi2_24h') AND 
        md_name = 'LineGraphGroup'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = '0 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('HDACi1_0h', 'HDACi2_0h') AND 
        md_name = 'LineGraphGroup'
    );
    
--  Organism Part
UPDATE public.biosamples_metadata
    SET md_value = 'in vitro' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN (
            'HDACi1_48h', 'HDACi2_48h', 
            'HDACi1_24h', 'HDACi2_24h', 
            'HDACi1_0h', 'HDACi2_0h'
        ) AND 
        md_name = 'Organism Part'
    );

--  Legends
UPDATE public.biosamples_metadata
    SET md_value = '0 hours HDACi untreated'
    WHERE ds_id = 6482
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('HDACi1_0h', 'HDACi2_0h');

UPDATE public.biosamples_metadata
    SET md_value = '24 hours HDACi treated'
    WHERE ds_id = 6482
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('HDACi1_24h', 'HDACi2_24h');

UPDATE public.biosamples_metadata
    SET md_value = '48 hours HDACi treated'
    WHERE ds_id = 6482
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('HDACi1_48h', 'HDACi2_48h');
    
--  this shows up on Stemformatics "Our Publications" page. it shouldnt
UPDATE public.dataset_metadata 
    SET ds_value = 'False' 
    WHERE ds_id = 6482 
    AND ds_name = 'showAsPublication';
    
    
--6483-specific stuff
-- Tie dataset to Cell State microarray publication for dataset search on unique DS metadata term
DELETE FROM dataset_metadata WHERE ds_id=6483 and ds_name='PG_type';
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (6483, 'PG_type', 'PGCellState');

--  update handle
UPDATE public.datasets 
    SET handle = 'Tonge_2014_IPSC-Myc_microarray' 
    WHERE id = 6483;
    
--  update lineGraphOrdering
UPDATE public.dataset_metadata
    SET ds_value = '{"0 hours":1, "48 hours":2}'
    WHERE ds_id = 6483 
    AND ds_name = 'lineGraphOrdering';
    
--  update title
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Divergent reprogramming routes lead to alternative stem cell states. Requirement of all four reprogramming factors' 
    WHERE ds_id = 6483 
    AND ds_name = 'Title';

--  update cell types
UPDATE public.dataset_metadata 
    SET ds_value = 'F-class iPSC, iPSC_tetOMyc' 
    WHERE ds_id = 6483
    AND ds_name = 'cellsSamplesAssayed';

--  we update annotations to match Christine's edits
--  Sample Type
UPDATE public.biosamples_metadata
    SET md_value = 'CAG-3F tet-Myc plus dox' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h') AND 
        md_name = 'Sample Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'CAG-3F tet-Myc no dox for 48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Sample Type'
    );
    
--  Replicate Group ID
UPDATE public.biosamples_metadata
    SET md_value = 'Tet-Myc1 0h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id = 'Myc5_0h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Tet-Myc1 48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id = 'Myc5_24h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Tet-Myc2 0h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id = 'Myc1_0h' AND 
        md_name = 'Replicate Group ID'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Tet-Myc2 48h' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id = 'Myc1_48h' AND 
        md_name = 'Replicate Group ID'
    );
    
--  Sample Description
UPDATE public.biosamples_metadata
    SET md_value = 'F-class' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h') AND 
        md_name = 'Sample Description'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'Differentiated' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Sample Description'
    );
    
--  Tissue
UPDATE public.biosamples_metadata
    SET md_value = 'NULL' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h', 'Myc5_48h', 'Myc1_48h') AND
        md_name = 'Tissue'
    );
    
--  Cell Type
UPDATE public.biosamples_metadata
    SET md_value = 'iPSC' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h') AND 
        md_name = 'Cell Type'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = 'iPSC_differentiated' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Cell Type'
    );
    
--  Organism
UPDATE public.biosamples_metadata
    SET md_value = 'Mus musculus' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h', 'Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Organism'
    );
    
--  Day
UPDATE public.biosamples_metadata
    SET md_value = '48 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Day'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = '0 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h') AND 
        md_name = 'Day'
    );
    
--  Disease State
UPDATE public.biosamples_metadata
    SET md_value = 'Normal' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h', 'Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Disease State'
    );
    
--  Labelling
UPDATE public.biosamples_metadata
    SET md_value = 'biotin' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h', 'Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Labelling'
    );
    
--  LineGraphGroup
UPDATE public.biosamples_metadata
    SET md_value = '48 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_48h', 'Myc1_48h') AND 
        md_name = 'LineGraphGroup'
    );
    
UPDATE public.biosamples_metadata
    SET md_value = '0 hours' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6483 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h') AND 
        md_name = 'LineGraphGroup'
    );
    
--  Organism Part
UPDATE public.biosamples_metadata
    SET md_value = 'in vitro' 
    WHERE (
        chip_type = 72 AND 
        ds_id = 6482 AND 
        chip_id IN ('Myc5_0h', 'Myc1_0h', 'Myc5_48h', 'Myc1_48h') AND 
        md_name = 'Organism Part'
    );

--  Legends
UPDATE public.biosamples_metadata
    SET md_value = '0 hours Myc untreated'
    WHERE ds_id = 6483
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('Myc1_0h', 'Myc5_0h');

UPDATE public.biosamples_metadata
    SET md_value = '48 hours Myc treated'
    WHERE ds_id = 6483
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('Myc1_48h', 'Myc5_48h');

-- END EDITS FOR PG DATASETS ASSOCIATED WITH CELL STATE PAPER (TONGE ET AL)

-- BEGIN EDITS FOR PG DATASETS ASSOCIATED WITH MIRNA PAPER (CLANCY ET AL)
--6128-specific stuff
--  change y-axis label
UPDATE public.assay_platforms
    SET y_axis_label = 'CPM (Log2)'
    WHERE chip_type = 103;
-- END EDITS FOR PG DATASETS ASSOCIATED WITH MIRNA PAPER (CLANCY ET AL)

-- BEGIN EDITS FOR PG DATASETS ASSOCIATED WITH METHYLATION PAPER (LEE ET AL)
--6131 AND 6368-specific stuff

-- Tie dataset to CpG Methylation publication for dataset search on unique DS metadata term
DELETE FROM dataset_metadata WHERE ds_id=6131 and ds_name='PG_type';
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (6131, 'PG_type', 'PGMethylationCpG');

--  change y-axis labels for 6131 and 6368
UPDATE public.assay_platforms
    SET y_axis_label = 'Average Methylated CpG (%)'
    WHERE chip_type in (107,137);

-- change probe name for 6131 and 6368
UPDATE public.assay_platforms
    SET probe_name = 'Feature'
    WHERE chip_type in (107,137);
    
----6131-specific
UPDATE public.dataset_metadata 
    SET ds_value = 'True' 
    WHERE ds_id = 6131 
    AND ds_name = 'showAsPublication';
    
UPDATE public.dataset_metadata 
    SET ds_value = '2014-12' 
    WHERE ds_id = 6131 
    AND ds_name = 'Publication Date';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Lee, D-S.' 
    WHERE ds_id = 6131 
    AND ds_name = 'First Authors';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: An epigenomic roadmap to induced pluripotency - MethylCpG' 
    WHERE ds_id = 6131 
    AND ds_name = 'Title';
    
--  update cell types
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id = 6131 
    AND ds_name = 'cellsSamplesAssayed';
    
--Mira Puri's corrections
UPDATE public.dataset_metadata 
    SET ds_value = 'Dong-Sung Lee*, Jong-Yeon Shin*, Peter D. Tonge, Mira C. Puri, Seungbok Lee, Hansoo Park, Won-Chul Lee, Samer M.I. Hussein, Thomas Bleazard, Ji-Young Yun, Jihye Kim, Mira Li, Nicole Cloonan, David Wood, Jennifer L. Clancy, Rowland Mosbergen, Jae-Hyuk Yi, Kap-Seok Yang, Hyungtae Kim, Hwanseok Rhee, Christine A. Wells, Thomas Preiss, Sean M. Grimmond, Ian M. Rogers, Andras Nagy & Jeong-Sun Seo' 
    WHERE ds_id = 6131 
    AND ds_name = 'Authors';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Reprogramming of somatic cells to induced pluripotent stem cells involves a dynamic rearrangement of the epigenetic landscape. To characterize this epigenomic roadmap, we have performed MethylC-seq, ChIP-seq (H3K4/K27/K36me3) and RNA-Seq on samples taken at several time points during murine secondary reprogramming as part of Project Grandiose. We find that DNA methylation gain during reprogramming occurs gradually, while loss is achieved only at the ESC-like state. Binding sites of activated factors exhibit focal demethylation during reprogramming, while ESC-like pluripotent cells are distinguished by extension of demethylation to the wider neighbourhood. We observed that genes with CpG-rich promoters demonstrate stable low methylation and strong engagement of histone marks, whereas genes with CpG-poor promoters are safeguarded by methylation. Such DNA methylation-driven control is the key to the regulation of ESC-pluripotency genes, including Dppa4, Dppa5a and Esrrb. These results reveal the crucial role that DNA methylation plays as an epigenetic switch driving somatic cells to pluripotency. DOI: 10.1038/ncomms6619' 
    WHERE ds_id = 6131 
    AND ds_name = 'Description';

--Dong Sung Lee's corrections
UPDATE public.datasets 
    SET handle = 'Lee_2014_CpGmethylation' 
    WHERE id = 6131;
    
UPDATE public.dataset_metadata
    SET ds_value = 'ENSMUSG00000058550,ENSMUSG00000060461,ENSMUSG00000021255'
    WHERE ds_id = 6131
    AND ds_name = 'topDifferentiallyExpressedGenes';

UPDATE public.dataset_metadata 
    SET ds_value = 'An epigenomic roadmap to induced pluripotency reveals DNA methylation as a reprogramming modulator.' 
    WHERE ds_id = 6131 
    AND ds_name = 'Publication Title';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Nature Communications. DOI: 10.1038/ncomms6619' 
    WHERE ds_id = 6131 
    AND ds_name = 'Publication Citation';
    
--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6131
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0','D11-H','D16-H','D18-H','D2-H','D5-H','D8-H');

--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id = 6131
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D16A-L', 'D21-L0', 'D21-L');
    
--  have the cellsSamplesAssayed been re-annotated manually?

--END EDITS FOR PG DATASETS ASSOCIATED WITH METHYLATION PAPER (LEE ET AL)

--BEGIN EDITS FOR PG DATASETS ASSOCIATED WITH UMBRELLA PAPER (HUSSEIN ET AL)

--[6197],6368,6127,6151,6150,6149
--  publication date
UPDATE public.dataset_metadata 
    SET ds_value = '2014-12-31' 
    WHERE ds_id IN (6197,6368,6127,6151,6150,6149) 
    AND ds_name = 'Publication Date';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Hussein, S. M. I., Puri, M. C., Tonge, P. D.' 
    WHERE ds_id IN (6197,6368,6127,6151,6150,6149) 
    AND ds_name = 'First Authors';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Genome-wide characterization of the routes to pluripotency.' 
    WHERE ds_id IN (6197,6368,6127,6151,6150,6149) 
    AND ds_name = 'Publication Title';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Nature. DOI: 10.1038/nature14046' 
    WHERE ds_id IN (6197,6368,6127,6151,6150,6149) 
    AND ds_name = 'Publication Citation';
    
--Mira Puri's corrections
UPDATE public.dataset_metadata 
    SET ds_value = 'Samer M. I. Hussein*, Mira C. Puri*, Peter D. Tonge*, Marco Benevento, Andrew J. Corso, Jennifer L. Clancy, Rowland Mosbergen, Mira Li, Dong-Sung Lee, Nicole Cloonan, David L. A. Wood, Javier Munoz, Robert Middleton, Othmar Korn, Hardip R. Patel, Carl A. White, Jong-Yeon Shin, Maely E. Gauthier, Kim-Anh Lê Cao, Jong-Il Kim, Jessica C. Mar, Nika Shakiba, William Ritchie, John E.J. Rasko, Sean M. Grimmond, Peter W. Zandstra, Christine A. Wells, Thomas Preiss, Jeong-Sun Seo, Albert J.R. Heck, Ian M. Rogers, and Andras Nagy' 
    WHERE ds_id IN (6197,6368,6127,6151,6150,6149) 
    AND ds_name = 'Authors';

UPDATE public.dataset_metadata 
    SET ds_value = 'Somatic cell reprogramming to a pluripotent state continues to challenge many of our assumptions about cellular specification, and despite major efforts, we lack a complete molecular characterization of the reprograming process. To address this gap we generated extensive transcriptomic, epigenomic and proteomic datasets describing the reprogramming routes leading to pluripotency. Through integrative analysis, we reveal that cells transition through distinct gene expression and epigenetic signatures and bifurcate towards reprogramming transgene-dependent and -independent stable pluripotent states. Early transcriptional events, driven by high reprogramming factor expression, are associated with widespread loss of H3K27me3, representing a general opening of the chromatin state. Maintenance of high transgene levels leads to re-acquisition of H3K27me3 and a stable pluripotent state that is alternative to the embryonic stem cell (ESC)-like fate. Lowering transgene levels at an intermediate phase, however, guides the process to the acquisition of ESC-like chromatin and DNA methylation signature. Our data provide a comprehensive molecular description of the reprogramming routes and is accessible through the Project Grandiose portal at www.stemformatics.org. DOI: 10.1038/nature14046' 
    WHERE ds_id IN (6197,6368,6127,6151,6150,6149) 
    AND ds_name = 'Description';
    
    
----6197
UPDATE public.datasets 
    SET handle = 'Hussein_Puri_Tonge_2014_RNASeq' 
    WHERE id = 6197;

UPDATE public.dataset_metadata 
    SET ds_value = 'True' 
    WHERE ds_id = 6197 
    AND ds_name = 'showAsPublication';
    
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Routes to induced pluripotency - RNASeq' 
    WHERE ds_id = 6197 
    AND ds_name = 'Title';
    
--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6197
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0','D11-H','D16-H','D18-H','D2-H','D5-H','D8-H');

--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id = 6197
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D16A-L', 'D21-L0', 'D21-L');

--Pete's instructions
--remove F and C class iPSCs from cellsSamplesAssayed
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id = 6197 
    AND ds_name = 'cellsSamplesAssayed';

----6368-specific
-- Tie dataset to CpG methylation publication for dataset search on unique DS metadata term
DELETE FROM dataset_metadata WHERE ds_id=6368 and ds_name='PG_type';
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (6368, 'PG_type', 'PGMethylationCpG');

UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Routes to induced pluripotency - Methylation' 
    WHERE ds_id = 6368 
    AND ds_name = 'Title';

UPDATE public.datasets 
    SET handle = 'Hussein_Puri_Tonge_2014_Methylation' 
    WHERE id = 6368;

--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6368
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0','D11-H','D16-H','D18-H','D2-H','D5-H','D8-H');

--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id = 6368
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D16A-L', 'D21-L0', 'D21-L');
    
--remove F and C class iPSCs from cellsSamplesAssayed
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id = 6368 
    AND ds_name = 'cellsSamplesAssayed';
    
----6127
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Routes to induced pluripotency - Cell Surface Proteome' 
    WHERE ds_id = 6127 
    AND ds_name = 'Title';

--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6127
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0', 'D11-H', 'D16-H', 'D18-H', 'D2-H', 'D5-H', 'D8-H');

--pre-emptive psql
--handle handle
UPDATE public.datasets 
    SET handle = 'Hussein_Puri_Tonge_Cell_Surface_Proteome' 
    WHERE id = 6127;

--remove F and C class iPSCs from cellsSamplesAssayed
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id = 6127 
    AND ds_name = 'cellsSamplesAssayed';


----6149,6150,6151
--  Christine's instructions
--  LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id IN (6149,6150,6151)
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0','D11-H','D16-H','D18-H','D2-H','D5-H','D8-H');

--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id IN (6149,6150,6151)
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D16A-L', 'D21-L0', 'D21-L');

--  pre-emptive psql fix
--  remove F and C class iPSCs from cellsSamplesAssayed
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id IN (6149,6150,6151)
    AND ds_name = 'cellsSamplesAssayed';

--  update genes of interest
UPDATE public.dataset_metadata
    SET ds_value = 'ENSMUSG00000074637,ENSMUSG00000000303,ENSMUSG00000012396'
    WHERE ds_id IN (6149,6150,6151)
    AND ds_name = 'topDifferentiallyExpressedGenes';

--  update platform for chromatin data
UPDATE public.dataset_metadata 
    SET ds_value = 'Illumina HiSeq 2000 ChIP-seq for Histone Marks mm9'
    WHERE ds_id IN (6149,6150,6151)
    AND ds_name = 'Platform';

--  change y-axis label
UPDATE public.assay_platforms
    SET y_axis_label = 'MACS enriched peaks (average RPKM)'
    WHERE chip_type = 108;

----6151
--  handle handles
UPDATE public.datasets 
    SET handle = 'Hussein_Puri_Tonge_2014_H3K36me3' 
    WHERE id = 6151;

--  handle titles
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Routes to induced pluripotency - H3K36me3' 
    WHERE ds_id = 6151 
    AND ds_name = 'Title';

----6150
--  handle handles
UPDATE public.datasets 
    SET handle = 'Hussein_Puri_Tonge_2014_H3K27me3' 
    WHERE id = 6150;

--  handle titles
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Routes to induced pluripotency - H3K27me3' 
    WHERE ds_id = 6150 
    AND ds_name = 'Title';

----6149
--  handle handles
UPDATE public.datasets 
    SET handle = 'Hussein_Puri_Tonge_2014_H3K4me3' 
    WHERE id = 6149;

--  handle titles
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Routes to induced pluripotency - H3K4me3' 
    WHERE ds_id = 6149 
    AND ds_name = 'Title';


----6128
--  show this on PG landing page
UPDATE public.dataset_metadata 
    SET ds_value = 'True' 
    WHERE ds_id = 6128 
    AND ds_name = 'showAsPublication';

--  add DOI
UPDATE public.dataset_metadata 
    SET ds_value = 'Nature Communications. DOI: 10.1038/ncomms6522' 
    WHERE ds_id = 6128 
    AND ds_name = 'Publication Citation';

--  update date
UPDATE public.dataset_metadata 
    SET ds_value = '2014-12' 
    WHERE ds_id = 6128 
    AND ds_name = 'Publication Date';

--  update first authors
UPDATE public.dataset_metadata 
    SET ds_value = 'Clancy, J. L.' 
    WHERE ds_id = 6128 
    AND ds_name = 'First Authors';
    
--  update paper title
UPDATE public.dataset_metadata 
    SET ds_value = 'Small RNA changes en route to distinct cellular states of induced pluripotency.' 
    WHERE ds_id = 6128 
    AND ds_name = 'Publication Title';
    
--  update dataset title
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Small RNA changes en route to distinct cellular states of induced pluripotency - miRNA' 
    WHERE ds_id = 6128 
    AND ds_name = 'Title';
    
----Mira Puri's corrections
--  author list
UPDATE public.dataset_metadata 
    SET ds_value = 'Jennifer L. Clancy, Hardip R. Patel, Samer M.I. Hussein, Peter D. Tonge, Nicole Cloonan, Andrew J. Corso, Mira Li, Dong-Sung Lee, Jong-Yeon Shin, Justin J.L. Wong, Charles G. Bailey, Marco Benevento, Javier Munoz, Aaron Chuah, David Wood, John E.J. Rasko, Albert J.R. Heck, Sean M. Grimmond, Ian M. Rogers, Jeong-Sun Seo, Christine A. Wells, Mira C. Puri, Andras Nagy & Thomas Preiss' 
    WHERE ds_id = 6128 
    AND ds_name = 'Authors';
    
--  abstract
UPDATE public.dataset_metadata 
    SET ds_value = 'MicroRNAs (miRNAs) are critical to somatic cell reprogramming into induced pluripotent stem cells (iPSCs), however, exactly how miRNA expression changes support the transition to pluripotency requires further investigation. Here we use a murine secondary reprogramming system to sample cellular trajectories towards iPSCs or a novel pluripotent "F-class" state and perform small RNA sequencing. We detect sweeping changes in an early and a late wave, revealing that distinct miRNA milieus characterize alternate states of pluripotency. miRNA isoform expression is common but surprisingly varies little between cell states. Referencing other omic data sets generated in parallel, we find that miRNA expression is changed through transcriptional and post-transcriptional mechanisms. miRNA transcription is commonly regulated by dynamic histone modification, while DNA methylation/demethylation consolidates these changes at multiple loci. Importantly, our results suggest that a novel subset of distinctly expressed miRNAs supports pluripotency in the F-class state, substituting for miRNAs that serve such roles in iPSCs. DOI: 10.1038/ncomms6522' 
    WHERE ds_id = 6128 
    AND ds_name = 'Description';

--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6128
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0', 'D11', 'D16', 'D18', 'D2', 'D5', 'D8');

--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id = 6128
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D16APG2r2', 'D21PG2r2DoxMinus', 'D21PG2r2DoxPlus');

--  pre-emptive fix
--  remove F and C class iPSCs from cellsSamplesAssayed
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id = 6128 
    AND ds_name = 'cellsSamplesAssayed';

--  updated handle
UPDATE public.datasets 
    SET handle = 'Clancy_2014_miRNA' 
    WHERE id = 6128;

--  Jennifer's corrections
--  update platform for miRNA data
UPDATE public.dataset_metadata 
    SET ds_value = 'Small RNA preparation protocol, SOLiD v4 Instrument mm9'
    WHERE ds_id = 6128
    AND ds_name = 'Platform';
    

----6130
--  show this on PG landing page
UPDATE public.dataset_metadata 
    SET ds_value = 'True' 
    WHERE ds_id = 6130 
    AND ds_name = 'showAsPublication';

--  add DOI
UPDATE public.dataset_metadata 
    SET ds_value = 'Nature Communications. DOI: 10.1038/ncomms6613' 
    WHERE ds_id = 6130 
    AND ds_name = 'Publication Citation';

--  update date
UPDATE public.dataset_metadata 
    SET ds_value = '2014-12' 
    WHERE ds_id = 6130 
    AND ds_name = 'Publication Date';

--  update first authors
UPDATE public.dataset_metadata 
    SET ds_value = 'Benevento, M.' 
    WHERE ds_id = 6130 
    AND ds_name = 'First Authors';

--  update paper title
UPDATE public.dataset_metadata 
    SET ds_value = 'Proteome adaptation in cell reprogramming proceeds via distinct transcriptional networks.' 
    WHERE ds_id = 6130 
    AND ds_name = 'Publication Title';

--  update dataset title
UPDATE public.dataset_metadata 
    SET ds_value = 'Project Grandiose: Proteome adaptation in cell reprogramming proceeds via distinct transcriptional networks - global LC-MS protein' 
    WHERE ds_id = 6130 
    AND ds_name = 'Title';

----Mira Puri's corrections
--  author list
UPDATE public.dataset_metadata 
    SET ds_value = 'Marco Benevento, Peter D. Tonge, Mira C. Puri, Samer M.I. Hussein, Nicole Cloonan, David L. Wood, Sean M. Grimmond, Andras Nagy, Javier Munoz & Albert J.R. Heck' 
    WHERE ds_id = 6130 
    AND ds_name = 'Authors';
    
--  abstract
UPDATE public.dataset_metadata 
    SET ds_value = 'The ectopic expression of Oct4, Klf4, c-Myc and Sox2 (OKMS) transcription factors allows reprogramming of somatic cells into induced pluripotent stem cells (iPSCs). The reprogramming process, which involves a complex network of molecular events, is not yet fully characterized. Here, we perform a quantitative mass spectrometry-based analysis to probe in-depth dynamic proteome changes during somatic cell reprogramming. Our data reveal defined waves of proteome resetting, with the first wave occurring 48 h after the activation of the reprogramming transgenes and involving specific biological processes linked to the c-Myc transcriptional network. A second wave of proteome reorganization occurs in a later stage of reprogramming, where we characterize the proteome of two distinct pluripotent cellular populations. In addition, the overlay of our proteome resource with parallel generated -omics data is explored to identify post-transcriptionally regulated proteins involved in key steps during reprogramming. DOI: 10.1038/ncomms6613' 
    WHERE ds_id = 6130 
    AND ds_name = 'Description';
    
--Christine's instructions
--LineGraphGroup High and Low to High Dox and Low Dox respectively
--  High Dox
UPDATE public.biosamples_metadata
    SET md_value = 'High Dox'
    WHERE ds_id = 6130
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D0','D11-H','D16-H','D18-H','D2-H','D5-H','D8-H');

--  Low Dox
UPDATE public.biosamples_metadata
    SET md_value = 'Low Dox'
    WHERE ds_id = 6130
    AND md_name = 'LineGraphGroup'
    AND chip_id IN ('D16A-L', 'D21-L0', 'D21-L');

--  pre-emptive fix
--remove F and C class iPSCs from cellsSamplesAssayed
UPDATE public.dataset_metadata 
    SET ds_value = 'mouse embryonic fibroblast (MEF), iPSC, ESC' 
    WHERE ds_id = 6130 
    AND ds_name = 'cellsSamplesAssayed';
    
--pre-emptive psql
--handle handle
UPDATE public.datasets 
    SET handle = 'Benevento_2014_Proteome' 
    WHERE id = 6130;

--  change y-axis label
UPDATE public.assay_platforms
    SET y_axis_label = 'Protein fold change (sample/internal standard) Log2'
    WHERE chip_type = 105;

