#!/bin/bash

# This is to convert the current feature_annotation file that 
# was before T#1822 was created.
# We want to add the unique identifier from mirBase as the first column 
# and get rid of mmu- and hsa-
# please note that to match probes the miR is replaced with mir
# Note: this was causing problems MI0003657

temp_file=/tmp/mirbase_accession_ids.txt
feature_file=feature_annotation.txt
main_id_test_file=/tmp/main_ids.txt
first_column_id_test_file=/tmp/first_column_ids.txt

cd /var/www/pylons-data/prod/

# do some data transformations
sed -i.bak 's/miR-/mir-/g' $feature_file
sed -i.bak1 's/hsa-//' $feature_file
sed -i.bak2 's/mmu-//' $feature_file

cut -f 1 -d',' $feature_file  | cut -f4 > $temp_file

wc -l  $temp_file

# merge the two files together
paste $temp_file $feature_file > new_$feature_file

# now check that the values are the same on the same lines - as a unit test 
cut -f1 new_$feature_file > $main_id_test_file
cut -f 1 -d',' new_$feature_file  | cut -f5 > $first_column_id_test_file
echo "this should be identical"
diff -s $main_id_test_file $first_column_id_test_file

# now actually update the feature file
mv new_$feature_file $feature_file
